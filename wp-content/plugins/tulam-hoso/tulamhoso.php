<?php
/*
Plugin Name: Tu lam ho so plugin
Plugin URI: http://sagojo.com/duhocnhatban/
Description: Plugin tu lam ho so du hoc Nhat ban
Version: 1.0
Author: hanhdo
Author URI: https://facebook.com/hanhdo205
*/

/** Step 2 (from text above). */
add_action( 'admin_menu', 'hoso_plugin_menu' );

/** Step 1. */
function hoso_plugin_menu() {
	add_menu_page( 'Tự làm hồ sơ', 'Tự làm hồ sơ', 'administrator', 'tulam-hoso-admin', 'hoso_admin_page', 'dashicons-admin-users', 6  );
	//add_users_page( 'Đăng ký thông tin', 'Thông tin học viên', 'manage_options', 'my-unique-identifier', 'hoso_plugin_options' );
	add_submenu_page( 'tulam-hoso-admin', 'Danh sách CTV', 'Danh sách CTV', 'administrator', 'congtacvien-page','congtacvien_menu_page' );	
	add_submenu_page( 'hoso_admin_page', 'Thông tin CTV', 'Thông tin CTV', 'administrator', 'thongtin-congtacvien-page','thongtin_congtacvien_menu_page' );	
	add_submenu_page( 'hoso_admin_page', 'Hồ sơ gởi bởi CTV', 'Hồ sơ gởi bởi CTV', 'administrator', 'hoso-congtacvien-page','hoso_congtacvien_menu_page' );
	add_submenu_page( 'hoso_admin_page', 'Chi tiết hồ sơ', 'Chi tiết hồ sơ', 'administrator', 'chitiet-hoso-page','chitiet_hoso_menu_page' );
}

/** Step 3. */
function hoso_admin_page() {
	if ( !current_user_can( 'administrator' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	include_once (WP_PLUGIN_DIR .'/tulam-hoso/tulam-hoso-admin-page.php');
}

function congtacvien_menu_page(){
	if ( !current_user_can( 'administrator' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
		include_once ( WP_PLUGIN_DIR .'/tulam-hoso/danhsach-congtacvien.php');
}

function thongtin_congtacvien_menu_page(){
	if ( !current_user_can( 'administrator' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
		include_once ( WP_PLUGIN_DIR .'/tulam-hoso/thongtin-congtacvien.php');
}

function hoso_congtacvien_menu_page(){
	if ( !current_user_can( 'administrator' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
		include_once ( WP_PLUGIN_DIR .'/tulam-hoso/hoso-nopboi-congtacvien.php');
}

function chitiet_hoso_menu_page(){
	if ( !current_user_can( 'administrator' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
		include_once ( WP_PLUGIN_DIR .'/tulam-hoso/chitiet-hoso.php');
}

function add_roles_on_plugin_activation() {
       add_role( 'ctv_role', 'Cong tac vien', array( 'read' => true, 'level_0' => true ) );
   }
   register_activation_hook( __FILE__, 'add_roles_on_plugin_activation' );


   
// Creating the senpai-widget 
class senpai_widget extends WP_Widget {

function __construct() {
parent::__construct(
// Base ID of your widget
'senpai_widget', 

// Widget name will appear in UI
__('Featured Senpai', 'featured-senpai'), 

// Widget description
array( 'description' => __( 'Danh sách senpai được lựa chọn.', 'sagojo-most-viewed' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
global $wpdb;
$title = apply_filters( 'widget_title', $instance['title'] );
// before and after widget arguments are defined by themes
echo $args['before_widget'];
echo "<div class='senpai-widget'>";
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];
$assigned=array();
$meta = get_post_meta( get_the_ID(),'wpuf_post_tags',true );
if ( ! empty( $meta ) ) {
$items = explode(',', $meta);
foreach($items as $item) {
$postid = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '" . $item . "'" );
array_push($assigned,$postid);
}
// This is where you run the code and display the output.
	$args = array(
		'post_type' => 'portfolio',
		'post__in' => $assigned,
		'posts_per_page'=>-1
    ); 
	$products = new WP_Query( $args );
	echo "<div class='widgets-grid-layout no-grav'>";	
	while ( $products->have_posts() ) {
            $products->the_post();?>
			<div class='widget-grid-view-image'>
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
			<?php if(has_post_thumbnail( $post->ID ) ) { the_post_thumbnail( 'medium' );}
			echo "</a></div>";
	}
echo "</div></div>";
} else echo "Đang cập nhật <img style='float:right;' src='".get_stylesheet_directory_uri()."/assets/img/loading.gif'>";
echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
	$title = $instance[ 'title' ];
}
else {
$title = __( 'Featured Senpai', 'sagojo-most-viewed' );
}

// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>

<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class most-read-widget ends here

function wpb_load_senpai_widget() {
	register_widget( 'senpai_widget' );
}
add_action( 'widgets_init', 'wpb_load_senpai_widget' );
?>