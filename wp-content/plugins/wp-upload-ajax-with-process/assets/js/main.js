(function($) {
	"use strict";

	window.tp_remove_data_upload = function (selector) {
        
        var data = [];

        $(''+ selector +'').click(function(e) {

        	var ajax_url = tp_ajax_url.ajax_url,
        	$this = $(this),
			attach_id = $(this).data('delete'),
			dataclass = $(this).data('class'),
			r = confirm("Bạn chắc chắn muốn bỏ file này?");

			if (r == true) {

				$.ajax({
					type: 'POST',
					url: ajax_url,
					data: {
						action: 'tp_delete_upload',
						attach_id: attach_id
					},
					success: function(respon)
					{						
						$this.parent(dataclass).remove();
						//if( ! $this.parent('#files').find('.success').length ){
						if(dataclass=='.success') {
							$('#upload-results').hide();
							$("#upload-hinh34").show();
							$(".store").val("");
						}
						//else if( ! $this.parent('#files-khaisinh').find('.success-khaisinh').length ){	
						else if(dataclass=='.success-khaisinh') {
							$("#upload-results-khaisinh").hide();
							$("#upload-khaisinh").show();
							$(".store-khaisinh").val("");
						}
						else if(dataclass=='.success-cmnd') {
							$("#upload-results-cmnd").hide();
							$("#upload-cmnd").show();
							//$(".store").val("");
						}
						else if(dataclass=='.success-hokhau') {
							$("#upload-results-hokhau").hide();
							$("#upload-hokhau").show();
							$(".store-hokhau").val("");
						}
						else if(dataclass=='.success-cap3') {
							$("#upload-results-cap3").hide();
							$("#upload-cap3").show();
							$(".store-cap3").val("");
						}
						else if(dataclass=='.success-hocba') {
							$("#upload-results-hocba").hide();
							$("#upload-hocba").show();
							$(".store-hocba").val("");
						}
						else if(dataclass=='.success-daihoc') {
							$("#upload-results-daihoc").hide();
							$("#upload-daihoc").show();
							$(".store-daihoc").val("");
						}
						else if(dataclass=='.success-xacnhan') {
							$("#upload-results-xacnhan").hide();
							$("#upload-xacnhan").show();
							$(".store-xacnhan").val("");
						}
						else if(dataclass=='.success-bangdiem') {
							$("#upload-results-bangdiem").hide();
							$("#upload-bangdiem").show();
							$(".store-bangdiem").val("");
						}
						else if(dataclass=='.success-chungnhan') {
							$("#upload-results-chungnhan").hide();
							$("#upload-chungnhan").show();
							$(".store-chungnhan").val("");
						}
						else if(dataclass=='.success-cmndbaolanh') {
							$("#upload-results-cmndbaolanh").hide();
							$("#upload-cmndbaolanh").show();
							$(".store-cmndbaolanh").val("");
						}
						else if(dataclass=='.success-taikhoan') {
							$("#upload-results-taikhoan").hide();
							$("#upload-taikhoan").show();
							$(".store-taikhoan").val("");
						}
						else if(dataclass=='.success-thunhap') {
							$("#upload-results-thunhap").hide();
							$("#upload-thunhap").show();
							$(".store-thunhap").val("");
						}
						else if(dataclass=='.success-giaykhac') {
							$("#upload-results-giaykhac").hide();
							$("#upload-giaykhac").show();
							$(".store-giaykhac").val("");
						}
					}
				});
			}
        	return false;
        });

    };

})(jQuery);