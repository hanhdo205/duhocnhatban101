<?php
/* 
Plugin Name: Thạch Phạm Upload File
Plugin URI: http://hoidap.thachpham.com/
Description: Upload file ngoài frontend bằng ajax và process.
Author: Vũ Đức Nam
Version: 0.1
Author URI: http://vuducnam.com/
*/

define('TPU_FOLDER', basename(dirname(__FILE__)));
define('TPU_URL', plugin_dir_url(TPU_FOLDER).TPU_FOLDER.'/');
define('TPU_CSS', TPU_URL.'assets/css/');
define('TPU_JS', TPU_URL.'assets/js/');
/**
* 
*/
class TP_Upload_File
{
	
	function __construct()
	{
		/**
		* Đây là action thêm style vào header
		*/
		add_action( 'wp_enqueue_scripts', array($this, 'tp_enqueue_styles') );

		/**
		* Đây là action thêm script vào header vs footer
		*/
		add_action( 'wp_enqueue_scripts', array($this, 'tp_enqueue_scripts') );

		/**
		* Đây là action để thêm shortcode [tp_upload]
		*/
		add_shortcode('tp_upload', array($this, 'tp_shortcode'));

		/**
		* Đây là action đăng ký ajax upload
		*/
		add_action( 'wp_ajax_tp_upload', array($this, 'tp_upload') );
        add_action( 'wp_ajax_nopriv_tp_upload', array($this, 'tp_upload') );
		
		add_action( 'wp_ajax_tp_upload_khaisinh', array($this, 'tp_upload_khaisinh') );
        add_action( 'wp_ajax_nopriv_tp_upload_khaisinh', array($this, 'tp_upload_khaisinh') );
		
		add_action( 'wp_ajax_tp_upload_cmnd', array($this, 'tp_upload_cmnd') );
        add_action( 'wp_ajax_nopriv_tp_upload_cmnd', array($this, 'tp_upload_cmnd') );
		
		add_action( 'wp_ajax_tp_upload_hokhau', array($this, 'tp_upload_hokhau') );
        add_action( 'wp_ajax_nopriv_tp_upload_hokhau', array($this, 'tp_upload_hokhau') );
		
		add_action( 'wp_ajax_tp_upload_cap3', array($this, 'tp_upload_cap3') );
        add_action( 'wp_ajax_nopriv_tp_upload_cap3', array($this, 'tp_upload_cap3') );
		
		add_action( 'wp_ajax_tp_upload_hocba', array($this, 'tp_upload_hocba') );
        add_action( 'wp_ajax_nopriv_tp_upload_hocba', array($this, 'tp_upload_hocba') );
		
		add_action( 'wp_ajax_tp_upload_daihoc', array($this, 'tp_upload_daihoc') );
        add_action( 'wp_ajax_nopriv_tp_upload_daihoc', array($this, 'tp_upload_daihoc') );
		
		add_action( 'wp_ajax_tp_upload_xacnhan', array($this, 'tp_upload_xacnhan') );
        add_action( 'wp_ajax_nopriv_tp_upload_xacnhan', array($this, 'tp_upload_xacnhan') );
		
		add_action( 'wp_ajax_tp_upload_bangdiem', array($this, 'tp_upload_bangdiem') );
        add_action( 'wp_ajax_nopriv_tp_upload_bangdiem', array($this, 'tp_upload_bangdiem') );
		
		add_action( 'wp_ajax_tp_upload_chungnhan', array($this, 'tp_upload_chungnhan') );
        add_action( 'wp_ajax_nopriv_tp_upload_chungnhan', array($this, 'tp_upload_chungnhan') );
		
		add_action( 'wp_ajax_tp_upload_cmndbaolanh', array($this, 'tp_upload_cmndbaolanh') );
        add_action( 'wp_ajax_nopriv_tp_upload_cmndbaolanh', array($this, 'tp_upload_cmndbaolanh') );
		
		add_action( 'wp_ajax_tp_upload_taikhoan', array($this, 'tp_upload_taikhoan') );
        add_action( 'wp_ajax_nopriv_tp_upload_taikhoan', array($this, 'tp_upload_taikhoan') );
		
		add_action( 'wp_ajax_tp_upload_thunhap', array($this, 'tp_upload_thunhap') );
        add_action( 'wp_ajax_nopriv_tp_upload_thunhap', array($this, 'tp_upload_thunhap') );
		
		add_action( 'wp_ajax_tp_upload_giaykhac', array($this, 'tp_upload_giaykhac') );
        add_action( 'wp_ajax_nopriv_tp_upload_giaykhac', array($this, 'tp_upload_giaykhac') );

        /**
		* Đây là action đăng ký ajax delete upload
		*/
		add_action( 'wp_ajax_tp_delete_upload', array($this, 'tp_delete_upload') );
        add_action( 'wp_ajax_nopriv_tp_delete_upload', array($this, 'tp_delete_upload') );
	}

	/**
	* Đây là function thêm style
	*/
	public function tp_enqueue_styles(){
		wp_enqueue_style( 'tp-style-icon', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', array(), '0.1', 'all' );
		wp_enqueue_style( 'tp-style-uikit', TPU_CSS . 'uikit.docs.min.css', array(), '0.1', 'all' );
		wp_enqueue_style( 'tp-style-main', TPU_CSS . 'main.css', array(), '0.1', 'all' );
	}

	/**
	* Đây là function thêm script
	*/
	public function tp_enqueue_scripts(){
		wp_enqueue_script( 'tpu-script-uikit', TPU_JS . 'uikit.min.js', array( 'jquery' ), '0.1', false );
		wp_enqueue_script( 'tpu-script-upload', TPU_JS . 'upload.js', array( 'jquery' ), '0.1', false );
		wp_enqueue_script( 'tpu-script-main', TPU_JS . 'main.js', array( 'jquery' ), '0.1', true );
		wp_localize_script( 'tpu-script-main', 'tp_ajax_url', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	}

	/**
	* Đây là function shortcode
	*/
	public function tp_shortcode(){
		ob_start();
			?>
				<div id="upload-drop" class="uk-placeholder uk-text-center">
		            <i class="fa fa-cloud-upload uk-icon-medium uk-text-muted uk-margin-small-right"></i> Attach binaries by dropping them here or <a class="uk-form-file">selecting one<input id="upload-select" name="uploadfile" type="file"></a>.
		            <div id="progressbar" class="uk-progress uk-hidden">
			            <div class="uk-progress-bar" style="width: 0%;">0%</div>
			        </div>
			        <div id="upload-results" style="display:none;">
				    	<div class="uk-alert">
				    		<ul id="files" class="uk-list"><li class="clearfix" style="clear: both;"></li></ul>
				    	</div>
				    </div>
		        </div>
		        <?php $upload_nonce = wp_create_nonce( 'tp_upload_nonce' );?>
		        <script>

				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {

				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'uploadfile',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...

				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },

				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },

				            allcomplete: function(data) {

				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);

					            $("#upload-results").show();
	
								if( data.status == "1" ){

									$('#files .clearfix').before('<li class="success"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/><br />'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-delete="'+ data.id +'"></a></li>');

									new tp_remove_data_upload("#files .success .remove_img");//hàm xóa file upload

								} else{
									
									$("#upload-results .uk-alert").addClass('uk-alert-danger');
									$('#files .clearfix').before('<li class="error">'+ data.message +'</li>');
									
								}

				            }
				        };

				        var select = UIkit.uploadSelect($("#upload-select"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);
				            
				    })(jQuery);

				</script>
			<?php
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}


	/**
	* Đây là function để chạy action upload file ajax
	*/
	public function tp_upload(){

		$status = array('status'=> '0', 'name'=> '', 'name_tmp' => '', 'url' => '', 'id' => '', 'message'=> 'No file sent.');

		if( isset($_FILES['uploadfile']['name']) && !empty($_FILES['uploadfile']['name']) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'tp_upload_nonce' ) ){

			$mimes = array( 'image/jpeg', 'image/png', 'image/gif', 'image/jpg' );

			$attach_id = false;

			$file_name = basename( $_FILES['uploadfile']['name'] );
			
			$file_type = wp_check_filetype( $file_name );
			
			$file_renamed = mt_rand( 1000, 1000000 ) . '.' . $file_type['ext'];
			
			$upload = array(
				'name' => $file_renamed,
				'type' => $file_type['type'],
				'tmp_name' => $_FILES['uploadfile']['tmp_name'],
				'error' => $_FILES['uploadfile']['error'],
				'size' => $_FILES['uploadfile']['size']
			);

			if( in_array( $file_type['type'], $mimes) ){

				$file = wp_handle_upload( $upload, array( 'test_form' => false ) );

				if ( $file && !isset( $file['error'] ) ) {
					
					$status['status'] = '1';
					$status['name'] = $file_renamed;
					$status['name_tmp'] = $_FILES['uploadfile']['name'];
					$status['url'] = esc_url($file['url']);
					$status['file'] = $file['file'];
					$status['message'] = 'File is uploaded successfully.';

					if ( isset( $file['file'] ) ) {

						$file_loc = $file['file'];

						$attachment = array(
							'post_mime_type' => $file_type['type'],
							'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $file_name ) ),
							'post_content' => '',
							'post_status' => 'inherit',
							'guid' => esc_url($file['url'])
						);

						$post_id = 0;
						$attach_id = wp_insert_attachment( $attachment, $file_loc, $post_id );
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file_loc );
						wp_update_attachment_metadata( $attach_id, $attach_data );
					}

					if ( $attach_id ) {

						$status['id'] = $attach_id;

					}

				}else{

					$status['message'] = $file['error'];
				}

			}else{
			
				$status['message'] = 'Invalid file format.';
			}

		}

		echo json_encode($status);
		die();
	}
	public function tp_upload_khaisinh(){

		$status = array('status'=> '0', 'name'=> '', 'name_tmp' => '', 'url' => '', 'id' => '', 'message'=> 'No file sent.');

		if( isset($_FILES['khaisinh']['name']) && !empty($_FILES['khaisinh']['name']) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'tp_upload_nonce' ) ){

			$mimes = array( 'image/jpeg', 'image/png', 'image/gif', 'image/jpg' );

			$attach_id = false;

			$file_name = basename( $_FILES['khaisinh']['name'] );
			
			$file_type = wp_check_filetype( $file_name );
			
			$file_renamed = mt_rand( 1000, 1000000 ) . '.' . $file_type['ext'];
			
			$upload = array(
				'name' => $file_renamed,
				'type' => $file_type['type'],
				'tmp_name' => $_FILES['khaisinh']['tmp_name'],
				'error' => $_FILES['khaisinh']['error'],
				'size' => $_FILES['khaisinh']['size']
			);

			if( in_array( $file_type['type'], $mimes) ){

				$file = wp_handle_upload( $upload, array( 'test_form' => false ) );

				if ( $file && !isset( $file['error'] ) ) {
					
					$status['status'] = '2';
					$status['name'] = $file_renamed;
					$status['name_tmp'] = $_FILES['khaisinh']['name'];
					$status['url'] = esc_url($file['url']);
					$status['file'] = $file['file'];
					$status['message'] = 'File is uploaded successfully.';

					if ( isset( $file['file'] ) ) {

						$file_loc = $file['file'];

						$attachment = array(
							'post_mime_type' => $file_type['type'],
							'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $file_name ) ),
							'post_content' => '',
							'post_status' => 'inherit',
							'guid' => esc_url($file['url'])
						);

						$post_id = 0;
						$attach_id = wp_insert_attachment( $attachment, $file_loc, $post_id );
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file_loc );
						wp_update_attachment_metadata( $attach_id, $attach_data );
					}

					if ( $attach_id ) {

						$status['id'] = $attach_id;

					}

				}else{

					$status['message'] = $file['error'];
				}

			}else{
			
				$status['message'] = 'Invalid file format.';
			}

		}

		echo json_encode($status);
		die();
	}	
	
	public function tp_upload_cmnd(){

		$status = array('status'=> '0', 'name'=> '', 'name_tmp' => '', 'url' => '', 'id' => '', 'message'=> 'No file sent.');

		if( isset($_FILES['cmnd']['name']) && !empty($_FILES['cmnd']['name']) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'tp_upload_nonce' ) ){

			$mimes = array( 'image/jpeg', 'image/png', 'image/gif', 'image/jpg' );

			$attach_id = false;

			$file_name = basename( $_FILES['cmnd']['name'] );
			
			$file_type = wp_check_filetype( $file_name );
			
			$file_renamed = mt_rand( 1000, 1000000 ) . '.' . $file_type['ext'];
			
			$upload = array(
				'name' => $file_renamed,
				'type' => $file_type['type'],
				'tmp_name' => $_FILES['cmnd']['tmp_name'],
				'error' => $_FILES['cmnd']['error'],
				'size' => $_FILES['cmnd']['size']
			);

			if( in_array( $file_type['type'], $mimes) ){

				$file = wp_handle_upload( $upload, array( 'test_form' => false ) );

				if ( $file && !isset( $file['error'] ) ) {
					
					$status['status'] = '3';
					$status['name'] = $file_renamed;
					$status['name_tmp'] = $_FILES['cmnd']['name'];
					$status['url'] = esc_url($file['url']);
					$status['file'] = $file['file'];
					$status['message'] = 'File is uploaded successfully.';

					if ( isset( $file['file'] ) ) {

						$file_loc = $file['file'];

						$attachment = array(
							'post_mime_type' => $file_type['type'],
							'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $file_name ) ),
							'post_content' => '',
							'post_status' => 'inherit',
							'guid' => esc_url($file['url'])
						);

						$post_id = 0;
						$attach_id = wp_insert_attachment( $attachment, $file_loc, $post_id );
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file_loc );
						wp_update_attachment_metadata( $attach_id, $attach_data );
					}

					if ( $attach_id ) {

						$status['id'] = $attach_id;

					}

				}else{

					$status['message'] = $file['error'];
				}

			}else{
			
				$status['message'] = 'Invalid file format.';
			}

		}

		echo json_encode($status);
		die();
	}
	public function tp_upload_hokhau(){

		$status = array('status'=> '0', 'name'=> '', 'name_tmp' => '', 'url' => '', 'id' => '', 'message'=> 'No file sent.');

		if( isset($_FILES['hokhau']['name']) && !empty($_FILES['hokhau']['name']) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'tp_upload_nonce' ) ){

			$mimes = array( 'image/jpeg', 'image/png', 'image/gif', 'image/jpg' );

			$attach_id = false;

			$file_name = basename( $_FILES['hokhau']['name'] );
			
			$file_type = wp_check_filetype( $file_name );
			
			$file_renamed = mt_rand( 1000, 1000000 ) . '.' . $file_type['ext'];
			
			$upload = array(
				'name' => $file_renamed,
				'type' => $file_type['type'],
				'tmp_name' => $_FILES['hokhau']['tmp_name'],
				'error' => $_FILES['hokhau']['error'],
				'size' => $_FILES['hokhau']['size']
			);

			if( in_array( $file_type['type'], $mimes) ){

				$file = wp_handle_upload( $upload, array( 'test_form' => false ) );

				if ( $file && !isset( $file['error'] ) ) {
					
					$status['status'] = '4';
					$status['name'] = $file_renamed;
					$status['name_tmp'] = $_FILES['hokhau']['name'];
					$status['url'] = esc_url($file['url']);
					$status['file'] = $file['file'];
					$status['message'] = 'File is uploaded successfully.';

					if ( isset( $file['file'] ) ) {

						$file_loc = $file['file'];

						$attachment = array(
							'post_mime_type' => $file_type['type'],
							'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $file_name ) ),
							'post_content' => '',
							'post_status' => 'inherit',
							'guid' => esc_url($file['url'])
						);

						$post_id = 0;
						$attach_id = wp_insert_attachment( $attachment, $file_loc, $post_id );
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file_loc );
						wp_update_attachment_metadata( $attach_id, $attach_data );
					}

					if ( $attach_id ) {

						$status['id'] = $attach_id;

					}

				}else{

					$status['message'] = $file['error'];
				}

			}else{
			
				$status['message'] = 'Invalid file format.';
			}

		}

		echo json_encode($status);
		die();
	}
	
	public function tp_upload_cap3(){

		$status = array('status'=> '0', 'name'=> '', 'name_tmp' => '', 'url' => '', 'id' => '', 'message'=> 'No file sent.');

		if( isset($_FILES['cap3']['name']) && !empty($_FILES['cap3']['name']) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'tp_upload_nonce' ) ){

			$mimes = array( 'image/jpeg', 'image/png', 'image/gif', 'image/jpg' );

			$attach_id = false;

			$file_name = basename( $_FILES['cap3']['name'] );
			
			$file_type = wp_check_filetype( $file_name );
			
			$file_renamed = mt_rand( 1000, 1000000 ) . '.' . $file_type['ext'];
			
			$upload = array(
				'name' => $file_renamed,
				'type' => $file_type['type'],
				'tmp_name' => $_FILES['cap3']['tmp_name'],
				'error' => $_FILES['cap3']['error'],
				'size' => $_FILES['cap3']['size']
			);

			if( in_array( $file_type['type'], $mimes) ){

				$file = wp_handle_upload( $upload, array( 'test_form' => false ) );

				if ( $file && !isset( $file['error'] ) ) {
					
					$status['status'] = '5';
					$status['name'] = $file_renamed;
					$status['name_tmp'] = $_FILES['cap3']['name'];
					$status['url'] = esc_url($file['url']);
					$status['file'] = $file['file'];
					$status['message'] = 'File is uploaded successfully.';

					if ( isset( $file['file'] ) ) {

						$file_loc = $file['file'];

						$attachment = array(
							'post_mime_type' => $file_type['type'],
							'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $file_name ) ),
							'post_content' => '',
							'post_status' => 'inherit',
							'guid' => esc_url($file['url'])
						);

						$post_id = 0;
						$attach_id = wp_insert_attachment( $attachment, $file_loc, $post_id );
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file_loc );
						wp_update_attachment_metadata( $attach_id, $attach_data );
					}

					if ( $attach_id ) {

						$status['id'] = $attach_id;

					}

				}else{

					$status['message'] = $file['error'];
				}

			}else{
			
				$status['message'] = 'Invalid file format.';
			}

		}

		echo json_encode($status);
		die();
	}
	public function tp_upload_hocba(){

		$status = array('status'=> '0', 'name'=> '', 'name_tmp' => '', 'url' => '', 'id' => '', 'message'=> 'No file sent.');

		if( isset($_FILES['hocba']['name']) && !empty($_FILES['hocba']['name']) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'tp_upload_nonce' ) ){

			$mimes = array( 'image/jpeg', 'image/png', 'image/gif', 'image/jpg' );

			$attach_id = false;

			$file_name = basename( $_FILES['hocba']['name'] );
			
			$file_type = wp_check_filetype( $file_name );
			
			$file_renamed = mt_rand( 1000, 1000000 ) . '.' . $file_type['ext'];
			
			$upload = array(
				'name' => $file_renamed,
				'type' => $file_type['type'],
				'tmp_name' => $_FILES['hocba']['tmp_name'],
				'error' => $_FILES['hocba']['error'],
				'size' => $_FILES['hocba']['size']
			);

			if( in_array( $file_type['type'], $mimes) ){

				$file = wp_handle_upload( $upload, array( 'test_form' => false ) );

				if ( $file && !isset( $file['error'] ) ) {
					
					$status['status'] = '6';
					$status['name'] = $file_renamed;
					$status['name_tmp'] = $_FILES['hocba']['name'];
					$status['url'] = esc_url($file['url']);
					$status['file'] = $file['file'];
					$status['message'] = 'File is uploaded successfully.';

					if ( isset( $file['file'] ) ) {

						$file_loc = $file['file'];

						$attachment = array(
							'post_mime_type' => $file_type['type'],
							'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $file_name ) ),
							'post_content' => '',
							'post_status' => 'inherit',
							'guid' => esc_url($file['url'])
						);

						$post_id = 0;
						$attach_id = wp_insert_attachment( $attachment, $file_loc, $post_id );
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file_loc );
						wp_update_attachment_metadata( $attach_id, $attach_data );
					}

					if ( $attach_id ) {

						$status['id'] = $attach_id;

					}

				}else{

					$status['message'] = $file['error'];
				}

			}else{
			
				$status['message'] = 'Invalid file format.';
			}

		}

		echo json_encode($status);
		die();
	}
	public function tp_upload_daihoc(){

		$status = array('status'=> '0', 'name'=> '', 'name_tmp' => '', 'url' => '', 'id' => '', 'message'=> 'No file sent.');

		if( isset($_FILES['daihoc']['name']) && !empty($_FILES['daihoc']['name']) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'tp_upload_nonce' ) ){

			$mimes = array( 'image/jpeg', 'image/png', 'image/gif', 'image/jpg' );

			$attach_id = false;

			$file_name = basename( $_FILES['daihoc']['name'] );
			
			$file_type = wp_check_filetype( $file_name );
			
			$file_renamed = mt_rand( 1000, 1000000 ) . '.' . $file_type['ext'];
			
			$upload = array(
				'name' => $file_renamed,
				'type' => $file_type['type'],
				'tmp_name' => $_FILES['daihoc']['tmp_name'],
				'error' => $_FILES['daihoc']['error'],
				'size' => $_FILES['daihoc']['size']
			);

			if( in_array( $file_type['type'], $mimes) ){

				$file = wp_handle_upload( $upload, array( 'test_form' => false ) );

				if ( $file && !isset( $file['error'] ) ) {
					
					$status['status'] = '7';
					$status['name'] = $file_renamed;
					$status['name_tmp'] = $_FILES['daihoc']['name'];
					$status['url'] = esc_url($file['url']);
					$status['file'] = $file['file'];
					$status['message'] = 'File is uploaded successfully.';

					if ( isset( $file['file'] ) ) {

						$file_loc = $file['file'];

						$attachment = array(
							'post_mime_type' => $file_type['type'],
							'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $file_name ) ),
							'post_content' => '',
							'post_status' => 'inherit',
							'guid' => esc_url($file['url'])
						);

						$post_id = 0;
						$attach_id = wp_insert_attachment( $attachment, $file_loc, $post_id );
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file_loc );
						wp_update_attachment_metadata( $attach_id, $attach_data );
					}

					if ( $attach_id ) {

						$status['id'] = $attach_id;

					}

				}else{

					$status['message'] = $file['error'];
				}

			}else{
			
				$status['message'] = 'Invalid file format.';
			}

		}

		echo json_encode($status);
		die();
	}
	public function tp_upload_xacnhan(){

		$status = array('status'=> '0', 'name'=> '', 'name_tmp' => '', 'url' => '', 'id' => '', 'message'=> 'No file sent.');

		if( isset($_FILES['xacnhan']['name']) && !empty($_FILES['xacnhan']['name']) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'tp_upload_nonce' ) ){

			$mimes = array( 'image/jpeg', 'image/png', 'image/gif', 'image/jpg' );

			$attach_id = false;

			$file_name = basename( $_FILES['xacnhan']['name'] );
			
			$file_type = wp_check_filetype( $file_name );
			
			$file_renamed = mt_rand( 1000, 1000000 ) . '.' . $file_type['ext'];
			
			$upload = array(
				'name' => $file_renamed,
				'type' => $file_type['type'],
				'tmp_name' => $_FILES['xacnhan']['tmp_name'],
				'error' => $_FILES['xacnhan']['error'],
				'size' => $_FILES['xacnhan']['size']
			);

			if( in_array( $file_type['type'], $mimes) ){

				$file = wp_handle_upload( $upload, array( 'test_form' => false ) );

				if ( $file && !isset( $file['error'] ) ) {
					
					$status['status'] = '8';
					$status['name'] = $file_renamed;
					$status['name_tmp'] = $_FILES['xacnhan']['name'];
					$status['url'] = esc_url($file['url']);
					$status['file'] = $file['file'];
					$status['message'] = 'File is uploaded successfully.';

					if ( isset( $file['file'] ) ) {

						$file_loc = $file['file'];

						$attachment = array(
							'post_mime_type' => $file_type['type'],
							'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $file_name ) ),
							'post_content' => '',
							'post_status' => 'inherit',
							'guid' => esc_url($file['url'])
						);

						$post_id = 0;
						$attach_id = wp_insert_attachment( $attachment, $file_loc, $post_id );
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file_loc );
						wp_update_attachment_metadata( $attach_id, $attach_data );
					}

					if ( $attach_id ) {

						$status['id'] = $attach_id;

					}

				}else{

					$status['message'] = $file['error'];
				}

			}else{
			
				$status['message'] = 'Invalid file format.';
			}

		}

		echo json_encode($status);
		die();
	}
	public function tp_upload_bangdiem(){

		$status = array('status'=> '0', 'name'=> '', 'name_tmp' => '', 'url' => '', 'id' => '', 'message'=> 'No file sent.');

		if( isset($_FILES['bangdiem']['name']) && !empty($_FILES['bangdiem']['name']) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'tp_upload_nonce' ) ){

			$mimes = array( 'image/jpeg', 'image/png', 'image/gif', 'image/jpg' );

			$attach_id = false;

			$file_name = basename( $_FILES['bangdiem']['name'] );
			
			$file_type = wp_check_filetype( $file_name );
			
			$file_renamed = mt_rand( 1000, 1000000 ) . '.' . $file_type['ext'];
			
			$upload = array(
				'name' => $file_renamed,
				'type' => $file_type['type'],
				'tmp_name' => $_FILES['bangdiem']['tmp_name'],
				'error' => $_FILES['bangdiem']['error'],
				'size' => $_FILES['bangdiem']['size']
			);

			if( in_array( $file_type['type'], $mimes) ){

				$file = wp_handle_upload( $upload, array( 'test_form' => false ) );

				if ( $file && !isset( $file['error'] ) ) {
					
					$status['status'] = '9';
					$status['name'] = $file_renamed;
					$status['name_tmp'] = $_FILES['bangdiem']['name'];
					$status['url'] = esc_url($file['url']);
					$status['file'] = $file['file'];
					$status['message'] = 'File is uploaded successfully.';

					if ( isset( $file['file'] ) ) {

						$file_loc = $file['file'];

						$attachment = array(
							'post_mime_type' => $file_type['type'],
							'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $file_name ) ),
							'post_content' => '',
							'post_status' => 'inherit',
							'guid' => esc_url($file['url'])
						);

						$post_id = 0;
						$attach_id = wp_insert_attachment( $attachment, $file_loc, $post_id );
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file_loc );
						wp_update_attachment_metadata( $attach_id, $attach_data );
					}

					if ( $attach_id ) {

						$status['id'] = $attach_id;

					}

				}else{

					$status['message'] = $file['error'];
				}

			}else{
			
				$status['message'] = 'Invalid file format.';
			}

		}

		echo json_encode($status);
		die();
	}
	public function tp_upload_chungnhan(){

		$status = array('status'=> '0', 'name'=> '', 'name_tmp' => '', 'url' => '', 'id' => '', 'message'=> 'No file sent.');

		if( isset($_FILES['chungnhan']['name']) && !empty($_FILES['chungnhan']['name']) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'tp_upload_nonce' ) ){

			$mimes = array( 'image/jpeg', 'image/png', 'image/gif', 'image/jpg' );

			$attach_id = false;

			$file_name = basename( $_FILES['chungnhan']['name'] );
			
			$file_type = wp_check_filetype( $file_name );
			
			$file_renamed = mt_rand( 1000, 1000000 ) . '.' . $file_type['ext'];
			
			$upload = array(
				'name' => $file_renamed,
				'type' => $file_type['type'],
				'tmp_name' => $_FILES['chungnhan']['tmp_name'],
				'error' => $_FILES['chungnhan']['error'],
				'size' => $_FILES['chungnhan']['size']
			);

			if( in_array( $file_type['type'], $mimes) ){

				$file = wp_handle_upload( $upload, array( 'test_form' => false ) );

				if ( $file && !isset( $file['error'] ) ) {
					
					$status['status'] = '10';
					$status['name'] = $file_renamed;
					$status['name_tmp'] = $_FILES['chungnhan']['name'];
					$status['url'] = esc_url($file['url']);
					$status['file'] = $file['file'];
					$status['message'] = 'File is uploaded successfully.';

					if ( isset( $file['file'] ) ) {

						$file_loc = $file['file'];

						$attachment = array(
							'post_mime_type' => $file_type['type'],
							'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $file_name ) ),
							'post_content' => '',
							'post_status' => 'inherit',
							'guid' => esc_url($file['url'])
						);

						$post_id = 0;
						$attach_id = wp_insert_attachment( $attachment, $file_loc, $post_id );
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file_loc );
						wp_update_attachment_metadata( $attach_id, $attach_data );
					}

					if ( $attach_id ) {

						$status['id'] = $attach_id;

					}

				}else{

					$status['message'] = $file['error'];
				}

			}else{
			
				$status['message'] = 'Invalid file format.';
			}

		}

		echo json_encode($status);
		die();
	}
	public function tp_upload_cmndbaolanh(){

		$status = array('status'=> '0', 'name'=> '', 'name_tmp' => '', 'url' => '', 'id' => '', 'message'=> 'No file sent.');

		if( isset($_FILES['cmndbaolanh']['name']) && !empty($_FILES['cmndbaolanh']['name']) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'tp_upload_nonce' ) ){

			$mimes = array( 'image/jpeg', 'image/png', 'image/gif', 'image/jpg' );

			$attach_id = false;

			$file_name = basename( $_FILES['cmndbaolanh']['name'] );
			
			$file_type = wp_check_filetype( $file_name );
			
			$file_renamed = mt_rand( 1000, 1000000 ) . '.' . $file_type['ext'];
			
			$upload = array(
				'name' => $file_renamed,
				'type' => $file_type['type'],
				'tmp_name' => $_FILES['cmndbaolanh']['tmp_name'],
				'error' => $_FILES['cmndbaolanh']['error'],
				'size' => $_FILES['cmndbaolanh']['size']
			);

			if( in_array( $file_type['type'], $mimes) ){

				$file = wp_handle_upload( $upload, array( 'test_form' => false ) );

				if ( $file && !isset( $file['error'] ) ) {
					
					$status['status'] = '11';
					$status['name'] = $file_renamed;
					$status['name_tmp'] = $_FILES['cmndbaolanh']['name'];
					$status['url'] = esc_url($file['url']);
					$status['file'] = $file['file'];
					$status['message'] = 'File is uploaded successfully.';

					if ( isset( $file['file'] ) ) {

						$file_loc = $file['file'];

						$attachment = array(
							'post_mime_type' => $file_type['type'],
							'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $file_name ) ),
							'post_content' => '',
							'post_status' => 'inherit',
							'guid' => esc_url($file['url'])
						);

						$post_id = 0;
						$attach_id = wp_insert_attachment( $attachment, $file_loc, $post_id );
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file_loc );
						wp_update_attachment_metadata( $attach_id, $attach_data );
					}

					if ( $attach_id ) {

						$status['id'] = $attach_id;

					}

				}else{

					$status['message'] = $file['error'];
				}

			}else{
			
				$status['message'] = 'Invalid file format.';
			}

		}

		echo json_encode($status);
		die();
	}
	public function tp_upload_taikhoan(){

		$status = array('status'=> '0', 'name'=> '', 'name_tmp' => '', 'url' => '', 'id' => '', 'message'=> 'No file sent.');

		if( isset($_FILES['taikhoan']['name']) && !empty($_FILES['taikhoan']['name']) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'tp_upload_nonce' ) ){

			$mimes = array( 'image/jpeg', 'image/png', 'image/gif', 'image/jpg' );

			$attach_id = false;

			$file_name = basename( $_FILES['taikhoan']['name'] );
			
			$file_type = wp_check_filetype( $file_name );
			
			$file_renamed = mt_rand( 1000, 1000000 ) . '.' . $file_type['ext'];
			
			$upload = array(
				'name' => $file_renamed,
				'type' => $file_type['type'],
				'tmp_name' => $_FILES['taikhoan']['tmp_name'],
				'error' => $_FILES['taikhoan']['error'],
				'size' => $_FILES['taikhoan']['size']
			);

			if( in_array( $file_type['type'], $mimes) ){

				$file = wp_handle_upload( $upload, array( 'test_form' => false ) );

				if ( $file && !isset( $file['error'] ) ) {
					
					$status['status'] = '12';
					$status['name'] = $file_renamed;
					$status['name_tmp'] = $_FILES['taikhoan']['name'];
					$status['url'] = esc_url($file['url']);
					$status['file'] = $file['file'];
					$status['message'] = 'File is uploaded successfully.';

					if ( isset( $file['file'] ) ) {

						$file_loc = $file['file'];

						$attachment = array(
							'post_mime_type' => $file_type['type'],
							'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $file_name ) ),
							'post_content' => '',
							'post_status' => 'inherit',
							'guid' => esc_url($file['url'])
						);

						$post_id = 0;
						$attach_id = wp_insert_attachment( $attachment, $file_loc, $post_id );
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file_loc );
						wp_update_attachment_metadata( $attach_id, $attach_data );
					}

					if ( $attach_id ) {

						$status['id'] = $attach_id;

					}

				}else{

					$status['message'] = $file['error'];
				}

			}else{
			
				$status['message'] = 'Invalid file format.';
			}

		}

		echo json_encode($status);
		die();
	}
	public function tp_upload_thunhap(){

		$status = array('status'=> '0', 'name'=> '', 'name_tmp' => '', 'url' => '', 'id' => '', 'message'=> 'No file sent.');

		if( isset($_FILES['thunhap']['name']) && !empty($_FILES['thunhap']['name']) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'tp_upload_nonce' ) ){

			$mimes = array( 'image/jpeg', 'image/png', 'image/gif', 'image/jpg' );

			$attach_id = false;

			$file_name = basename( $_FILES['thunhap']['name'] );
			
			$file_type = wp_check_filetype( $file_name );
			
			$file_renamed = mt_rand( 1000, 1000000 ) . '.' . $file_type['ext'];
			
			$upload = array(
				'name' => $file_renamed,
				'type' => $file_type['type'],
				'tmp_name' => $_FILES['thunhap']['tmp_name'],
				'error' => $_FILES['thunhap']['error'],
				'size' => $_FILES['thunhap']['size']
			);

			if( in_array( $file_type['type'], $mimes) ){

				$file = wp_handle_upload( $upload, array( 'test_form' => false ) );

				if ( $file && !isset( $file['error'] ) ) {
					
					$status['status'] = '13';
					$status['name'] = $file_renamed;
					$status['name_tmp'] = $_FILES['thunhap']['name'];
					$status['url'] = esc_url($file['url']);
					$status['file'] = $file['file'];
					$status['message'] = 'File is uploaded successfully.';

					if ( isset( $file['file'] ) ) {

						$file_loc = $file['file'];

						$attachment = array(
							'post_mime_type' => $file_type['type'],
							'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $file_name ) ),
							'post_content' => '',
							'post_status' => 'inherit',
							'guid' => esc_url($file['url'])
						);

						$post_id = 0;
						$attach_id = wp_insert_attachment( $attachment, $file_loc, $post_id );
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file_loc );
						wp_update_attachment_metadata( $attach_id, $attach_data );
					}

					if ( $attach_id ) {

						$status['id'] = $attach_id;

					}

				}else{

					$status['message'] = $file['error'];
				}

			}else{
			
				$status['message'] = 'Invalid file format.';
			}

		}

		echo json_encode($status);
		die();
	}
	public function tp_upload_giaykhac(){

		$status = array('status'=> '0', 'name'=> '', 'name_tmp' => '', 'url' => '', 'id' => '', 'message'=> 'No file sent.');

		if( isset($_FILES['giaykhac']['name']) && !empty($_FILES['giaykhac']['name']) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'tp_upload_nonce' ) ){

			$mimes = array( 'image/jpeg', 'image/png', 'image/gif', 'image/jpg' );

			$attach_id = false;

			$file_name = basename( $_FILES['giaykhac']['name'] );
			
			$file_type = wp_check_filetype( $file_name );
			
			$file_renamed = mt_rand( 1000, 1000000 ) . '.' . $file_type['ext'];
			
			$upload = array(
				'name' => $file_renamed,
				'type' => $file_type['type'],
				'tmp_name' => $_FILES['giaykhac']['tmp_name'],
				'error' => $_FILES['giaykhac']['error'],
				'size' => $_FILES['giaykhac']['size']
			);

			if( in_array( $file_type['type'], $mimes) ){

				$file = wp_handle_upload( $upload, array( 'test_form' => false ) );

				if ( $file && !isset( $file['error'] ) ) {
					
					$status['status'] = '14';
					$status['name'] = $file_renamed;
					$status['name_tmp'] = $_FILES['giaykhac']['name'];
					$status['url'] = esc_url($file['url']);
					$status['file'] = $file['file'];
					$status['message'] = 'File is uploaded successfully.';

					if ( isset( $file['file'] ) ) {

						$file_loc = $file['file'];

						$attachment = array(
							'post_mime_type' => $file_type['type'],
							'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $file_name ) ),
							'post_content' => '',
							'post_status' => 'inherit',
							'guid' => esc_url($file['url'])
						);

						$post_id = 0;
						$attach_id = wp_insert_attachment( $attachment, $file_loc, $post_id );
						$attach_data = wp_generate_attachment_metadata( $attach_id, $file_loc );
						wp_update_attachment_metadata( $attach_id, $attach_data );
					}

					if ( $attach_id ) {

						$status['id'] = $attach_id;

					}

				}else{

					$status['message'] = $file['error'];
				}

			}else{
			
				$status['message'] = 'Invalid file format.';
			}

		}

		echo json_encode($status);
		die();
	}

	/**
	* Đây là function để chạy action delete file ajax
	*/
	public function tp_delete_upload(){

		$status = array('status'=> '0');

		if( isset($_REQUEST['attach_id']) && !empty($_REQUEST['attach_id']) ){

			wp_delete_attachment( $_REQUEST['attach_id'] );
		}

		echo json_encode($status);
		die();
	}

}

new TP_Upload_File();

?>