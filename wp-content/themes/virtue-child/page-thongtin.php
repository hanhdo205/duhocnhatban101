<?php
/*
Template Name: Giay to ban than hoc vien
*/
?>
<?php //if(is_user_logged_in()) { ?>
	<?php global $virtue, $post; 
		$map 				= get_post_meta( $post->ID, '_kad_contact_map', true ); 
		$form_math 			= get_post_meta( $post->ID, '_kad_contact_form_math', true );
		$contactformtitle 	= get_post_meta( $post->ID, '_kad_contact_form_title', true );
		$form 				= get_post_meta( $post->ID, '_kad_contact_form', true );
		//if ($form == 'yes') { ?>
			<script type="text/javascript">jQuery(document).ready(function ($) {$.extend($.validator.messages, {
			        required: "<?php echo __('This field is required.', 'kadencetoolkit'); ?>",
					email: "<?php echo __('Please enter a valid email address.', 'kadencetoolkit'); ?>",
				 });
				$("#contactForm").validate();
			});</script>
			<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.validate.js"></script>
		<?php //} 
		if ($map == 'yes') { ?>
		    <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
		    	<?php 	$address 	= get_post_meta( $post->ID, '_kad_contact_address', true ); 
						$maptype 	= get_post_meta( $post->ID, '_kad_contact_maptype', true ); 
						$height 	= get_post_meta( $post->ID, '_kad_contact_mapheight', true );
						$mapzoom 	= get_post_meta( $post->ID, '_kad_contact_zoom', true );
							
							if(!empty($height)) {
								$mapheight = $height;
							} else {
								$mapheight = 300;
							}
							if(!empty($mapzoom)) {
								$zoom = $mapzoom;
							} else {
								$zoom = 15;
							} ?>
		    	<script type="text/javascript">
					jQuery(window).load(function() {
					jQuery('#map_address').gmap3({
						map: {
						    address:"<?php echo $address;?>",
							options: {
			              		zoom:<?php echo $zoom;?>,
								draggable: true,
								mapTypeControl: true,
								mapTypeId: google.maps.MapTypeId.<?php echo esc_attr($maptype);?>,
								scrollwheel: false,
								panControl: true,
								rotateControl: false,
								scaleControl: true,
								streetViewControl: true,
								zoomControl: true
							}
						},
						marker:{
			            values:[
			            		 {address: "<?php echo $address;?>",
						 	    data:"<div class='mapinfo'>'<?php echo $address;?>'</div>",
						 	},
			            ],
			            options:{
			              draggable: false,
			            },
						events:{
			              click: function(marker, event, context){
			                var map = jQuery(this).gmap3("get"),
			                  infowindow = jQuery(this).gmap3({get:{name:"infowindow"}});
			                if (infowindow){
			                  infowindow.open(map, marker);
			                  infowindow.setContent(context.data);
			                } else {
			                  jQuery(this).gmap3({
			                    infowindow:{
			                      anchor:marker, 
			                      options:{content: context.data}
			                    }
			                  });
			                }
			              },
			              closeclick: function(){
			                var infowindow = jQuery(this).gmap3({get:{name:"infowindow"}});
			                if (infowindow){
			                  infowindow.close();
			                }
						  }
						}
			          }
			        });
			      });
			</script>
		    <?php echo '<style type="text/css" media="screen">#map_address {height:'.esc_attr($mapheight).'px; margin-bottom:20px;}</style>';
    }

		  $attachments = array();
		if(is_user_logged_in()) {
		$current_user = wp_get_current_user();
		$user_id = get_current_user_id();
		$user_email = $current_user->user_email;
		$user_hoten = get_user_meta( $user_id, 'user_ht', true );
		}
		else $user_id = 1;
		if(isset($_GET['id']))
			$numID = $_GET['id'];
		else $numID = mt_rand_str(8);
		if(isset($_GET['school']))
					$school = $_GET['school'];
		$row = $wpdb->get_row("SELECT * FROM `wp_hoso` WHERE numID = '$numID'", ARRAY_A);
			if(null !== $row) {
					$hinh34 = $row['hinh34'];
					$hinh34_duyet = $row['hinh34_duyet'];
					$khaisinh = $row['khaisinh'];
					$khaisinh_duyet = $row['khaisinh_duyet'];
					$hokhau = $row['hokhau'];
					$cmnd = $row['cmnd'];
					$cap3 = $row['cap3'];
					$hocba = $row['hocba'];
					$daihoc = $row['daihoc'];
					$bangdiem = $row['bangdiem'];
					$tiengnhat = $row['tiengnhat'];
					$congtac = $row['congtac'];
					$hokhau_duyet = $row['hokhau_duyet'];
					$cmnd_duyet = $row['cmnd_duyet'];
					$cap3_duyet = $row['cap3_duyet'];
					$hocba_duyet = $row['hocba_duyet'];
					$daihoc_duyet = $row['daihoc_duyet'];
					$bangdiem_duyet = $row['bangdiem_duyet'];
					$tiengnhat_duyet = $row['tiengnhat_duyet'];
					$congtac_duyet = $row['congtac_duyet'];
					$cmndbaolanh = $row['cmndbaolanh'];
					$taikhoan = $row['taikhoan'];
					$thunhap = $row['thunhap'];
					$view = $row['view'];				
			}
	if(isset($_POST['submitted'])) {
		if(isset($form_math) && $form_math == 'yes') {
			if(md5($_POST['kad_captcha']) != $_POST['hval']) {
				$kad_captchaError = __('Check your math.', 'kadencetoolkit');
				$hasError = true;
			}
		}
	/*if(trim($_POST['contactName']) === '') {
		$nameError = __('Please enter your name.', 'kadencetoolkit');
		$hasError = true;
	} else {
		$name = trim($_POST['contactName']);
	}

	if(trim($_POST['email']) === '')  {
		$emailError = __('Please enter your email address.', 'kadencetoolkit');
		$hasError = true;
	} else if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['email']))) {
		$emailError = __('You entered an invalid email address.', 'kadencetoolkit');
		$hasError = true;
	} else {
		$email = trim($_POST['email']);
	}*/
	
	if(trim($_POST['uploadfile']) === '') {
		if(!$hinh34){
			$hinh34Error = __('Bạn chưa upload file.', 'kadencetoolkit');
			$hasError = true;
		}
	} else {
		array_push($attachments, $_POST['uploadfile'] );
		$hinh34 = $_POST['store-url'];
		$hinh34_duyet='3';
	}
	
	if(trim($_POST['upload-khaisinh']) === '') {
		if(!$khaisinh){
			$khaisinhError = __('Bạn chưa upload file.', 'kadencetoolkit');
			$hasError = true;
		}
	} else {
		array_push($attachments, $_POST['upload-khaisinh'] );
		$khaisinh = $_POST['store-khaisinh-url'];
		$khaisinh_duyet='3';
	}
	
	if(trim($_POST['upload-hokhau']) === '') {
		if(!$hokhau){
			$hokhauError = __('Bạn chưa upload file.', 'kadencetoolkit');
			$hasError = true;
		}
	} else {
		array_push($attachments, $_POST['upload-hokhau'] );
		$hokhau = $_POST['store-hokhau-url'];
		$hokhau_duyet='3';
	}
	
	if(trim($_POST['upload-cmnd']) === '') {
		if(!$cmnd){
			$cmndError = __('Bạn chưa upload file.', 'kadencetoolkit');
			$hasError = true;
		}
	} else {
		array_push($attachments, $_POST['upload-cmnd'] );
		$cmnd = $_POST['store-cmnd-url'];
		$cmnd_duyet='3';
	}
	
	if(trim($_POST['upload-cap3']) === '') {
		if(!$cap3){
			$cap3Error = __('Bạn chưa upload file.', 'kadencetoolkit');
			$hasError = true;
		}
	} else {
		array_push($attachments, $_POST['upload-cap3'] );
		$cap3 = $_POST['store-cap3-url'];
		$cap3_duyet='3';
	}
	
	if(trim($_POST['upload-hocba']) === '') {
		if(!$hocba){
			$hocbaError = __('Bạn chưa upload file.', 'kadencetoolkit');
			$hasError = true;
		}
	} else {
		array_push($attachments, $_POST['upload-hocba'] );
		$hocba = $_POST['store-hocba-url'];
		$hocba_duyet='3';
	}
	
	if(trim($_POST['upload-daihoc']) === '') {
		//$daihocError = __('Bạn chưa upload file.', 'kadencetoolkit');
		//$hasError = true;
	} else {
		array_push($attachments, $_POST['upload-daihoc'] );
		$daihoc = $_POST['store-daihoc-url'];
		$daihoc_duyet='3';
	}
	
	if(trim($_POST['upload-xacnhan']) === '') {
		//$xacnhanError = __('Bạn chưa upload file.', 'kadencetoolkit'); ko can
		//$hasError = true; ko can
	} else {
		array_push($attachments, $_POST['upload-xacnhan'] );
		$congtac = $_POST['store-xacnhan-url'];
		$congtac_duyet='3';
	}
	
	if(trim($_POST['upload-bangdiem']) === '') {
		//$bangdiemError = __('Bạn chưa upload file.', 'kadencetoolkit');
		//$hasError = true;
	} else {
		array_push($attachments, $_POST['upload-bangdiem'] );
		$bangdiem = $_POST['store-bangdiem-url'];
		$bangdiem_duyet='3';
	}
	
	if(trim($_POST['upload-chungnhan']) === '') {
		//$chungnhanError = __('Bạn chưa upload file.', 'kadencetoolkit');
		//$hasError = true;
	} else {
		array_push($attachments, $_POST['upload-chungnhan'] );
		$tiengnhat = $_POST['store-chungnhan-url'];
		$tiengnhat_duyet='3';
	}

	/*if(trim($_POST['comments']) === '') {
		$commentError = __('Please enter a message.', 'kadencetoolkit');
		$hasError = true;
	} else {
		if(function_exists('stripslashes')) {
			$comments = stripslashes(trim($_POST['comments']));
		} else {
			$comments = trim($_POST['comments']);
		}
	}*/

	if(!isset($hasError)) {
		if (isset($virtue['contact_email'])) {
			$emailTo = $virtue['contact_email'];
		} else {
			$emailTo = get_option('admin_email');
		}
		$sitename = get_bloginfo('name');
		$subject = '['.esc_html($sitename) . ' ' . __("Contact", "kadencetoolkit").'] '. __("From", "kadencetoolkit") . ' ' . esc_html($name);
			
			if (null !== $row) {
						$query = $wpdb->update('wp_hoso', array('hinh34'=>$hinh34,'khaisinh'=>$khaisinh,'hokhau'=>$hokhau,'cmnd'=>$cmnd,'cap3'=>$cap3,'hocba'=>$hocba,'daihoc'=>$daihoc,'bangdiem'=>$bangdiem,'tiengnhat'=>$tiengnhat,'congtac'=>$congtac,'hinh34_duyet'=>$hinh34_duyet,'khaisinh_duyet'=>$khaisinh_duyet,'hokhau_duyet'=>$hokhau_duyet,'cmnd_duyet'=>$cmnd_duyet,'cap3_duyet'=>$cap3_duyet,'hocba_duyet'=>$hocba_duyet,'daihoc_duyet'=>$daihoc_duyet,'bangdiem_duyet'=>$bangdiem_duyet,'tiengnhat_duyet'=>$tiengnhat_duyet,'congtac_duyet'=>$congtac_duyet,'view'=>'0'), array('numID'=>$numID));						
					} else {
						$query = $wpdb->insert('wp_hoso',array('userID'=>$user_id,'numID'=>$numID,'hinh34'=>$hinh34,'khaisinh'=>$khaisinh,'hokhau'=>$hokhau,'cmnd'=>$cmnd,'cap3'=>$cap3,'hocba'=>$hocba,'daihoc'=>$daihoc,'bangdiem'=>$bangdiem,'tiengnhat'=>$tiengnhat,'congtac'=>$congtac,'hinh34_duyet'=>$hinh34_duyet,'khaisinh_duyet'=>$khaisinh_duyet,'hokhau_duyet'=>$hokhau_duyet,'cmnd_duyet'=>$cmnd_duyet,'cap3_duyet'=>$cap3_duyet,'hocba_duyet'=>$hocba_duyet,'daihoc_duyet'=>$daihoc_duyet,'bangdiem_duyet'=>$bangdiem_duyet,'tiengnhat_duyet'=>$tiengnhat_duyet,'congtac_duyet'=>$congtac_duyet));						
					}
				
				if($query){
					//wp_mail($emailTo, $subject, $body, $headers, $attachments);
					$emailSent = true;
				}

		
	}

} 
//} //if logged in 
?>
  	<div id="pageheader" class="titleclass">
		<div class="container">
			<?php //get_template_part('templates/page', 'header'); ?>
		</div><!--container-->
	</div><!--titleclass-->
	<?php if ($map == 'yes') { ?>
        <div id="mapheader" class="titleclass">
            <div class="container">
		        <div id="map_address">
	            </div>
	        </div><!--container-->
        </div><!--titleclass-->
  	<?php } ?>
	<div id="content" class="container">
   		<div class="row">
   		<?php if ($form == 'yes') { ?>
	  		<div id="main" class="main col-md-12" role="main"> 
	  	<?php } else { ?>
      		<div id="main" class="main col-md-12" role="main">
	      <?php } ?>
		  
	      <?php get_template_part('templates/content', 'page'); ?>
	      
			<div class="checkout-wrap">
				  <ul class="checkout-bar">
				  <?php if(get_user_role()=="ctv_role"){ ?>
					<li class="previous visited">					
					  <a href="../tao-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Thông tin học viên</a>					
					</li>    
					<li class="active"><a href="#">Giấy tờ bản thân học viên</a></li>   
					<li class="next">
					<?php if($hinh34&&$khaisinh&&$hokhau&&$cmnd&&$cap3&&$hocba)
					 { ?><a href="../ctv-giay-to-nguoi-bao-lanh/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Giấy tờ người bảo lãnh</a>
					 <?php } else { ?>Giấy tờ người bảo lãnh<?php } ?></li>    
					<li class="next">
					<?php if($hinh34&&$khaisinh&&$hokhau&&$cmnd&&$cap3&&$hocba&&$cmndbaolanh&&$taikhoan&&$thunhap)
					 { ?><a href="../ctv-nop-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Nộp hồ sơ</a>
					 <?php } else { ?>Nộp hồ sơ<?php } ?></li>    
					<li class="next"><?php if($view)
					 { ?><a href="../ctv-hoan-thien-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Hoàn thiện hồ sơ</a>
					 <?php } else { ?>Hoàn thiện hồ sơ<?php } ?></li>   
					 <?php } else { ?>
					 <li class="previous visited">					
					  <a href="../thong-tin-hoc-vien/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Thông tin học viên</a>					
					</li>    
					<li class="active"><a href="#">Giấy tờ bản thân học viên</a></li>   
					<li class="next">
					<?php if($hinh34&&$khaisinh&&$hokhau&&$cmnd&&$cap3&&$hocba)
					 { ?><a href="../giay-to-nguoi-bao-lanh/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Giấy tờ người bảo lãnh</a>
					 <?php } else { ?>Giấy tờ người bảo lãnh<?php } ?></li>    
					<li class="next">
					<?php if($hinh34&&$khaisinh&&$hokhau&&$cmnd&&$cap3&&$hocba&&$cmndbaolanh&&$taikhoan&&$thunhap)
					 { ?><a href="../nop-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Nộp hồ sơ</a>
					 <?php } else { ?>Nộp hồ sơ<?php } ?></li>    
					<li class="next"><?php if($view)
					 { ?><a href="../hoan-thien-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Hoàn thiện hồ sơ</a>
					 <?php } else { ?>Hoàn thiện hồ sơ<?php } ?></li> 
					 <?php } ?>
				  </ul>
			</div>
      		<?php //if ($form == 'yes') { ?>
      		<div class="contactformcase col-md-9">
      			<?php if (!empty($contactformtitle)) { 
      					echo '<h3>'. esc_html($contactformtitle).'</h3>';
      					} 
      					if(isset($emailSent) && $emailSent == true) { ?>
							<div class="thanks">
								<div class="form-caption">
									<div class="form-title">
										<h1 class="head-title">Giấy tờ bản thân học viên</h1>
									</div>
									<p><?php _e('Bạn đã cập nhật thành công bản sao giấy tờ cá nhân, chúng tôi sẽ kiểm tra ngay.', 'kadencetoolkit');?></p>
									<p><?php _e('Để tiếp tục, bạn vui lòng chuyển qua bước 3 "Giấy tờ người bảo lãnh"', 'kadencetoolkit');?></p>
									<hr>
									<p align="center">
									<?php if(get_user_role()=="ctv_role"){ ?>
									<a href="../ctv-giay-to-nguoi-bao-lanh/?id=<?php echo $numID;?>&school=<?php echo $school;?>" class="kad-btn kad-btn-primary" >Chuyển qua bước 3</a>
									<?php } else { ?>
									<a href="../giay-to-nguoi-bao-lanh/?id=<?php echo $numID;?>&school=<?php echo $school;?>" class="kad-btn kad-btn-primary" >Chuyển qua bước 3</a>
									<?php }?>
									</p>
								</div>
							</div>
						<?php } else {
							
							if(isset($hasError) || isset($captchaError)) { ?>
								<p class="error"><?php _e('Sorry, an error occured.', 'kadencetoolkit');?><p>
							<?php } ?>
							<div class="form-caption">
								<div class="form-title">
									<h1 class="head-title">Giấy tờ bản thân học viên</h1>
								</div>
								<?php //if(is_user_logged_in()) { ?>
								<form action="<?php the_permalink(); ?>?id=<?php echo $numID;?>&school=<?php echo $school;?>" id="contactForm" method="post" enctype="multipart/form-data">
								<div class="contactform">
									
									<!--<p>
										<label for="contactName"><?php _e('Họ và tên:', 'kadencetoolkit'); ?></label>
										<?php if(isset($nameError)) { ?>
											<span class="error"><?php echo esc_html($nameError);?></span>
										<?php } ?>
										<input type="text" name="contactName" id="contactName" value="<?php if(isset($_POST['contactName'])) echo esc_attr($_POST['contactName']); else if(is_user_logged_in()) echo $user_hoten;?>" class="required requiredField full" />
									</p>
									<hr>
									<p>
										<label for="email"><?php _e('Địa chỉ email:', 'kadencetoolkit'); ?></label>
											<?php if(isset($emailError)) { ?>
												<span class="error"><?php echo esc_html($emailError);?></span>
											<?php } ?>
										<input type="text" name="email" id="email" value="<?php if(isset($_POST['email'])) echo esc_attr($_POST['email']); else if(is_user_logged_in()) echo $user_email;?>" class="required requiredField email full" />
									</p>-->
									
									<p>
										<label for="file"><?php _e('Hình 3x4:', 'kadencetoolkit'); ?> <?php if($hinh34_duyet==1) {?>(<a href="<?php echo $hinh34;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="hinh34" title="Hình của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/check_ok.png"></a>) <?php } else if($hinh34_duyet==2) {?>(<a href="<?php echo $hinh34;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="hinh34" title="Hình của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bad.png"></a>) <?php } else if($hinh34_duyet==3){?>(<a href="<?php echo $hinh34;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="hinh34" title="Hình của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tick_octagon.png"></a>) <?php }?></label>
											<?php if(isset($hinh34Error)) { ?>
												<span class="error"><?php echo esc_html($hinh34Error);?></span>
											<?php } ?>
										<?php if(trim($_POST['uploadfile'])!= '') { ?>
										<span id="upload-results">
											<span class="uk-alert">
												<span id="files" class="uk-list"><span class="success"><a href="<?php echo esc_attr($_POST['store-url']);?>" data-rel="lightbox" class="lightboxhover" data-lightbox="hinh34" title="Hình 3x4"><img src="<?php echo esc_attr($_POST['store-url']);?>" alt="" data-name="<?php echo esc_attr($_POST['store-tmp']);?>" style="max-width:35px;"></a><?php echo esc_attr($_POST['store-tmp']);?></span><span class="clearfix"></span></span>
											</span>
										</span>
										<input type="hidden" class="store-tmp" name="store-tmp" value="<?php if(isset($_POST['store-tmp'])) echo esc_attr($_POST['store-tmp']);?>" >
										<input type="hidden" class="store-url" name="store-url" value="<?php if(isset($_POST['store-url'])) echo esc_attr($_POST['store-url']);?>" >
										<input type="hidden" class="store" name="uploadfile" value="<?php echo esc_attr($_POST['uploadfile']);?>" >
										<?php } else {?>
										<input type="file" id="upload-hinh34" name="uploadfile" class="full">
										<input type="hidden" class="store-tmp" name="store-tmp" value="<?php if(isset($_POST['store-tmp'])) echo esc_attr($_POST['store-tmp']);?>" >
										<input type="hidden" class="store-url" name="store-url" value="<?php if(isset($_POST['store-url'])) echo esc_attr($_POST['store-url']);?>" >
										<input type="hidden" class="store" name="uploadfile" value="<?php if(isset($_POST['uploadfile'])) echo esc_attr($_POST['uploadfile']);?>" >
										<span id="progressbar" class="uk-progress uk-hidden">
											<span class="full uk-progress-bar" style="width: 0%;">0%</span>
										</span>
										<span id="upload-results" style="display:none;">
											<span class="uk-alert">
												<span id="files" class="uk-list"><span class="clearfix"></span></span>
											</span>
										</span>
										<?php } ?>
									</p>
									
									<hr>
									<p>
										<label for="file"><?php _e('Khai sinh:', 'kadencetoolkit'); ?> <?php if($khaisinh_duyet==1) {?>(<a href="<?php echo $khaisinh;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="khaisinh" title="Khai sinh của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/check_ok.png"></a>) <?php } else if($khaisinh_duyet==2) {?>(<a href="<?php echo $khaisinh;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="khaisinh" title="Khai sinh của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bad.png"></a>) <?php } else if($khaisinh_duyet==3) {?>(<a href="<?php echo $khaisinh;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="khaisinh" title="Khai sinh của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tick_octagon.png"></a>) <?php } else {?> (<a href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sample/khaisinh.jpg" data-rel="lightbox" class="lightboxhover" data-lightbox="khaisinhmau" title="Khai sinh mẫu">xem mẫu</a>)<?php }?></label>
											<?php if(isset($khaisinhError)) { ?>
												<span class="error"><?php echo esc_html($khaisinhError);?></span>
											<?php } ?>
										<?php if(trim($_POST['upload-khaisinh'])!= '') { ?>
										<span id="upload-results-khaisinh">
											<span class="uk-alert">
												<span id="files-khaisinh" class="uk-list"><span class="success-khaisinh"><a href="<?php echo esc_attr($_POST['store-khaisinh-url']);?>" data-rel="lightbox" class="lightboxhover" data-lightbox="khaisinh" title="Khai sinh"><img src="<?php echo esc_attr($_POST['store-khaisinh-url']);?>" alt="" data-name="<?php echo esc_attr($_POST['store-khaisinh-tmp']);?>" style="max-width:35px;"></a><?php echo esc_attr($_POST['store-khaisinh-tmp']);?></span><span class="clearfix"></span></span>
											</span>
										</span>
										<input type="hidden" class="store-khaisinh-tmp" name="store-khaisinh-tmp" value="<?php if(isset($_POST['store-khaisinh-tmp'])) echo esc_attr($_POST['store-khaisinh-tmp']);?>" >
										<input type="hidden" class="store-khaisinh-url" name="store-khaisinh-url" value="<?php if(isset($_POST['store-khaisinh-url'])) echo esc_attr($_POST['store-khaisinh-url']);?>" >
										<input type="hidden" class="store-khaisinh" name="upload-khaisinh" value="<?php echo esc_attr($_POST['upload-khaisinh']);?>" >
										<?php } else {?>	
										<input type="file" id="upload-khaisinh" name="khaisinh" class="full">
										<input type="hidden" class="store-khaisinh-tmp" name="store-khaisinh-tmp" value="<?php if(isset($_POST['store-khaisinh-tmp'])) echo esc_attr($_POST['store-khaisinh-tmp']);?>" >
										<input type="hidden" class="store-khaisinh-url" name="store-khaisinh-url" value="<?php if(isset($_POST['store-khaisinh-url'])) echo esc_attr($_POST['store-khaisinh-url']);?>" >
										<input type="hidden" class="store-khaisinh" name="upload-khaisinh" value="<?php if(isset($_POST['upload-khaisinh'])) echo esc_attr($_POST['upload-khaisinh']);?>" >
										<span id="progressbar-khaisinh" class="uk-progress uk-hidden">
											<span class="full uk-progress-bar" style="width: 0%;">0%</span>
										</span>
										<span id="upload-results-khaisinh" style="display:none;">
											<span class="uk-alert">
												<span id="files-khaisinh" class="uk-list"><span class="clearfix"></span></span>
											</span>
										</span>
										<?php } ?>
									</p>
									<hr>
									
										<label for="file"><?php _e('Hộ khẩu:', 'kadencetoolkit'); ?> <?php if($hokhau_duyet==1) {?>(<a href="<?php echo $hokhau;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="hokhau" title="Hộ khẩu của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/check_ok.png"></a>) <?php } else if($hokhau_duyet==2) {?>(<a href="<?php echo $hokhau;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="hokhau" title="Hộ khẩu của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bad.png"></a>) <?php } else if($hokhau_duyet==3) {?>(<a href="<?php echo $hokhau;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="hokhau" title="Hộ khẩu của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tick_octagon.png"></a>) <?php } else {?> (<div id="kad-wp-gallery-hk" class="kad-wp-gallery kad-light-wp-gallery kt-gallery-column-3"><div class="grid_item kad_gallery_fade_in gallery_item"><a href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sample/hokhau_bia.jpg" data-rel="lightbox" class="lightboxhover" data-lightbox="hokhaumau[]" title="Hộ khẩu mẫu">xem mẫu</a></div>)<div class="grid_item kad_gallery_fade_in gallery_item"><a style="display:none;" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sample/hokhau_1.jpg" data-rel="lightbox" class="lightboxhover" data-lightbox="hokhaumau[]" title="Hộ khẩu mẫu trang 1">1</a></div><div class="grid_item kad_gallery_fade_in gallery_item"><a style="display:none;" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sample/hokhau_2.jpg" data-rel="lightbox" class="lightboxhover" data-lightbox="hokhaumau[]" title="Hộ khẩu mẫu trang 2">2</a></div><div class="grid_item kad_gallery_fade_in gallery_item"><a style="display:none;" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sample/hokhau_3.jpg" data-rel="lightbox" class="lightboxhover" data-lightbox="hokhaumau[]" title="Hộ khẩu mẫu trang 3">3</a></div></div><?php }?></label>
											<?php if(isset($hokhauError)) { ?>
												<span class="error"><?php echo esc_html($hokhauError);?></span>
											<?php } ?>
										<?php if(trim($_POST['upload-hokhau'])!= '') { ?>
										<span id="upload-results-hokhau">
											<span class="uk-alert">
												<span id="files-hokhau" class="uk-list"><span class="success-hokhau"><a href="<?php echo esc_attr($_POST['store-hokhau-url']);?>" data-rel="lightbox" class="lightboxhover" data-lightbox="hokhau" title="Hộ khẩu"><img src="<?php echo esc_attr($_POST['store-hokhau-url']);?>" alt="" data-name="<?php echo esc_attr($_POST['store-hokhau-tmp']);?>" style="max-width:35px;"></a><?php echo esc_attr($_POST['store-hokhau-tmp']);?></span><span class="clearfix"></span></span>
											</span>
										</span>
										<input type="hidden" class="store-hokhau-tmp" name="store-hokhau-tmp" value="<?php if(isset($_POST['store-hokhau-tmp'])) echo esc_attr($_POST['store-hokhau-tmp']);?>" >
										<input type="hidden" class="store-hokhau-url" name="store-hokhau-url" value="<?php if(isset($_POST['store-hokhau-url'])) echo esc_attr($_POST['store-hokhau-url']);?>" >
										<input type="hidden" class="store-hokhau" name="upload-hokhau" value="<?php if(isset($_POST['upload-hokhau'])) echo esc_attr($_POST['upload-hokhau']);?>" >
										<?php } else {?>
										<input type="file" id="upload-hokhau" name="hokhau" class="full">
										<input type="hidden" class="store-hokhau-tmp" name="store-hokhau-tmp" value="<?php if(isset($_POST['store-hokhau-tmp'])) echo esc_attr($_POST['store-hokhau-tmp']);?>" >
										<input type="hidden" class="store-hokhau-url" name="store-hokhau-url" value="<?php if(isset($_POST['store-hokhau-url'])) echo esc_attr($_POST['store-hokhau-url']);?>" >
										<input type="hidden" class="store-hokhau" name="upload-hokhau" value="<?php if(isset($_POST['upload-hokhau'])) echo esc_attr($_POST['upload-hokhau']);?>" >
										<span id="progressbar-hokhau" class="uk-progress uk-hidden">
											<span class="full uk-progress-bar" style="width: 0%;">0%</span>
										</span>
										<span id="upload-results-hokhau" style="display:none;">
											<span class="uk-alert">
												<span id="files-hokhau" class="uk-list"><span class="clearfix"></span></span>
											</span>
										</span>
										<?php } ?>
									
									<hr>
									<p>
										<label for="file"><?php _e('CMND:', 'kadencetoolkit'); ?> <?php if($cmnd_duyet==1) {?>(<a href="<?php echo $cmnd;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="cmnd" title="Chứng minh nhân dân của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/check_ok.png"></a>) <?php } else if($cmnd_duyet==2) {?>(<a href="<?php echo $cmnd;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="cmnd" title="Chứng minh nhân dân của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bad.png"></a>) <?php } else if($cmnd_duyet==3) {?>(<a href="<?php echo $cmnd;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="cmnd" title="Chứng minh nhân dân của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tick_octagon.png"></a>) <?php } else{?> (<a href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sample/cmnd.jpg" data-rel="lightbox" class="lightboxhover" data-lightbox="cmndmau" title="Chứng minh nhân dân mẫu">xem mẫu</a>)<?php }?></label>
											<?php if(isset($cmndError)) { ?>
												<span class="error"><?php echo esc_html($cmndError);?></span>
											<?php } ?>
										<?php if(trim($_POST['upload-cmnd'])!= '') { ?>
										<span id="upload-results-cmnd">
											<span class="uk-alert">
												<span id="files-cmnd" class="uk-list"><span class="success-cmnd"><a href="<?php echo esc_attr($_POST['store-cmnd-url']);?>" data-rel="lightbox" class="lightboxhover" data-lightbox="cmnd" title="Chứng minh nhân dân"><img src="<?php echo esc_attr($_POST['store-cmnd-url']);?>" alt="" data-name="<?php echo esc_attr($_POST['store-cmnd-tmp']);?>" style="max-width:35px;"></a><?php echo esc_attr($_POST['store-cmnd-tmp']);?></span><span class="clearfix"></span></span>
											</span>
										</span>
										<input type="hidden" class="store-cmnd-tmp" name="store-cmnd-tmp" value="<?php if(isset($_POST['store-cmnd-tmp'])) echo esc_attr($_POST['store-cmnd-tmp']);?>" >
										<input type="hidden" class="store-cmnd-url" name="store-cmnd-url" value="<?php if(isset($_POST['store-cmnd-url'])) echo esc_attr($_POST['store-cmnd-url']);?>" >
										<input type="hidden" class="store-cmnd" name="upload-cmnd" value="<?php if(isset($_POST['upload-cmnd'])) echo esc_attr($_POST['upload-cmnd']);?>" >
										<?php } else {?>
										<input type="file" id="upload-cmnd" name="cmnd" class="full">
										<input type="hidden" class="store-cmnd-tmp" name="store-cmnd-tmp" value="<?php if(isset($_POST['store-cmnd-tmp'])) echo esc_attr($_POST['store-cmnd-tmp']);?>" >
										<input type="hidden" class="store-cmnd-url" name="store-cmnd-url" value="<?php if(isset($_POST['store-cmnd-url'])) echo esc_attr($_POST['store-cmnd-url']);?>" >
										<input type="hidden" class="store-cmnd" name="upload-cmnd" value="<?php if(isset($_POST['upload-cmnd'])) echo esc_attr($_POST['upload-cmnd']);?>" >
										<span id="progressbar-cmnd" class="uk-progress uk-hidden">
											<span class="full uk-progress-bar" style="width: 0%;">0%</span>
										</span>
										<span id="upload-results-cmnd" style="display:none;">
											<span class="uk-alert">
												<span id="files-cmnd" class="uk-list"><span class="clearfix"></span></span>
											</span>
										</span>
										<?php } ?>
									</p>									

									<div class="form-title">
										<div class="head-title"><?php _e('Các loại bằng cấp', 'kadencetoolkit'); ?></div>
									</div>									

									<p>
										<label for="file"><?php _e('Bằng tốt nghiệp cấp 3:', 'kadencetoolkit'); ?> <?php if($cap3_duyet==1) {?>(<a href="<?php echo $cap3;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="cap3" title="Bằng tốt nghiệp cấp 3 của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/check_ok.png"></a>) <?php } else if($cap3_duyet==2) {?>(<a href="<?php echo $cap3;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="cap3" title="Bằng tốt nghiệp cấp 3 của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bad.png"></a>) <?php }else if($cap3_duyet==3) {?>(<a href="<?php echo $cap3;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="cap3" title="Bằng tốt nghiệp cấp 3 của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tick_octagon.png"></a>) <?php }?></label>
											<?php if(isset($cap3Error)) { ?>
												<span class="error"><?php echo esc_html($cap3Error);?></span>
											<?php } ?>
										<?php if(trim($_POST['upload-cap3'])!= '') { ?>
										<span id="upload-results-cap3">
											<span class="uk-alert">
												<span id="files-cap3" class="uk-list"><span class="success-cap3"><a href="<?php echo esc_attr($_POST['store-cap3-url']);?>" data-rel="lightbox" class="lightboxhover" data-lightbox="cap3" title="Bằng tốt nghiệp cấp 3"><img src="<?php echo esc_attr($_POST['store-cap3-url']);?>" alt="" data-name="<?php echo esc_attr($_POST['store-cap3-tmp']);?>" style="max-width:35px;"></a><?php echo esc_attr($_POST['store-cap3-tmp']);?></span><span class="clearfix"></span></span>
											</span>
										</span>
										<input type="hidden" class="store-cap3-tmp" name="store-cap3-tmp" value="<?php if(isset($_POST['store-cap3-tmp'])) echo esc_attr($_POST['store-cap3-tmp']);?>" >
										<input type="hidden" class="store-cap3-url" name="store-cap3-url" value="<?php if(isset($_POST['store-cap3-url'])) echo esc_attr($_POST['store-cap3-url']);?>" >
										<input type="hidden" class="store-cap3" name="upload-cap3" value="<?php if(isset($_POST['upload-cap3'])) echo esc_attr($_POST['upload-cap3']);?>" >
										<?php } else {?>
										<input type="file" id="upload-cap3" name="cap3" class="full">
										<input type="hidden" class="store-cap3-tmp" name="store-cap3-tmp" value="<?php if(isset($_POST['store-cap3-tmp'])) echo esc_attr($_POST['store-cap3-tmp']);?>" >
										<input type="hidden" class="store-cap3-url" name="store-cap3-url" value="<?php if(isset($_POST['store-cap3-url'])) echo esc_attr($_POST['store-cap3-url']);?>" >
										<input type="hidden" class="store-cap3" name="upload-cap3" value="<?php if(isset($_POST['upload-cap3'])) echo esc_attr($_POST['upload-cap3']);?>" >
										<span id="progressbar-cap3" class="uk-progress uk-hidden">
											<span class="full uk-progress-bar" style="width: 0%;">0%</span>
										</span>
										<span id="upload-results-cap3" style="display:none;">
											<span class="uk-alert">
												<span id="files-cap3" class="uk-list"><span class="clearfix"></span></span>
											</span>
										</span>
										<?php } ?>
									</p>
									<hr>
									<p>
										<label for="file"><?php _e('Học bạ cấp 3:', 'kadencetoolkit'); ?> <?php if($hocba_duyet==1) {?>(<a href="<?php echo $hocba;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="hocba" title="Học bạ cấp 3 của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/check_ok.png"></a>) <?php } else if($hocba_duyet==2) {?>(<a href="<?php echo $hocba;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="hocba" title="Học bạ cấp 3 của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bad.png"></a>) <?php } else if($hocba_duyet==3) {?>(<a href="<?php echo $hocba;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="hocba" title="Học bạ cấp 3 của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tick_octagon.png"></a>) <?php }?></label>
											<?php if(isset($hocbaError)) { ?>
												<span class="error"><?php echo esc_html($hocbaError);?></span>
											<?php } ?>
										<?php if(trim($_POST['upload-hocba'])!= '') { ?>
										<span id="upload-results-hocba">
											<span class="uk-alert">
												<span id="files-hocba" class="uk-list"><span class="success-hocba"><a href="<?php echo esc_attr($_POST['store-hocba-url']);?>" data-rel="lightbox" class="lightboxhover" data-lightbox="hocba" title="Học bạn cấp 3"><img src="<?php echo esc_attr($_POST['store-hocba-url']);?>" alt="" data-name="<?php echo esc_attr($_POST['store-hocba-tmp']);?>" style="max-width:35px;"></a><?php echo esc_attr($_POST['store-hocba-tmp']);?></span><span class="clearfix"></span></span>
											</span>
										</span>
										<input type="hidden" class="store-hocba-tmp" name="store-hocba-tmp" value="<?php if(isset($_POST['store-hocba-tmp'])) echo esc_attr($_POST['store-hocba-tmp']);?>" >
										<input type="hidden" class="store-hocba-url" name="store-hocba-url" value="<?php if(isset($_POST['store-hocba-url'])) echo esc_attr($_POST['store-hocba-url']);?>" >
										<input type="hidden" class="store-hocba" name="upload-hocba" value="<?php if(isset($_POST['upload-hocba'])) echo esc_attr($_POST['upload-hocba']);?>" >
										<?php } else {?>
										<input type="file" id="upload-hocba" name="hocba" class="full">
										<input type="hidden" class="store-hocba-tmp" name="store-hocba-tmp" value="<?php if(isset($_POST['store-hocba-tmp'])) echo esc_attr($_POST['store-hocba-tmp']);?>" >
										<input type="hidden" class="store-hocba-url" name="store-hocba-url" value="<?php if(isset($_POST['store-hocba-url'])) echo esc_attr($_POST['store-hocba-url']);?>" >
										<input type="hidden" class="store-hocba" name="upload-hocba" value="<?php if(isset($_POST['upload-hocba'])) echo esc_attr($_POST['upload-hocba']);?>" >
										<span id="progressbar-hocba" class="uk-progress uk-hidden">
											<span class="full uk-progress-bar" style="width: 0%;">0%</span>
										</span>
										<span id="upload-results-hocba" style="display:none;">
											<span class="uk-alert">
												<span id="files-hocba" class="uk-list"><span class="clearfix"></span></span>
											</span>
										</span>
										<?php } ?>
									</p>
									<hr>
									
										<label for="file"><?php _e('Bằng ĐH - CĐ TC:', 'kadencetoolkit'); ?> <?php if($daihoc_duyet==1) {?>(<a href="<?php echo $daihoc;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="daihoc" title="Bằng cấp của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/check_ok.png"></a>) <?php } else if($daihoc_duyet==2) {?>(<a href="<?php echo $daihoc;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="daihoc" title="Bằng cấp của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bad.png"></a>) <?php } else if($daihoc_duyet==3) {?>(<a href="<?php echo $daihoc;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="daihoc" title="Bằng cấp của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tick_octagon.png"></a>) <?php } else {?>(<div id="kad-wp-gallery-dh" class="kad-wp-gallery kad-light-wp-gallery kt-gallery-column-3"><div class="grid_item kad_gallery_fade_in gallery_item"><a href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sample/bangcunhan_bia.jpg" data-rel="lightbox" class="lightboxhover" data-lightbox="bangdhmau[]" title="Bằng ĐH - CĐ TC tờ bìa mẫu">xem mẫu</a>)</div><div class="grid_item kad_gallery_fade_in gallery_item"><a style="display:none;" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sample/bangcunhan_ruot.jpg" data-rel="lightbox" class="lightboxhover" data-lightbox="bangdhmau[]" title="Bằng ĐH - CĐ TC tờ ruột mẫu">xem mẫu</a></div></div><?php }?></label>
											<?php if(isset($daihocError)) { ?>
												<span class="error"><?php echo esc_html($daihocError);?></span>
											<?php } ?>
										<?php if(trim($_POST['upload-daihoc'])!= '') { ?>
										<span id="upload-results-daihoc">
											<span class="uk-alert">
												<span id="files-daihoc" class="uk-list"><span class="success-daihoc"><a href="<?php echo esc_attr($_POST['store-daihoc-url']);?>" data-rel="lightbox" class="lightboxhover" data-lightbox="daihoc" title="Bằng ĐH - CĐ - TC"><img src="<?php echo esc_attr($_POST['store-daihoc-url']);?>" alt="" data-name="<?php echo esc_attr($_POST['store-daihoc-tmp']);?>" style="max-width:35px;"></a><?php echo esc_attr($_POST['store-daihoc-tmp']);?></span><span class="clearfix"></span></span>
											</span>
										</span>
										<input type="hidden" class="store-daihoc-tmp" name="store-daihoc-tmp" value="<?php if(isset($_POST['store-daihoc-tmp'])) echo esc_attr($_POST['store-daihoc-tmp']);?>" >
										<input type="hidden" class="store-daihoc-url" name="store-daihoc-url" value="<?php if(isset($_POST['store-daihoc-url'])) echo esc_attr($_POST['store-daihoc-url']);?>" >
										<input type="hidden" class="store-daihoc" name="upload-daihoc" value="<?php if(isset($_POST['upload-daihoc'])) echo esc_attr($_POST['upload-daihoc']);?>" >
										<?php } else {?>
										<input type="file" id="upload-daihoc" name="daihoc" class="full">
										<input type="hidden" class="store-daihoc-tmp" name="store-daihoc-tmp" value="<?php if(isset($_POST['store-daihoc-tmp'])) echo esc_attr($_POST['store-daihoc-tmp']);?>" >
										<input type="hidden" class="store-daihoc-url" name="store-daihoc-url" value="<?php if(isset($_POST['store-daihoc-url'])) echo esc_attr($_POST['store-daihoc-url']);?>" >
										<input type="hidden" class="store-daihoc" name="upload-daihoc" value="<?php if(isset($_POST['upload-daihoc'])) echo esc_attr($_POST['upload-daihoc']);?>" >
										<span id="progressbar-daihoc" class="uk-progress uk-hidden">
											<span class="full uk-progress-bar" style="width: 0%;">0%</span>
										</span>
										<span id="upload-results-daihoc" style="display:none;">
											<span class="uk-alert">
												<span id="files-daihoc" class="uk-list"><span class="clearfix"></span></span>
											</span>
										</span>
										<?php } ?>
										<em>Trường hợp đang theo học đại học hoặc cao đẳng thì thay bằng giấy xác nhận đang theo học tại trường</em>
									
									<hr>
									
									<p>
										<label for="file"><?php _e('Bảng điểm:', 'kadencetoolkit'); ?> <?php if($bangdiem_duyet==1) {?>(<a href="<?php echo $bangdiem;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="bangdiem" title="Bảng điểm của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/check_ok.png"></a>) <?php } else if($bangdiem_duyet==2) {?>(<a href="<?php echo $bangdiem;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="bangdiem" title="Bảng điểm của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bad.png"></a>) <?php } else if($bangdiem_duyet==3) {?>(<a href="<?php echo $bangdiem;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="bangdiem" title="Bảng điểm của bạn"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tick_octagon.png"></a>) <?php } else {?>(<a href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sample/bangdiem.jpg" data-rel="lightbox" class="lightboxhover" data-lightbox="bangdiemmau" title="Bảng điểm mẫu">xem mẫu</a>)<?php }?></label>
											<?php if(isset($bangdiemError)) { ?>
												<span class="error"><?php echo esc_html($bangdiemError);?></span>
											<?php } ?>
										<?php if(trim($_POST['upload-bangdiem'])!= '') { ?>
										<span id="upload-results-bangdiem">
											<span class="uk-alert">
												<span id="files-bangdiem" class="uk-list"><span class="success-bangdiem"><a href="<?php echo esc_attr($_POST['store-bangdiem-url']);?>" data-rel="lightbox" class="lightboxhover" data-lightbox="bangdiem" title="Bảng điểm"><img src="<?php echo esc_attr($_POST['store-bangdiem-url']);?>" alt="" data-name="<?php echo esc_attr($_POST['store-bangdiem-tmp']);?>" style="max-width:35px;"></a><?php echo esc_attr($_POST['store-bangdiem-tmp']);?></span><span class="clearfix"></span></span>
											</span>
										</span>
										<input type="hidden" class="store-bangdiem-tmp" name="store-bangdiem-tmp" value="<?php if(isset($_POST['store-bangdiem-tmp'])) echo esc_attr($_POST['store-bangdiem-tmp']);?>" >
										<input type="hidden" class="store-bangdiem-url" name="store-bangdiem-url" value="<?php if(isset($_POST['store-bangdiem-url'])) echo esc_attr($_POST['store-bangdiem-url']);?>" >
										<input type="hidden" class="store-bangdiem" name="upload-bangdiem" value="<?php if(isset($_POST['upload-bangdiem'])) echo esc_attr($_POST['upload-bangdiem']);?>" >
										<?php } else {?>
										<input type="file" id="upload-bangdiem" name="bangdiem" class="full">
										<input type="hidden" class="store-bangdiem-tmp" name="store-bangdiem-tmp" value="<?php if(isset($_POST['store-bangdiem-tmp'])) echo esc_attr($_POST['store-bangdiem-tmp']);?>" >
										<input type="hidden" class="store-bangdiem-url" name="store-bangdiem-url" value="<?php if(isset($_POST['store-bangdiem-url'])) echo esc_attr($_POST['store-bangdiem-url']);?>" >
										<input type="hidden" class="store-bangdiem" name="upload-bangdiem" value="<?php if(isset($_POST['upload-bangdiem'])) echo esc_attr($_POST['upload-bangdiem']);?>" >
										<span id="progressbar-bangdiem" class="uk-progress uk-hidden">
											<span class="full uk-progress-bar" style="width: 0%;">0%</span>
										</span>
										<span id="upload-results-bangdiem" style="display:none;">
											<span class="uk-alert">
												<span id="files-bangdiem" class="uk-list"><span class="clearfix"></span></span>
											</span>
										</span>
										<?php } ?>
									</p>
									<div class="form-title">
										<div class="head-title"><?php _e('Giấy tờ khác', 'kadencetoolkit'); ?></div>
									</div>
									
									<p>
										<label for="file"><?php _e('Giấy chứng nhận tiếng Nhật:', 'kadencetoolkit'); ?><?php if($tiengnhat_duyet==1) {?> (<a href="<?php echo $tiengnhat;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="tiengnhat" title="Giấy chứng nhận học tiếng Nhật"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/check_ok.png"></a>) <?php } else if($tiengnhat_duyet==2) {?>(<a href="<?php echo $tiengnhat;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="tiengnhat" title="Giấy chứng nhận học tiếng Nhật"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bad.png"></a>) <?php } else if($tiengnhat_duyet==3) {?> (<a href="<?php echo $tiengnhat;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="tiengnhat" title="Giấy chứng nhận học tiếng Nhật"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tick_octagon.png"></a>) <?php }?></label>
											<?php if(isset($chungnhanError)) { ?>
												<span class="error"><?php echo esc_html($chungnhanError);?></span>
											<?php } ?>
										<?php if(trim($_POST['upload-chungnhan'])!= '') { ?>
										<span id="upload-results-chungnhan">
											<span class="uk-alert">
												<span id="files-chungnhan" class="uk-list"><span class="success-chungnhan"><a href="<?php echo esc_attr($_POST['store-chungnhan-url']);?>" data-rel="lightbox" class="lightboxhover" data-lightbox="chungnhan" title="Giấy chứng nhận học tiếng Nhật"><img src="<?php echo esc_attr($_POST['store-chungnhan-url']);?>" alt="" data-name="<?php echo esc_attr($_POST['store-chungnhan-tmp']);?>" style="max-width:35px;"></a><?php echo esc_attr($_POST['store-chungnhan-tmp']);?></span><span class="clearfix"></span></span>
											</span>
										</span>
										<input type="hidden" class="store-chungnhan-tmp" name="store-chungnhan-tmp" value="<?php if(isset($_POST['store-chungnhan-tmp'])) echo esc_attr($_POST['store-chungnhan-tmp']);?>" >
										<input type="hidden" class="store-chungnhan-url" name="store-chungnhan-url" value="<?php if(isset($_POST['store-chungnhan-url'])) echo esc_attr($_POST['store-chungnhan-url']);?>" >
										<input type="hidden" class="store-chungnhan" name="upload-chungnhan" value="<?php if(isset($_POST['upload-chungnhan'])) echo esc_attr($_POST['upload-chungnhan']);?>" >
										<?php } else {?>
										<input type="file" id="upload-chungnhan" name="chungnhan" class="full">
										<input type="hidden" class="store-chungnhan-tmp" name="store-chungnhan-tmp" value="<?php if(isset($_POST['store-chungnhan-tmp'])) echo esc_attr($_POST['store-chungnhan-tmp']);?>" >
										<input type="hidden" class="store-chungnhan-url" name="store-chungnhan-url" value="<?php if(isset($_POST['store-chungnhan-url'])) echo esc_attr($_POST['store-chungnhan-url']);?>" >
										<input type="hidden" class="store-chungnhan" name="upload-chungnhan" value="<?php if(isset($_POST['upload-chungnhan'])) echo esc_attr($_POST['upload-chungnhan']);?>" >
										<span id="progressbar-chungnhan" class="uk-progress uk-hidden">
											<span class="full uk-progress-bar" style="width: 0%;">0%</span>
										</span>
										<span id="upload-results-chungnhan" style="display:none;">
											<span class="uk-alert">
												<span id="files-chungnhan" class="uk-list"><span class="clearfix"></span></span>
											</span>
										</span>
										<?php } ?>
									</p>
									<hr>																		
									
									<p>
										<label for="file"><?php _e('Giấy xác nhận công tác:', 'kadencetoolkit'); ?> <?php if($congtac_duyet==1) {?>(<a href="<?php echo $congtac;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="congtac" title="Giấy xác nhận công tác"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/check_ok.png"></a>) <?php } else if($congtac_duyet==2) {?>(<a href="<?php echo $congtac;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="congtac" title="Giấy xác nhận công tác"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bad.png"></a>) <?php } else if($congtac_duyet==1) {?>(<a href="<?php echo $congtac;?>" data-rel="lightbox" class="lightboxhover" data-lightbox="congtac" title="Giấy xác nhận công tác"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tick_octagon.png"></a>) <?php } else {?>(<a href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sample/xacnhancongtac.jpg" data-rel="lightbox" class="lightboxhover" data-lightbox="xacnhancongtac" title="Giấy xác nhận công tác mẫu">xem mẫu</a>)<?php }?></label>
											<?php if(isset($xacnhanError)) { ?>
												<span class="error"><?php echo esc_html($xacnhanError);?></span>
											<?php } ?>
										<?php if(trim($_POST['upload-xacnhan'])!= '') { ?>
										<span id="upload-results-xacnhan">
											<span class="uk-alert">
												<span id="files-xacnhan" class="uk-list"><span class="success-xacnhan"><a href="<?php echo esc_attr($_POST['store-xacnhan-url']);?>" data-rel="lightbox" class="lightboxhover" data-lightbox="xacnhan" title="Giấy xác nhận"><img src="<?php echo esc_attr($_POST['store-xacnhan-url']);?>" alt="" data-name="<?php echo esc_attr($_POST['store-xacnhan-tmp']);?>" style="max-width:35px;"></a><?php echo esc_attr($_POST['store-xacnhan-tmp']);?></span><span class="clearfix"></span></span>
											</span>
										</span>
										<input type="hidden" class="store-xacnhan-tmp" name="store-xacnhan-tmp" value="<?php if(isset($_POST['store-xacnhan-tmp'])) echo esc_attr($_POST['store-xacnhan-tmp']);?>" >
										<input type="hidden" class="store-xacnhan-url" name="store-xacnhan-url" value="<?php if(isset($_POST['store-xacnhan-url'])) echo esc_attr($_POST['store-xacnhan-url']);?>" >
										<input type="hidden" class="store-xacnhan" name="upload-xacnhan" value="<?php if(isset($_POST['upload-xacnhan'])) echo esc_attr($_POST['upload-xacnhan']);?>" >
										<?php } else {?>
										<input type="file" id="upload-xacnhan" name="xacnhan" class="full">
										<input type="hidden" class="store-xacnhan-tmp" name="store-xacnhan-tmp" value="<?php if(isset($_POST['store-xacnhan-tmp'])) echo esc_attr($_POST['store-xacnhan-tmp']);?>" >
										<input type="hidden" class="store-xacnhan-url" name="store-xacnhan-url" value="<?php if(isset($_POST['store-xacnhan-url'])) echo esc_attr($_POST['store-xacnhan-url']);?>" >
										<input type="hidden" class="store-xacnhan" name="upload-xacnhan" value="<?php if(isset($_POST['upload-xacnhan'])) echo esc_attr($_POST['upload-xacnhan']);?>" >
										<span id="progressbar-xacnhan" class="uk-progress uk-hidden">
											<span class="full uk-progress-bar" style="width: 0%;">0%</span>
										</span>
										<span id="upload-results-xacnhan" style="display:none;">
											<span class="uk-alert">
												<span id="files-xacnhan" class="uk-list"><span class="clearfix"></span></span>
											</span>
										</span>
										<?php } ?>
									</p>
									<hr>
									
									<?php if(isset($form_math) && $form_math == 'yes') {
										$one = rand(5, 50);
										$two = rand(1, 9);
										$result = md5($one + $two); ?>
										<p>
											<label for="kad_captcha"><?php echo $one.' + '.$two; ?> = </label>
											<input type="text" name="kad_captcha" id="kad_captcha" class="kad_captcha kad-quarter" />
												<?php if(isset($kad_captchaError)) { ?>
													<label class="error"><?php echo esc_html($kad_captchaError);?></label>
												<?php } ?>
											<input type="hidden" name="hval" id="hval" value="<?php echo esc_attr($result);?>" />
										</p>
									<?php } ?>
									<p align="center">
										<input type="submit" class="kad-btn kad-btn-primary" id="submit" value="<?php _e('Upload giấy tờ', 'kadencetoolkit'); ?>"></input>
									</p>
								</div><!-- /.contactform-->
								<input type="hidden" name="submitted" id="submitted" value="true" />
							</form>
							<?php //} //if logged in
							//else { echo "<hr>Bạn chưa đăng nhập"; } ?>
							</div>
						<?php } ?>
      		</div>
			<!--<div class="free-signup widget-free-signup col-md-3">
				<div class="textwidget"><div class="texttitle">TỰ LÀM HỒ SƠ DU HỌC MIỄN PHÍ LÀ GÌ?</div>
				<span style="font-size:13px;font-style:italic;color:#555;">Là chương trình giúp các bạn có thể <strong>tự làm hồ sơ du học Nhật Bản</strong> thông qua sự hướng dẫn từ website và senpai <strong>mà không phải mất chi phí tư vấn cho công ty môi giới du học</strong><hr>
				<strong>TẠI SAO LẠI LÀ MIỄN PHÍ?</strong> Giúp học sinh và gia đình đỡ đi một chút gánh nặng<hr>
				<strong>CHƯƠNG TRÌNH HOẠT ĐỘNG NHỜ NGUỒN KINH PHÍ NÀO?</strong> Chương trình hoạt động với sự tài trợ kinh phí từ các trường ở Nhật Bản</span></div>
			</div>--><!--contactform-->
      		<?php //} ?>
			<?php $upload_nonce = wp_create_nonce( 'tp_upload_nonce' );?>
		        <script>

				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {

				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'uploadfile',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...

				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },

				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },

				            allcomplete: function(data) {

				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);

					            $("#upload-results").show();
	
								if( data.status == "1" ){

									$('#files .clearfix').before('<span class="success"><a href="'+ data.url +'" data-rel="lightbox" class="lightboxhover" data-lightbox="hinh34" title="Hình 3x4"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success" data-delete="'+ data.id +'"></a></span>');
									$(".store").val(data.file);
									$(".store-tmp").val(data.name_tmp);
									$(".store-url").val(data.url);
									$("#upload-hinh34").hide();
									new tp_remove_data_upload("#files .success .remove_img");//hàm xóa file upload

								} else {
									
									$("#upload-results .uk-alert").addClass('uk-alert-danger');
									$('#files .clearfix').before('<span class="error">'+ data.message +'</span>');
									
								}
								
				            }
				        };

				        var select = UIkit.uploadSelect($("#upload-hinh34"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);
				            
				    })(jQuery);

				</script>
				
				 <script>

				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar-khaisinh"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {

				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload_khaisinh&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'khaisinh',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...

				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },

				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },

				            allcomplete: function(data) {

				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);

					            $("#upload-results-khaisinh").show();						
								
								if( data.status == "2" ){

									$('#files-khaisinh .clearfix').before('<span class="success-khaisinh"><a href="'+ data.url +'" data-rel="lightbox" class="lightboxhover" data-lightbox="khaisinh" title="Giấy khai sinh"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success-khaisinh" data-delete="'+ data.id +'"></a></span>');
									$(".store-khaisinh").val(data.file);
									$(".store-khaisinh-tmp").val(data.name_tmp);
									$(".store-khaisinh-url").val(data.url);
									$("#upload-khaisinh").hide();
									new tp_remove_data_upload("#files-khaisinh .success-khaisinh .remove_img");//hàm xóa file upload

								} else {
									
									$("#upload-results-khaisinh .uk-alert").addClass('uk-alert-danger');
									$('#files-khaisinh .clearfix').before('<span class="error">'+ data.message +'</span>');
									
								}

				            }
				        };

				        var select = UIkit.uploadSelect($("#upload-khaisinh"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);
				            
				    })(jQuery);

				</script>
				<script>

				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar-cmnd"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {

				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload_cmnd&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'cmnd',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...

				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },

				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },

				            allcomplete: function(data) {

				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);

					            $("#upload-results-cmnd").show();						
								
								if( data.status == "3" ){

									$('#files-cmnd .clearfix').before('<span class="success-cmnd"><a href="'+ data.url +'" data-rel="lightbox" class="lightboxhover" data-lightbox="cmnd" title="Chứng minh nhân dân"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success-cmnd" data-delete="'+ data.id +'"></a></span>');
									$(".store-cmnd").val(data.file);
									$(".store-cmnd-tmp").val(data.name_tmp);
									$(".store-cmnd-url").val(data.url);
									$("#upload-cmnd").hide();
									new tp_remove_data_upload("#files-cmnd .success-cmnd .remove_img");//hàm xóa file upload

								} else {
									
									$("#upload-results-cmnd .uk-alert").addClass('uk-alert-danger');
									$('#files-khaisinh .clearfix').before('<span class="error">'+ data.message +'</span>');
									
								}

				            }
				        };

				        var select = UIkit.uploadSelect($("#upload-cmnd"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);
				            
				    })(jQuery);

				</script>
				<script>
				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar-hokhau"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {
				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload_hokhau&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'hokhau',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...
				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },
				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },
				            allcomplete: function(data) {
				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);
					            $("#upload-results-hokhau").show();														
								if( data.status == "4" ){
									$('#files-hokhau .clearfix').before('<span class="success-hokhau"><a href="'+ data.url +'" data-rel="lightbox" class="lightboxhover" data-lightbox="hokhau" title="Hộ khẩu"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success-hokhau" data-delete="'+ data.id +'"></a></span>');
									$(".store-hokhau").val(data.file);
									$(".store-hokhau-tmp").val(data.name_tmp);
									$(".store-hokhau-url").val(data.url);
									$("#upload-hokhau").hide();
									new tp_remove_data_upload("#files-hokhau .success-hokhau .remove_img");//hàm xóa file upload
								} else {									
									$("#upload-results-hokhau .uk-alert").addClass('uk-alert-danger');
									$('#files-khaisinh .clearfix').before('<span class="error">'+ data.message +'</span>');									
								}
				            }
				        };
				        var select = UIkit.uploadSelect($("#upload-hokhau"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);				            
				    })(jQuery);
				</script>
				<script>
				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar-cap3"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {
				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload_cap3&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'cap3',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...
				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },
				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },
				            allcomplete: function(data) {
				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);
					            $("#upload-results-cap3").show();														
								if( data.status == "5" ){
									$('#files-cap3 .clearfix').before('<span class="success-cap3"><a href="'+ data.url +'" data-rel="lightbox" class="lightboxhover" data-lightbox="cap3" title="Bằng tốt nghiệp cấp 3"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success-cap3" data-delete="'+ data.id +'"></a></span>');
									$(".store-cap3").val(data.file);
									$(".store-cap3-tmp").val(data.name_tmp);
									$(".store-cap3-url").val(data.url);
									$("#upload-cap3").hide();
									new tp_remove_data_upload("#files-cap3 .success-cap3 .remove_img");//hàm xóa file upload
								} else {									
									$("#upload-results-cap3 .uk-alert").addClass('uk-alert-danger');
									$('#files-khaisinh .clearfix').before('<span class="error">'+ data.message +'</span>');									
								}
				            }
				        };
				        var select = UIkit.uploadSelect($("#upload-cap3"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);				            
				    })(jQuery);
				</script>
				<script>
				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar-hocba"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {
				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload_hocba&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'hocba',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...
				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },
				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },
				            allcomplete: function(data) {
				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);
					            $("#upload-results-hocba").show();														
								if( data.status == "6" ){
									$('#files-hocba .clearfix').before('<span class="success-hocba"><a href="'+ data.url +'" data-rel="lightbox" class="lightboxhover" data-lightbox="hocba" title="Học bạ cấp 3"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success-hocba" data-delete="'+ data.id +'"></a></span>');
									$(".store-hocba").val(data.file);
									$(".store-hocba-tmp").val(data.name_tmp);
									$(".store-hocba-url").val(data.url);
									$("#upload-hocba").hide();
									new tp_remove_data_upload("#files-hocba .success-hocba .remove_img");//hàm xóa file upload
								} else {									
									$("#upload-results-hocba .uk-alert").addClass('uk-alert-danger');
									$('#files-khaisinh .clearfix').before('<span class="error">'+ data.message +'</span>');									
								}
				            }
				        };
				        var select = UIkit.uploadSelect($("#upload-hocba"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);				            
				    })(jQuery);
				</script>
				<script>
				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar-daihoc"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {
				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload_daihoc&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'daihoc',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...
				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },
				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },
				            allcomplete: function(data) {
				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);
					            $("#upload-results-daihoc").show();														
								if( data.status == "7" ){
									$('#files-daihoc .clearfix').before('<span class="success-daihoc"><a href="'+ data.url +'" data-rel="lightbox" class="lightboxhover" data-lightbox="daihoc" title="Bằng ĐH - CĐ - TC"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success-daihoc" data-delete="'+ data.id +'"></a></span>');
									$(".store-daihoc").val(data.file);
									$(".store-daihoc-tmp").val(data.name_tmp);
									$(".store-daihoc-url").val(data.url);
									$("#upload-daihoc").hide();
									new tp_remove_data_upload("#files-daihoc .success-daihoc .remove_img");//hàm xóa file upload
								} else {									
									$("#upload-results-daihoc .uk-alert").addClass('uk-alert-danger');
									$('#files-khaisinh .clearfix').before('<span class="error">'+ data.message +'</span>');									
								}
				            }
				        };
				        var select = UIkit.uploadSelect($("#upload-daihoc"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);				            
				    })(jQuery);
				</script>
				<script>
				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar-xacnhan"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {
				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload_xacnhan&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'xacnhan',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...
				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },
				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },
				            allcomplete: function(data) {
				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);
					            $("#upload-results-xacnhan").show();														
								if( data.status == "8" ){
									$('#files-xacnhan .clearfix').before('<span class="success-xacnhan"><a href="'+ data.url +'" data-rel="lightbox" class="lightboxhover" data-lightbox="xacnhan" title="Giấy xác nhận"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success-xacnhan" data-delete="'+ data.id +'"></a></span>');
									$(".store-xacnhan").val(data.file);
									$(".store-xacnhan-tmp").val(data.name_tmp);
									$(".store-xacnhan-url").val(data.url);
									$("#upload-xacnhan").hide();
									new tp_remove_data_upload("#files-xacnhan .success-xacnhan .remove_img");//hàm xóa file upload
								} else {									
									$("#upload-results-xacnhan .uk-alert").addClass('uk-alert-danger');
									$('#files-khaisinh .clearfix').before('<span class="error">'+ data.message +'</span>');									
								}
				            }
				        };
				        var select = UIkit.uploadSelect($("#upload-xacnhan"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);				            
				    })(jQuery);
				</script>
				<script>
				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar-bangdiem"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {
				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload_bangdiem&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'bangdiem',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...
				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },
				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },
				            allcomplete: function(data) {
				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);
					            $("#upload-results-bangdiem").show();														
								if( data.status == "9" ){
									$('#files-bangdiem .clearfix').before('<span class="success-bangdiem"><a href="'+ data.url +'" data-rel="lightbox" class="lightboxhover" data-lightbox="bangdiem" title="Bảng điểm"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success-bangdiem" data-delete="'+ data.id +'"></a></span>');
									$(".store-bangdiem").val(data.file);
									$(".store-bangdiem-tmp").val(data.name_tmp);
									$(".store-bangdiem-url").val(data.url);
									$("#upload-bangdiem").hide();
									new tp_remove_data_upload("#files-bangdiem .success-bangdiem .remove_img");//hàm xóa file upload
								} else {									
									$("#upload-results-bangdiem .uk-alert").addClass('uk-alert-danger');
									$('#files-khaisinh .clearfix').before('<span class="error">'+ data.message +'</span>');									
								}
				            }
				        };
				        var select = UIkit.uploadSelect($("#upload-bangdiem"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);				            
				    })(jQuery);
				</script>
				<script>
				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar-chungnhan"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {
				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload_chungnhan&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'chungnhan',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...
				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },
				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },
				            allcomplete: function(data) {
				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);
					            $("#upload-results-chungnhan").show();														
								if( data.status == "10" ){
									$('#files-chungnhan .clearfix').before('<span class="success-chungnhan"><a href="'+ data.url +'" data-rel="lightbox" class="lightboxhover" data-lightbox="chungnhan" title="Giấy chứng nhận học tiếng Nhật"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success-chungnhan" data-delete="'+ data.id +'"></a></span>');
									$(".store-chungnhan").val(data.file);
									$(".store-chungnhan-tmp").val(data.name_tmp);
									$(".store-chungnhan-url").val(data.url);
									$("#upload-chungnhan").hide();
									new tp_remove_data_upload("#files-chungnhan .success-chungnhan .remove_img");//hàm xóa file upload
								} else {									
									$("#upload-results-chungnhan .uk-alert").addClass('uk-alert-danger');
									$('#files-khaisinh .clearfix').before('<span class="error">'+ data.message +'</span>');									
								}
				            }
				        };
				        var select = UIkit.uploadSelect($("#upload-chungnhan"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);				            
				    })(jQuery);
				</script>
				<script>
				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar-cmndbaolanh"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {
				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload_cmndbaolanh&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'cmndbaolanh',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...
				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },
				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },
				            allcomplete: function(data) {
				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);
					            $("#upload-results-cmndbaolanh").show();														
								if( data.status == "11" ){
									$('#files-cmndbaolanh .clearfix').before('<span class="success-cmndbaolanh"><a href="'+ data.url +'" data-rel="lightbox" class="lightboxhover" data-lightbox="cmndbaolanh" title="CMND người bảo lãnh"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success-cmndbaolanh" data-delete="'+ data.id +'"></a></span>');
									$(".store-cmndbaolanh").val(data.file);
									$(".store-cmndbaolanh-tmp").val(data.name_tmp);
									$(".store-cmndbaolanh-url").val(data.url);
									$("#upload-cmndbaolanh").hide();
									new tp_remove_data_upload("#files-cmndbaolanh .success-cmndbaolanh .remove_img");//hàm xóa file upload
								} else {									
									$("#upload-results-cmndbaolanh .uk-alert").addClass('uk-alert-danger');
									$('#files-khaisinh .clearfix').before('<span class="error">'+ data.message +'</span>');									
								}
				            }
				        };
				        var select = UIkit.uploadSelect($("#upload-cmndbaolanh"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);				            
				    })(jQuery);
				</script>
				<script>
				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar-taikhoan"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {
				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload_taikhoan&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'taikhoan',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...
				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },
				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },
				            allcomplete: function(data) {
				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);
					            $("#upload-results-taikhoan").show();														
								if( data.status == "12" ){
									$('#files-taikhoan .clearfix').before('<span class="success-taikhoan"><a href="'+ data.url +'" data-rel="lightbox" class="lightboxhover" data-lightbox="taikhoan" title="Xác nhận tài khoản ngân hàng"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success-taikhoan" data-delete="'+ data.id +'"></a></span>');
									$(".store-taikhoan").val(data.file);
									$(".store-taikhoan-tmp").val(data.name_tmp);
									$(".store-taikhoan-url").val(data.url);
									$("#upload-taikhoan").hide();
									new tp_remove_data_upload("#files-taikhoan .success-taikhoan .remove_img");//hàm xóa file upload
								} else {									
									$("#upload-results-taikhoan .uk-alert").addClass('uk-alert-danger');
									$('#files-khaisinh .clearfix').before('<span class="error">'+ data.message +'</span>');									
								}
				            }
				        };
				        var select = UIkit.uploadSelect($("#upload-taikhoan"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);				            
				    })(jQuery);
				</script>
				<script>
				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar-thunhap"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {
				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload_thunhap&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'thunhap',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...
				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },
				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },
				            allcomplete: function(data) {
				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);
					            $("#upload-results-thunhap").show();														
								if( data.status == "13" ){
									$('#files-thunhap .clearfix').before('<span class="success-thunhap"><a href="'+ data.url +'" data-rel="lightbox" class="lightboxhover" data-lightbox="thunhap" title="Xác nhận việc làm và thu nhập"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success-thunhap" data-delete="'+ data.id +'"></a></span>');
									$(".store-thunhap").val(data.file);
									$(".store-thunhap-tmp").val(data.name_tmp);
									$(".store-thunhap-url").val(data.url);
									$("#upload-thunhap").hide();
									new tp_remove_data_upload("#files-thunhap .success-thunhap .remove_img");//hàm xóa file upload
								} else {									
									$("#upload-results-thunhap .uk-alert").addClass('uk-alert-danger');
									$('#files-khaisinh .clearfix').before('<span class="error">'+ data.message +'</span>');									
								}
				            }
				        };
				        var select = UIkit.uploadSelect($("#upload-thunhap"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);				            
				    })(jQuery);
				</script>
				<script>
				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar-giaykhac"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {
				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload_giaykhac&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'giaykhac',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...
				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },
				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },
				            allcomplete: function(data) {
				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);
					            $("#upload-results-giaykhac").show();														
								if( data.status == "14" ){
									$('#files-giaykhac .clearfix').before('<span class="success-giaykhac"><a href="'+ data.url +'" data-rel="lightbox" class="lightboxhover" data-lightbox="giaykhac" title="Giấy tờ khác"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success-giaykhac" data-delete="'+ data.id +'"></a></span>');
									$(".store-giaykhac").val(data.file);
									$(".store-giaykhac-tmp").val(data.name_tmp);
									$(".store-giaykhac-url").val(data.url);
									$("#upload-giaykhac").hide();
									new tp_remove_data_upload("#files-giaykhac .success-giaykhac .remove_img");//hàm xóa file upload
								} else {									
									$("#upload-results-giaykhac .uk-alert").addClass('uk-alert-danger');
									$('#files-khaisinh .clearfix').before('<span class="error">'+ data.message +'</span>');									
								}
				            }
				        };
				        var select = UIkit.uploadSelect($("#upload-giaykhac"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);				            
				    })(jQuery);
				</script>
				
			</div><!--main col-md-12-->