<?php
/*
Template Name: Thong tin thanh toan
*/

if(is_user_logged_in()) {
				$current_user = wp_get_current_user();
				$user_id = get_current_user_id();
				$row = $wpdb->get_row("SELECT taikhoan FROM wp_ctv WHERE agent = '$user_id'", ARRAY_A);
				if(null !== $row) {
					$taikhoan_sql = $row['taikhoan'];									
				}
?>
	<?php global $virtue, $post; 
		$map 				= get_post_meta( $post->ID, '_kad_contact_map', true ); 
		$form_math 			= get_post_meta( $post->ID, '_kad_contact_form_math', true );
		$contactformtitle 	= get_post_meta( $post->ID, '_kad_contact_form_title', true );
		$form 				= get_post_meta( $post->ID, '_kad_contact_form', true );
	?>
			<script type="text/javascript">jQuery(document).ready(function ($) {$.extend($.validator.messages, {
			        required: "<?php echo __('Ô này không được để trống.', 'kadencetoolkit'); ?>",
					email: "<?php echo __('Địa chỉ email không hợp lệ.', 'kadencetoolkit'); ?>",
				 });
				$("#contactForm").validate();
			});</script>
			<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.validate.js"></script>
		<?php
	if(isset($_POST['submitted'])) {
		if(isset($form_math) && $form_math == 'yes') {
			if(md5($_POST['kad_captcha']) != $_POST['hval']) {
				$kad_captchaError = __('Check your math.', 'kadencetoolkit');
				$hasError = true;
			}
		}
	
	if(trim($_POST['taikhoan']) === '') {
		$taikhoanError = __('Bạn chưa nhập thông tin tài khoản ngân hàng.', 'kadencetoolkit');
		$hasError = true;
	} else {
		if(function_exists('stripslashes')) {
			$taikhoan = stripslashes(trim($_POST['taikhoan']));
		} else {
			$taikhoan = trim($_POST['taikhoan']);
		}
	}

	if(!isset($hasError)) {
		$query = $wpdb->update('wp_ctv',array('taikhoan'=>$taikhoan), array('agent' => $user_id));
		if($query){			
			$emailSent = true;
		}
	}

}
?>

  	<div id="pageheader" class="titleclass">
		<div class="container">
			<?php //get_template_part('templates/page', 'header'); ?>
		</div><!--container-->
	</div><!--titleclass-->
	<div id="content" class="container">
   		<div class="row">
			
			<div class="main contactformcase col-md-9" role="main">
			<?php if(isset($emailSent) && $emailSent == true) { ?>
			<p>Đã cập nhật thông tin thành công</p>
			<?php } ?>
			<div class="form-caption">
				<div class="form-title">
					<h1 class="head-title">Cập nhật thông tin tài khoản ngân hàng</h1>
				</div>
				
				<?php get_template_part('templates/content', 'page'); ?>
				<?php if(is_user_logged_in()) { ?>
				<?php
								
								if(isset($hasError) || isset($captchaError)) { ?>
									<p class="error"><?php _e('Đã có lỗi xảy ra, hãy kiểm tra và chắc chắn bạn không bỏ sót ô nào.', 'kadencetoolkit');?><p>
								<?php } ?>

								<form action="<?php the_permalink(); ?>" id="contactForm" method="post">
									<div class="contactform">					
										<p>
											<label for="taikhoanText"><?php _e('Thông tin tài khoản ngân hàng của bạn:', 'kadencetoolkit'); ?></label>
												<?php if(isset($taikhoanError)) { ?>
													<span class="error"><?php echo esc_html($taikhoanError);?></span>
												<?php } ?>
											<textarea name="taikhoan" id="taikhoanText" rows="10" class="required requiredField" placeholder="các thông tin như số tài khoản, ngân hàng, chi nhánh...để chúng tôi có thể chuyển tiền hoa hồng cho bạn" ><?php if($taikhoan) echo $taikhoan; else if($taikhoan_sql) echo $taikhoan_sql; ?></textarea>
										</p>
																			
										
										<?php if(isset($form_math) && $form_math == 'yes') {
											$one = rand(5, 50);
											$two = rand(1, 9);
											$result = md5($one + $two); ?>
											<p>
												<label for="kad_captcha"><?php echo $one.' + '.$two; ?> = </label>
												<input type="text" name="kad_captcha" id="kad_captcha" class="required requiredField kad_captcha kad-quarter" />
													<?php if(isset($kad_captchaError)) { ?>
														<label class="error"><?php echo esc_html($kad_captchaError);?></label>
													<?php } ?>
												<input type="hidden" name="hval" id="hval" value="<?php echo esc_attr($result);?>" />
											</p>
										<?php } ?>
										<hr>
										<p align="center">
											<input type="submit" class="kad-btn kad-btn-primary fwrd button" id="submit" value="<?php _e('Cập nhật thông tin', 'kadencetoolkit'); ?>" ></input>
										</p>
									</div><!-- /.contactform-->
									<input type="hidden" name="submitted" id="submitted" value="true" />
								</form>
				<?php
				} //if logged in
				else { echo "Bạn chưa đăng nhập"; }
				?>				
				</div>
			</div>
	
<?php } ?>