<?php
	wp_enqueue_style('virtue_child_skin', get_stylesheet_directory_uri() . '/assets/css/skins/redhot.css', false, null);
 
function my_child_theme_setup() {
    load_child_theme_textdomain( 'virtue', get_stylesheet_directory_uri() . '/languages' );
}
 add_action( 'after_setup_theme', 'my_child_theme_setup' );

//HTML email
add_filter( 'wp_mail_content_type', function( $content_type ) {
	return 'text/html';
});
//customize the comments template
function wordpressapi_comments($comment, $args, $depth) {
 $GLOBALS['comment'] = $comment; ?>
 <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
 <div id="comment-<?php comment_ID(); ?>">
 <!--<div>
 <?php //echo get_avatar($comment,$size='32',$default='<path_to_url>' ); ?>
 <?php //$user_name_str = substr(get_comment_author(),0, 20); ?>
 </div>-->
 <?php $user_name_str = substr(get_comment_author(),0, 20); ?>
 <?php if ($comment->comment_approved == '0') : ?>
 <em><?php _e('Your comment is awaiting moderation.') ?></em>
 <br />
 <?php endif; ?>
 <?php comment_text() ?>
 <?php printf(__('<span class="comment-author"><cite>%s</cite></span> | '), $user_name_str) ?>
 <div class="comment_date"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>"><?php //printf(__('%1$s at %2$s'), get_comment_date(),'&nbsp;'. get_comment_time()) ?><?php printf(__('%1$s'), get_comment_date()) ?></a><?php edit_comment_link(__('(Edit)'),'&nbsp; ',''); echo " | "; ?></div>
 <div class="divreply">
 <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
 </div>

<?php
 }
		
// add categories to attachments  
function wptp_add_categories_to_attachments() {
      register_taxonomy_for_object_type( 'job_listing_category', 'attachment' );  
}  
//add_action( 'init' , 'wptp_add_categories_to_attachments' ); 

function average_rating() {
    global $wpdb;
    $post_id = get_the_ID();
    $ratings = $wpdb->get_results("

        SELECT $wpdb->commentmeta.meta_value
        FROM $wpdb->commentmeta
        INNER JOIN $wpdb->comments on $wpdb->comments.comment_id=$wpdb->commentmeta.comment_id
        WHERE ($wpdb->commentmeta.meta_key='rating'
		AND $wpdb->commentmeta.meta_value IS NOT NULL )		
        AND $wpdb->comments.comment_post_id=$post_id 
        AND $wpdb->comments.comment_approved =1

        ");
    $counter = 0;
    $average_rating = 0;    
    if ($ratings) {
        foreach ($ratings as $rating) {
            $average_rating = $average_rating + $rating->meta_value;
            $counter++;
        } 
        //round the average to the nearast 1/2 point
        //return (round(($average_rating/$counter)*2,0)/2); 
		$diem = ($average_rating/$counter)*10/5;
			if($diem>9) $danhgia="Tuyệt vời ";
			else if($diem>8) $danhgia="Rất tốt ";
			else if($diem>7) $danhgia="Tốt ";
			else if($diem>6) $danhgia="Khá ";
			else $danhgia="Trung bình ";
		return '<span class="diem">'.$danhgia.$diem.'</span><span class="baseon"> dựa trên '.$counter.' đánh giá</span>';
    } else {
        //no ratings
        return '<span class="baseon">Chưa có đánh giá</span>';
    }
} 
function average_rating_on_comment() {
    global $wpdb;
    $post_id = get_the_ID();
    $ratings = $wpdb->get_results("

        SELECT $wpdb->commentmeta.meta_value
        FROM $wpdb->commentmeta
        INNER JOIN $wpdb->comments on $wpdb->comments.comment_id=$wpdb->commentmeta.comment_id
        WHERE ($wpdb->commentmeta.meta_key='rating'
		AND $wpdb->commentmeta.meta_value IS NOT NULL )	
        AND $wpdb->comments.comment_post_id=$post_id 
        AND $wpdb->comments.comment_approved =1

        ");
    $counter = 0;
    $average_rating = 0;    
    if ($ratings) {
        foreach ($ratings as $rating) {
            $average_rating = $average_rating + $rating->meta_value;
            $counter++;
        } 
        //round the average to the nearast 1/2 point
        //return (round(($average_rating/$counter)*2,0)/2); 
		$diem = ($average_rating/$counter)*10/5;
		$sao = ($average_rating/$counter);
			if($diem>9) $danhgia="Tuyệt vời ";
			else if($diem>8) $danhgia="Rất tốt ";
			else if($diem>7) $danhgia="Tốt ";
			else if($diem>6) $danhgia="Khá ";
			else $danhgia="Trung bình ";
		return '<div class="col-md-8"><span class="danhgia">'.$danhgia.'</span><span class="diemdanhgia">'.$diem.'</span><span class="duatren"> dựa vào '.$counter.' đánh giá</span></div><div class="col-md-4"><span class="saodanhgia">'.do_shortcode('[star rating="'.$sao.'" max="5"]').'</span></div><div style="clear:both;"></div>';
    } else {
        //no ratings
        return '<span class="baseon">Chưa có đánh giá</span>';
    }
} 
function remove_comment_fields($fields) {
    unset($fields['url']);
    return $fields;
}
add_filter('comment_form_default_fields','remove_comment_fields'); 
function job_type( $post = null ) {
	if ( $job_type = get_the_job_type( $post ) ) {
		return $job_type->name;
	}
}

function change_job_listing_slug( $args ) {
  $args['rewrite']['slug'] = 'cac-truong-tai-nhat';
  return $args;
}
add_filter( 'register_post_type_job_listing', 'change_job_listing_slug' );

// Add comment support to the post type
add_filter( 'register_post_type_job_listing', 'register_post_type_job_listing_enable_comments' );

function register_post_type_job_listing_enable_comments( $post_type ) {
	$post_type['supports'][] = 'comments';
	return $post_type;
}

// Make comments open by default for new job_listing
add_filter( 'submit_job_form_save_job_data', 'custom_submit_job_form_save_job_data' );

function custom_submit_job_form_save_job_data( $data ) {
	$data['comment_status'] = 'open';
	return $data;
}

//Dung cho trang tim viec theo nganh nghe, hien thi category cha va category con
function get_job_categories( $args = '' ) {
	$defaults = array( 'taxonomy' => 'job_listing_category', 'hide_empty'=>'0' );
	$args = wp_parse_args( $args, $defaults );

	$taxonomy = apply_filters( 'get_categories_taxonomy', $args['taxonomy'], $args );

	// Back compat
	if ( isset($args['type']) && 'link' == $args['type'] ) {
		_deprecated_argument( __FUNCTION__, '3.0', '' );
		$taxonomy = $args['taxonomy'] = 'link_category';
	}

	$categories = (array) get_terms( $taxonomy, $args );

	foreach ( array_keys( $categories ) as $k )
		_make_cat_compat( $categories[$k] );
	return $categories;
}
//Dung cho trang tim viec theo nganh nghe, hien thi link cua category
function get_job_category_link( $category ) {
	if ( ! is_object( $category ) )
		$category = (int) $category;

	$category = get_term_link( $category, 'job_listing_category' );

	if ( is_wp_error( $category ) )
		return '';

	return $category;
}
/**
	 * When viewing a taxonomy archive, use the same template for all.
	 *
	 * @since Jobify 1.0
	 *
	 * @return void
	 */	

//Adding a salary field for jobs

//Add the field to the frontend
add_filter( 'submit_job_form_fields', 'frontend_add_salary_field' );

function frontend_add_salary_field( $fields ) {
  $fields['job']['job_salary'] = array(
    'label'       => __( 'Học phí', 'virtue' ),
    'type'        => 'number',
    'priority'    => 2.5
  );
  return $fields;
}

//Add the field to admin
add_filter( 'job_manager_job_listing_data_fields', 'admin_add_salary_field' );

function admin_add_salary_field( $fields ) {
  $fields['_job_salary'] = array(
    'label'       => __( 'Học phí', 'virtue' ),
    'type'        => 'text',
    //'placeholder' => 'e.g. 2000 (zero if negotiable)',
    'description' => '',
	'required'    => true,
    'priority'    => 2.5
  );
  return $fields;
}

//Display "Salary" on the single job page
//add_action( 'single_job_listing_meta_end', 'display_job_salary_data' );

function display_job_salary_data() {
  global $post;

  $salary = get_post_meta( $post->ID, '_job_salary', true );

  if ( $salary ) {
	echo number_format($salary,0,",",".");
  }
}

//Adding a currency field for jobs
//Adding a Salary Filter to the Job Search Form 
/**
 * This can either be done with a filter (below) or the field can be added directly to the job-filters.php template file!
 *
 * job-manager-filter class handling was added in v1.23.6
 */
add_action( 'job_manager_job_filters_search_jobs_end', 'filter_by_salary_field' );
function filter_by_salary_field() {
	?>
  <a href="javascript:toggleprice('by_price');"><div class="title_filter_by_price">Tìm theo mức học phí</div></a>
	<div id="by_price" class="search_salary">		
		<ul class="job_types">
		<li><label for="search_salary_100" class="xephang"><?php _e( 'Dưới 100 triệu', 'virtue' ); ?></label><input type="radio" name="filter_by_salary" value="under100" class="job-manager-filter"><span class="shortcode-star-rating"><?php _e( 'Dưới 100 triệu', 'virtue' ); ?></span></li>
		<li><label for="search_salary_100-120" class="xephang"><?php _e( 'Từ 100 triệu đến 120 triệu', 'virtue' ); ?></label><input type="radio" name="filter_by_salary" value="100000000-120000000" class="job-manager-filter"><span class="shortcode-star-rating"><?php _e( 'Từ 100 triệu đến 120 triệu', 'virtue' ); ?></span></li>
		<li><label for="search_salary_120-140" class="xephang"><?php _e( 'Từ 120 triệu đến 140 triệu', 'virtue' ); ?></label><input type="radio" name="filter_by_salary" value="120000000-140000000" class="job-manager-filter"><span class="shortcode-star-rating"><?php _e( 'Từ 120 triệu đến 140 triệu', 'virtue' ); ?></span></li>
		<li><label for="search_salary_140-160" class="xephang"><?php _e( 'Từ 140 triệu đến 160 triệu', 'virtue' ); ?></label><input type="radio" name="filter_by_salary" value="140000000-160000000" class="job-manager-filter"><span class="shortcode-star-rating"><?php _e( 'Từ 140 triệu đến 160 triệu', 'virtue' ); ?></span></li>
		<li><label for="search_salary_160" class="xephang"><?php _e( 'Từ 160 triệu trở lên', 'virtue' ); ?></label><input type="radio" name="filter_by_salary" value="over160" class="job-manager-filter"><span class="shortcode-star-rating"><?php _e( 'Từ 160 triệu trở lên', 'virtue' ); ?></span></li>
		</ul>
	</div>
	<?php
}
/**
 * This code gets your posted field and modifies the job search query
 */
add_filter( 'job_manager_get_listings', 'filter_by_salary_field_query_args', 10, 2 );
function filter_by_salary_field_query_args( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		// If this is set, we are filtering by salary
		if ( ! empty( $form_data['filter_by_salary'] ) ) {
			$selected_range = sanitize_text_field( $form_data['filter_by_salary'] );
			switch ( $selected_range ) {
				case '1' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_salary',
						'value'   => '0',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;
				case 'under100' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_salary',
						'value'   => '100000000',
						'compare' => '<=',
						'type'    => 'NUMERIC'
					);
				break;
				case 'over160' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_salary',
						'value'   => '160000000',
						'compare' => '>=',
						'type'    => 'NUMERIC'
					);
				break;				
				default :
					$query_args['meta_query'][] = array(
						'key'     => '_job_salary',
						'value'   => array_map( 'absint', explode( '-', $selected_range ) ),
						'compare' => 'BETWEEN',
						'type'    => 'NUMERIC'
					);
				break;
			}
			// This will show the 'reset' link
			add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
		}
	}
	return $query_args;
}
add_filter( 'job_manager_get_listings_custom_filter_text', 'apply_salary_filter_text' ,10 , 1 );
function apply_salary_filter_text( $text ) {
    $params = array();
    parse_str( $_POST['form_data'], $params );

    $salary = $params['filter_by_salary'] ;

    if ($salary) {
      $lev_arr = array(
        'under100'=>'dưới 100 triệu',
		'100000000-120000000'=>'từ 100 triệu đến 120 triệu',
		'120000000-140000000'=>'từ 120 triệu đến 140 triệu',
		'140000000-160000000'=>'từ 140 triệu đến 160 triệu',
		'over160'=>'từ 160 triệu trở lên'
      );

      $text .= ' ' . __( 'học phí ', 'wp-job-manager-tags' ) .   $lev_arr[$salary];

      return $text;
    }

    return $text;
  }
//Gets a formatted list of job tags for a post ID
function get_job_tag_list( $job_id ) {
	$terms = wp_strip_all_tags(get_the_term_list( $job_id, 'job_listing_tag', '', apply_filters( 'virtue_tag_list_sep', ', ' ), '' ));
	/*if ( ! apply_filters( 'enable_job_tag_archives', get_option( 'virtue_enable_tag_archive' ) ) )
		$terms = strip_tags( $terms );*/
	return $terms;
}

//Display "Tags" on the single job page
function display_job_tags_data() {
  global $post;
  $tags = get_job_tag_list( $post->ID );
  if ( $tags ) {   
	echo $tags;
  }
}
function display_categories_data() {
  global $post;
  $term_list = wp_strip_all_tags(get_the_term_list($post->ID, 'job_listing_category', ', ',', ',''));
  if ($term_list ) 
	echo $term_list;
}

//Adding a Number of student field for jobs

//Add the field to the frontend
add_filter( 'submit_job_form_fields', 'frontend_add_nostudent_field' );

function frontend_add_nostudent_field( $fields ) {
  $fields['job']['no_student'] = array(
    'label'       => __( 'Số lượng du học sinh đã nộp hồ sơ', 'virtue' ),
    'type'        => 'number',
    //'placeholder' => 'vd: 5000000',
	//'description' => 'Mức lương bạn có thể trả, ghi số 0 nếu thương lượng',
	//'required'    => true,
    'priority'    => 3
  );
  return $fields;
}

//Add the field to admin
add_filter( 'job_manager_job_listing_data_fields', 'admin_add_no_student_field' );

function admin_add_no_student_field( $fields ) {
  $fields['_no_student'] = array(
    'label'       => __( 'Số lượng du học sinh đã nộp hồ sơ', 'virtue' ),
    'type'        => 'text',
    //'placeholder' => 'e.g. 2000 (zero if negotiable)',
    'description' => '',
	'required'    => true,
    'priority'    => 3
  );
  return $fields;
}
//the_no_students function.
function the_available_students( $post = null ) {
	$count = get_the_no_students( $post );
	if($count) {$available = 20-$count;return $available;}
}
//the_no_students function.
function the_no_students( $post = null ) {
	$count = get_the_no_students( $post );
	if($count) return $count;
}

//get_the_no_students function.
function get_the_no_students( $post = null ) {
	$post = get_post( $post );
	if ( $post->post_type !== 'job_listing' )
		return;
	return apply_filters( 'the_no_students', $post->_no_student, $post );
}
//Adding a Vietnamese students field for jobs

//Add the field to the frontend
add_filter( 'submit_job_form_fields', 'frontend_add_vn_student_field' );

function frontend_add_vn_student_field( $fields ) {
  $fields['job']['vn_student'] = array(
    'label'       => __( 'Du học sinh Việt Nam', 'virtue' ),
    'type'        => 'select',
    'options' 	  => ['...','Có du học sinh Việt Nam đang học','Không có du học sinh Việt Nam đang học'],
    'required'    => true,
    'default'	  => 1,
    'placeholder' => '',
    'priority'    => 5.5
  );
  return $fields;
}

//Add the field to admin
add_filter( 'job_manager_job_listing_data_fields', 'admin_add_vn_student_field' );

function admin_add_vn_student_field( $fields ) {
  $fields['_vn_student'] = array(
    'label'       => __( 'Du học sinh Việt Nam', 'virtue' ),
    'type'        => 'select',
    'options' 	  => ['...','Có du học sinh Việt Nam đang học','Không có du học sinh Việt Nam đang học'],
    'default'	  => 2,
    'placeholder' => '',
    'description' => '',
    'priority'    => 2.5
  );
  return $fields;
}
//the_students function.
function the_students( $post = null ) {
	$jobtimes = get_the_students( $post );
	if ($jobtimes) {
      $lev_arr = array(
          1=>'<i class="fa fa-star" style="color:#df0000;" aria-hidden="true"></i><span class="co-dhs">Có du học sinh Việt Nam đang học</span>',
          2=>'<i class="fa fa-star-o" aria-hidden="true"></i><span class="khong-dhs">Không có du học sinh Việt Nam đang học</span>'        
        );

    }
	echo $lev_arr[$jobtimes];
}

//get_the_students function.
function get_the_students( $post = null ) {
	$post = get_post( $post );
	if ( $post->post_type !== 'job_listing' )
		return;
	return apply_filters( 'the_students', $post->_vn_student, $post );
}
/**
 * the_job_location function.
 * @param  boolean $map_link whether or not to link to the map on google maps
 * @return [type]
 */
function the_job_address( $map_link = true, $post = null ) {
	$location = get_the_job_address( $post );

	if ( $location ) {
		if ( $map_link )
			echo apply_filters( 'the_job_location_map_link', '<a class="google_map_link" href="http://maps.google.com/maps?q=' . urlencode( $location ) . '&zoom=14&size=512x512&maptype=roadmap&sensor=false" target="_blank">' . $location . '</a>', $location, $post );
		else
			echo $location;
	} else {
		//echo apply_filters( 'the_job_location_anywhere_text', __( 'Anywhere', 'wp-job-manager' ) );
		echo '';
	}
}
function the_map_address( $map_link = true, $post = null ) {
	$location = get_the_streetview_address( $post );
	$maplocation = get_the_job_address( $post );	
	if ( $location ) {
		if ( $map_link )
		
			echo apply_filters( 'the_job_location_map_link', '<a class="google_map_link" href="'.$location.'" target="_blank"><img src="'. get_stylesheet_directory_uri().'/assets/img/duhocnhatbanjp_chitiettruong.png"></a>', $location, $post );
		else
			echo $location;
	} else if ( $maplocation ) {
		if ( $map_link )
			echo apply_filters( 'the_job_location_map_link', '<a class="google_map_link" href="http://maps.google.com/maps?q=' . urlencode( $maplocation ) . '&zoom=14&size=512x512&maptype=roadmap&sensor=false" target="_blank"><img src="'. get_stylesheet_directory_uri().'/assets/img/duhocnhatbanjp_chitiettruong.png"></a>', $maplocation, $post );
		else
			echo $maplocation;
	} else {
		//echo apply_filters( 'the_job_location_anywhere_text', __( 'Anywhere', 'wp-job-manager' ) );
		echo '';
	}
}
/**
 * get_the_job_location function.
 *
 * @access public
 * @param mixed $post (default: null)
 * @return void
 */
function get_the_job_address( $post = null ) {
	$post = get_post( $post );
	if ( $post->post_type !== 'job_listing' )
		return;

	return apply_filters( 'the_job_address', $post->_job_location, $post );
}
function get_the_streetview_address( $post = null ) {
	$post = get_post( $post );
	if ( $post->post_type !== 'job_listing' )
		return;

	return apply_filters( 'the_map_address', $post->streetview, $post );
}
  
//Adding a Job Times field for jobs

//Add the field to the frontend
add_filter( 'submit_job_form_fields', 'frontend_add_times_field' );

function frontend_add_times_field( $fields ) {
  $fields['job']['job_times'] = array(
    'label'       => __( 'Ký túc xá', 'virtue' ),
    'type'        => 'select',
    'options' 	  => ['...','Có ký túc xá','Không có ký túc xá','Giới thiệu ký túc xá'],
    'required'    => true,
    'default'	  => 1,
    'placeholder' => '',
    'priority'    => 5.5
  );
  return $fields;
}

//Add the field to admin
add_filter( 'job_manager_job_listing_data_fields', 'admin_add_times_field' );

function admin_add_times_field( $fields ) {
  $fields['_job_times'] = array(
    'label'       => __( 'Ký túc xá', 'virtue' ),
    'type'        => 'select',
    'options' 	  => ['...','Có ký túc xá','Không có ký túc xá','Giới thiệu ký túc xá'],
    'default'	  => 2,
    'placeholder' => '',
    'description' => '',
    'priority'    => 2.5
  );
  return $fields;
}
//the_jobtimes function.
function the_jobtimes( $post = null ) {
	$jobtimes = get_the_job_times( $post );
	if ($jobtimes) {
      $lev_arr = array(
          1=>'<i class="fa fa-bed" style="color:#0283DF;" aria-hidden="true"></i><span class="co-ktx">Có ký túc xá</span>',
          2=>'<i class="fa fa-user-times" aria-hidden="true"></i><span class="khong-ktx">Không có ký túc xá</span>',
          3=>'<i class="fa fa-user-plus" aria-hidden="true"></i><span class="gioithieu-ktx">Giới thiệu ký túc xá</span>'        
        );
      echo $lev_arr[$jobtimes];
    }
    else echo "<i class=\"fa fa-refresh\" style=\"color:#0a9c4b;\" aria-hidden=\"true\"></i><span class=\"capnhat-ktx\">Ký túc xá: đang cập nhật</span>";
	
}

//get_the_job_times function.
function get_the_job_times( $post = null ) {
	$post = get_post( $post );
	if ( $post->post_type !== 'job_listing' )
		return;
	return apply_filters( 'the_jobtimes', $post->_job_times, $post );
}
if ( ! function_exists( 'get_job_listing_regions' ) ) :
/**
 * Get job categories
 *
 * @access public
 * @return array
 */
function get_job_listing_regions() {
  if ( ! get_option( 'job_manager_enable_categories' ) ) {
    return array();
  }

  return get_terms( "job_listing_region", array(
    'orderby'       => 'id',
      'order'         => 'ASC',
      'hide_empty'    => false,
  ) );
}
endif;
//Search form
function search_job_form () {
$nowY = date('Y');
$nextY = date('Y', strtotime('+1 year'));
$next2Y = date('Y', strtotime('+2 year'));
$next3Y = date('Y', strtotime('+3 year'));
$next4Y = date('Y', strtotime('+4 year'));
$next5Y = date('Y', strtotime('+5 year'));
?>
<form class="home_filters filters_form" method="GET" action="<?php echo site_url();?>/cac-truong-tai-nhat">
			  <div class="search_jobs">
			  
				<!--<div class="fsearch_keywords">
					<label for="search_keywords">Từ khóa</label>
					<input type="text" name="search_keywords" id="search_keywords" class="form-control" placeholder="Nhập tiêu đề, vị trí công việc..." value="">
				</div>-->
				
				<div class="col-md-4" style="padding-left: 5px;padding-right: 5px;">
					<div class="fsearch_categories">
						<div class="select-style">
						
							<p class="search-title">Chọn khu vực, thành phố</p>
							<select id="search_category" name="search_category" class="form-control">
								<option value="">Nơi dự định nhập học</option>
								<?php //get_child_job_listing_categories();?>							
								<?php foreach ( get_job_listing_categories() as $cat ) : ?>
								<option value="<?php echo esc_attr( $cat->term_id ); ?>" data-image="<?php echo category_description($cat->term_id); ?>" data-description="<?php echo esc_html( $cat->name ); ?>"><?php echo esc_html( $cat->name ); ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-3" style="padding-left: 5px;padding-right: 5px;">
					<div class="fsearch_location">
						<div class="select-style">
						
							<p class="search-title">Tháng nhập học</p>
							<select id="search_location" name="search_region" class="form-control">
								<option value="">Tháng dự định nhập học</option>
								<?php foreach ( get_job_listing_regions() as $cat ) : ?>
								<option value="<?php echo esc_attr( $cat->term_id ); ?>"><?php echo esc_html( $cat->name ); ?></option>
								<?php endforeach; ?>
							</select>
						</div>		
					</div>
				</div>
				
				
				<div class="col-md-3" style="padding-left: 5px;padding-right: 5px;">
					<div class="fsearch_location">
						<div class="select-style">
						
							<p class="search-title">Năm nhập học</p>
							<select id="search_year" name="search_year" class="form-control">
								<option value="">Năm dự định nhập học</option>
								<option value="<?php echo $nowY;?>"><?php echo $nowY;?></option>
								<option value="<?php echo $nextY;?>"><?php echo $nextY;?></option>
								<option value="<?php echo $next2Y;?>"><?php echo $next2Y;?></option>
								<option value="<?php echo $next3Y;?>"><?php echo $next3Y;?></option>
								<option value="<?php echo $next4Y;?>"><?php echo $next4Y;?></option>
								<option value="<?php echo $next5Y;?>"><?php echo $next5Y;?></option>
							</select>
						</div>		
					</div>
				</div>

		  
				
	
				<div class="col-md-2" style="padding-left: 5px;padding-right: 5px;">
			<p class="search-title">&nbsp;</p>
			<input type="submit" id="timkiem" class="kad-btn kad-btn-primary" value="Tìm kiếm">
		</div></div>
		</form>
<?php	
}
function chon_truong() {
	$postid = get_the_ID();
	$posttitle = get_the_title();
    return '<p align="center" style="padding:25px 0 50px 0;"><a class="btn3d" style="margin-top: 5px;" title="'. $posttitle .'" href="'. site_url() .'/dang-ky-lam-thu-tuc-nhap-hoc/?schoolid='. $postid .'"><span>Đăng ký làm thủ tục nhập học</span></a></p>';
}
add_shortcode('chontruong', 'chon_truong');

function mt_rand_str ($l, $c = 'abcdefghijklmnopqrstuvwxyz1234567890') {
    for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);
    return $s;
}
function get_user_role() {
	global $current_user;
	$user_roles = $current_user->roles;
	$user_role = array_shift($user_roles);
	return $user_role;
}	
	
add_action( 'job_manager_job_filters_search_jobs_showing_job', 'filter_by_featured_field' );
function filter_by_featured_field() {
	?>
	<div id="by_featured" class="search_featured">
		<label for="search_featured"><?php _e( 'Hàng đầu', 'virtue' ); ?></label>
		<select name="filter_by_featured" class="job-manager-filter">
			<option value="0" <?php if( empty( $_GET['featured'] ) ) echo "selected";?>><?php _e( 'Tất cả các trường', 'virtue' ); ?></option>
			<option value="1" <?php if( ! empty( $_GET['featured'] ) ) echo "selected";?>><?php _e( 'Các trường hàng đầu', 'virtue' ); ?></option>			
		</select>			
	</div>
	<?php
}
/**
 * This code gets your posted field and modifies the job search query
 */
add_filter( 'job_manager_get_listings', 'filter_by_featured_field_query_args', 10, 2 );
function filter_by_featured_field_query_args( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		// If this is set, we are filtering by featured
		if ( ! empty( $form_data['filter_by_featured'] ) ) {
			$selected_range = sanitize_text_field( $form_data['filter_by_featured'] );
			switch ( $selected_range ) {
				case '1' :
					$query_args['meta_query'][] = array(
						'key'     => '_featured',
						'value'   => '1',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;								
				default :
					$query_args['meta_query'][] = array(
						'key'     => '_featured',
						'value'   => '0',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;
			}
			// This will show the 'reset' link
			add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
		}
	}
	return $query_args;
}
add_filter( 'job_manager_get_listings_custom_filter_text', 'apply_featured_filter_text' ,10 , 1 );
function apply_featured_filter_text( $text ) {
    $params = array();
    parse_str( $_POST['form_data'], $params );

    $featured = $params['filter_by_featured'] ;

    if ($featured) {
      $lev_arr = array(
        '0'=>'',
		'1'=>'(trường hàng đầu)'
      );

      $text .= ' ' .   $lev_arr[$featured];

      return $text;
    }

    return $text;
  }
add_action( 'job_manager_job_filters_search_jobs_showing_job', 'filter_by_ktx_field' );
function filter_by_ktx_field() {
	?>
	<div id="by_ktx" class="search_ktx">
		<label for="search_ktx"><?php _e( 'Ký túc xá', 'virtue' ); ?></label>
		<select name="filter_by_ktx" class="job-manager-filter">
			<option value="0"><?php _e( 'Ký túc xá', 'virtue' ); ?></option>
			<option value="1" ><?php _e( 'Có ký túc xá', 'virtue' ); ?></option>
			<option value="2" ><?php _e( 'Không có ký túc xá', 'virtue' ); ?></option>
			<option value="3" ><?php _e( 'Giới thiệu ký túc xá', 'virtue' ); ?></option>			
		</select>			
	</div>
	<?php
}
/**
 * This code gets your posted field and modifies the job search query
 */
add_filter( 'job_manager_get_listings', 'filter_by_ktx_field_query_args', 10, 2 );
function filter_by_ktx_field_query_args( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		// If this is set, we are filtering by featured
		if ( ! empty( $form_data['filter_by_ktx'] ) ) {
			$selected_range = sanitize_text_field( $form_data['filter_by_ktx'] );
			switch ( $selected_range ) {
				case '1' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_times',
						'value'   => '1',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;
				case '2' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_times',
						'value'   => '2',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;
				case '3' :
					$query_args['meta_query'][] = array(
						'key'     => '_job_times',
						'value'   => '3',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;				
				default :
					$query_args['meta_query'][] = array(
						'key'     => '_job_times',
						'value'   => '0',
						'compare' => '=',
						'type'    => 'NUMERIC'
					);
				break;
			}
			// This will show the 'reset' link
			add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
		}
	}
	return $query_args;
}
add_filter( 'job_manager_get_listings_custom_filter_text', 'apply_ktx_filter_text' ,10 , 1 );
function apply_ktx_filter_text( $text ) {
    $params = array();
    parse_str( $_POST['form_data'], $params );

    $featured = $params['filter_by_ktx'] ;

    if ($featured) {
      $lev_arr = array(
        '0'=>'',
		'1'=>'Có ký túc xá',
        '2'=>'Không có ký túc xá',
        '3'=>'Giới thiệu ký túc xá'
      );

      $text .= ' ' .   $lev_arr[$featured];

      return $text;
    }

    return $text;
  }	  
//add_action( 'job_manager_job_filters_search_jobs_showing_job', 'sort_by_hocphi_field' );
function sort_by_hocphi_field() {
	?>
	<div id="by_featured" class="search_featured">
		<label for="search_featured"><?php _e( 'Học phí', 'virtue' ); ?></label>
		<select name="sort_by_hocphi" class="job-manager-filter">
			<option value="1"><?php _e( 'Từ thấp đến cao', 'virtue' ); ?></option>
			<option value="2"><?php _e( 'Từ cao đến thấp', 'virtue' ); ?></option>			
		</select>			
	</div>
	<?php
}

//Add the SENPAI field to admin
add_filter( 'job_manager_job_listing_data_fields', 'admin_add_senpai_field' );

function admin_add_senpai_field( $fields ) {
	$assigned=array();
	//global $post;
	$meta = get_post_meta( get_the_ID(),'wpuf_post_tags',true );
	$items = explode(',', $meta);
	foreach($items as $item)
		array_push($assigned,"'".$item."'");
	$assignedTags = implode(",",$assigned);
	$args = array(
		'posts_per_page' => -1,         
		'post_type' => 'portfolio',
		'orderby' => 'title,'
    );
	$html=array();
	$products = new WP_Query( $args );
	while ( $products->have_posts() ) {
            $products->the_post();
		array_push($html,"'".get_the_title()."'");
	}
	$array_tags = implode(",",$html);
	?>
	<!--/*Tag suggest by Hanh*/-->
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery-1.9.0.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery-ui-1.10.0.custom.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery.taghandler.js"></script>
	<link type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/jquery.taghandler.min.css" rel="stylesheet">
	<link type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/jquery-ui.css" rel="stylesheet">
	<link type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/prettify.css" rel="stylesheet">
	<style>
	#wpuf_post_tags {display:none;}
	.tagHandler {
		width: 100%;
		max-width: 100%;
	}
	label.senpai {
    font-weight: 700;
	}
	</style>
	<script type="text/javascript">
		 var j190 = $.noConflict(true);
	</script>		
	<!--/*Tag suggest by Hanh*/-->
	<script type="text/javascript">
	j190(function(){
		j190(".methodExample").tagHandler({				
			assignedTags: [ <?php echo $assignedTags; ?> ],								            
			availableTags: [ <?php echo $array_tags; ?> ],
			autocomplete: true,
			allowAdd: false,
			afterAdd: function() {document.getElementById("wpuf_post_tags").value =(j190("#example_get_tags").tagHandler("getTags").join(",\n"));},
			afterDelete: function() {document.getElementById("wpuf_post_tags").value =(j190("#example_get_tags").tagHandler("getTags").join(",\n"));},
			afterLoad : function() { document.getElementById("wpuf_post_tags").value =(j190("#example_get_tags").tagHandler("getTags").join(",\n")); }
			});																
		prettyPrint();
	})
	</script>
	<label class="senpai" for="senpai">Senpai phụ trách: </label>	
	<ul id="example_get_tags" class="methodExample"></ul>
	<div style="clear:both;"></div>
<?php
$fields['wpuf_post_tags'] = array(  
	'label'       => __( 'Các thông tin dùng để phân loại, sắp xếp trường', 'virtue' ),
    'type'      => 'text',
	'priority'    => 1
  );
return $fields;

}
//Custom Post Type Set Comments ON by default without show METABOX
function default_comments_on( $data ) {
    if( $data['post_type'] == 'job_listing' ) {
        $data['comment_status'] = "open";
    }

    return $data;
}
add_filter( 'wp_insert_post_data', 'default_comments_on' );
add_action( 'save_post', 'default_comments_on' );
/**
 * This code gets your posted field and modifies the job search query
 */
//add_filter( 'job_manager_get_listings', 'sort_by_hocphi_field_query_args', 10, 2 );
function sort_by_hocphi_field_query_args( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		// If this is set, we are filtering by featured
		if ( ! empty( $form_data['sort_by_hocphi'] ) ) {
			$selected_range = sanitize_text_field( $form_data['sort_by_hocphi'] );
			switch ( $selected_range ) {
				case '1' :
					$query_args['orderby'] = array(
						'job_salary'         => 'ASC'
					);
				break;								
				case '2' :
					$query_args['orderby'] = array(
						'job_salary'         => 'DESC'
					);
				break;
				default :
					$query_args['orderby'] = array(
						'job_salary'         => 'DESC'
					);
				break;
			}
			// This will show the 'reset' link
			add_filter( 'job_manager_get_listings_custom_filter', '__return_true' );
		}
	}
	return $query_args;
}
function custom_tag_cloud_widget($args) {
	$args['largest'] = 15; //largest tag
	$args['smallest'] = 15; //smallest tag
	$args['unit'] = 'px'; //tag font unit
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'custom_tag_cloud_widget' );
// WordPress - Store timestamp of a user's last login as user meta
function user_last_login( $user_login, $user ){
    update_user_meta( $user->ID, '_last_login', current_time('mysql') );
}
add_action( 'wp_login', 'user_last_login', 10, 2 );

//function for getting the last login
function get_last_login($user_id) {
   $last_login = get_user_meta($user_id, '_last_login', true);
 
   //picking up wordpress date time format
   $date_format = get_option('date_format') . ' ' . get_option('time_format');
 
   //converting the login time to wordpress format
   $the_last_login = mysql2date($date_format, $last_login, false);
 
   //finally return the value
   return $the_last_login;
}

function login_form() {
?>
		<form class="clearfix" action="<?php echo site_url('wp-login.php') ?>" method="post">
					<label class="grey" for="log"><?php _e('Tài khoản senpai:') ?></label>
					<input class="field full" type="text" name="log" id="log" value="<?php echo wp_specialchars(stripslashes($user_login), 1) ?>" size="23" />
					<label class="grey" for="pwd"><?php _e('Password:') ?></label>
					<input class="field full" type="password" name="pwd" id="pwd" size="23" />
	            	<label><input name="rememberme" id="rememberme" type="checkbox" checked="checked" value="forever" />&nbsp;<?php _e('Remember Me') ?></label>
        			<div class="clear"></div>
					<input type="submit" name="submit" value="<?php _e('Log In') ?>" class="bt_login" />
					<input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>"/>
					<p class="clear" style="margin-top: 10px;"><a class="lost-pwd" href="<?php echo site_url(); ?>/lostpassword/"><?php _e('Lost Password') ?></a></p>
		</form>
		<div style="border-bottom:1px solid #e9e9e9;"></div>
		<p class="clear">Bạn chưa có tài khoản senpai?</p>
		<p class="clear"><a class="line_bot" href="<?php echo site_url();?>/dang-ky-thanh-vien/" rel="nofollow">Đăng ký miễn phí tại đây.</a></p>
<?php
}
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}
add_filter( 'job_manager_show_addons_page', '__return_false' );
function get_post_by_title($page_title, $post_type ='job_listing' , $output = OBJECT) {
    global $wpdb;
        $post = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type= %s", $page_title, $post_type));
        if ( $post )
            return get_post($post);

    return null;
}