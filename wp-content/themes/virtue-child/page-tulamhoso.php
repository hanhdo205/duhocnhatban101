<?php
/*
Template Name: Thong tin hoc vien
*/
?>

<div id="pageheader" class="titleclass">
	<div class="container">
		<?php //get_template_part('templates/page', 'header'); ?>
	</div><!--container-->
</div><!--titleclass-->
<?php 
global $wpdb;
//if(is_user_logged_in()) {
if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
		  $upload_overrides = array( 'test_form' => false );
		  //$attachments = array();
				if(is_user_logged_in()) {
				$current_user = wp_get_current_user();
				$user_id = get_current_user_id();
				$user_email = $current_user->user_email;
				$user_hoten = get_user_meta( $user_id, 'user_ht', true );
				}
				else $user_id = 1;
				if(isset($_GET['id']))
					$numID = $_GET['id'];					
				else $numID = mt_rand_str(8);
				if(isset($_GET['school']))
					$school = $_GET['school'];
				$view = 0;			

			$result = $wpdb->get_row("SELECT * FROM `wp_hoso` WHERE numID = '$numID'", ARRAY_A);
			if(null !== $result) {
					$thongtinhocvien = $result['thongtinhocvien'];
					$hoten = $result['hoten'];
					$email = $result['email'];
					$hinh34 = $result['hinh34'];
					$khaisinh = $result['khaisinh'];
					$hokhau = $result['hokhau'];
					$cmnd = $result['cmnd'];
					$cap3 = $result['cap3'];
					$hocba = $result['hocba'];
					$cmndbaolanh = $result['cmndbaolanh'];
					$taikhoan = $result['taikhoan'];
					$thunhap = $result['thunhap'];
					$thongtinhocvien_duyet = $result['thongtinhocvien_duyet'];
					$view = $result['view'];
			}
			
		  if(isset($_POST['submitted'])) {
				if(isset($form_math) && $form_math == 'yes') {
					if(md5($_POST['kad_captcha']) != $_POST['hval']) {
						$kad_captchaError = __('Check your math.', 'kadencetoolkit');
						$hasError = true;
					}
				}
			if(trim($_POST['contactName']) === '') {
				$nameError = __('Please enter your name.', 'kadencetoolkit');
				$hasError = true;
			} else {
				$name = trim($_POST['contactName']);
			}

			if(trim($_POST['email']) === '')  {
				$emailError = __('Please enter your email address.', 'kadencetoolkit');
				$hasError = true;
			} else if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['email']))) {
				$emailError = __('You entered an invalid email address.', 'kadencetoolkit');
				$hasError = true;
			} else {
				$email = trim($_POST['email']);
			}

			/*if(trim($_POST['comments']) === '') {
				$commentError = __('Please enter a message.', 'kadencetoolkit');
				$hasError = true;
			} else {
				if(function_exists('stripslashes')) {
					$comments = stripslashes(trim($_POST['comments']));
				} else {
					$comments = trim($_POST['comments']);
				}
			}*/
			
			$uploadedfile = $_FILES['records'];
			$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );	 						  
			  if ( $movefile ) {
				if (!$movefile[ 'file' ]) {
					if(!$thongtinhocvien) {
						$fileError = __('Bạn chưa upload file', 'kadencetoolkit');
						 //var_dump( $movefile);
						$hasError = true;
					}
				}
				else {
					//array_push($attachments, $movefile[ 'file' ] );
					$attachments = $movefile[ 'file' ];
					$thongtinhocvien = $movefile[ 'url' ];
					$thongtinhocvien_duyet = '3';
				}
			  } else {
					
					echo "Possible file upload attack!\n";
					
			  }
			  
			  if(!isset($hasError)) {
				if (isset($virtue['contact_email'])) {
					$emailTo = $virtue['contact_email'];
				} else {
					$emailTo = get_option('admin_email');
				}
				$sitename = get_bloginfo('name');
				$subject = '['.esc_html($sitename) . ' ' . __("Contact", "kadencetoolkit").'] '. __("From", "kadencetoolkit") . ' ' . esc_html($name);
				
				//$body = __('Name', 'kadencetoolkit').": $name \n\n<br>";
				//$body .= __('Email', 'kadencetoolkit').": $email \n\n<br>";
				//$body .= __('Comments', 'kadencetoolkit').":\n $comments";
				//$headers[] = 'Reply-To: ' . esc_html($name) . '<' . $email . '>' . '\r\n';
				//$headers[] = 'From: Thong tin hoc vien <' . $email . '>' . "\r\n";
				//$headers[] = 'Cc: ' . esc_html($name) . ' <' . $email . '>' . "\r\n";
				
				//wp_mail($emailTo, $subject, $body, $headers, $attachments);
				 
							
					if(null !== $result) {
						$query = $wpdb->update('wp_hoso', array('hoten'=>$name,'email'=>$email,'thongtinhocvien'=>$thongtinhocvien, 'thongtinhocvien_duyet'=>$thongtinhocvien_duyet,'view'=>'0'), array('numID'=>$numID));						
					} else {
						$query = $wpdb->insert('wp_hoso',array('userID'=>$user_id,'school'=>$school,'hoten'=>$name,'email'=>$email,'numID'=>$numID,'thongtinhocvien'=>$thongtinhocvien,'thongtinhocvien_duyet'=>$thongtinhocvien_duyet));						
					}
				
				if($query){
					//wp_mail($emailTo, $subject, $body, $headers, $attachments);
					$emailSent = true;
				}
			}
		}	  
	  //} //if logged in
	  ?>	

<div id="content" class="container">
   	<div class="row">
     	<div class="main <?php echo esc_attr(kadence_main_class()); ?>" role="main">
			
				<div class="checkout-wrap">
				  <ul class="checkout-bar">
					<li class="active">
					  <a href="#">Thông tin học viên</a>
					</li>
					<?php if(get_user_role()=="ctv_role"){ ?>
					<li class="next"><?php if($thongtinhocvien && $hoten && $email) {?><a href="../ctv-giay-to-ban-than-hoc-vien/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Giấy tờ bản thân học viên</a> <?php } else { ?>Giấy tờ bản thân học viên <?php } ?></li>   
					<li class="next"><?php if($thongtinhocvien && $hoten && $email&&$hinh34&&$khaisinh&&$hokhau&&$cmnd&&$cap3&&$hocba) {?><a href="../ctv-giay-to-nguoi-bao-lanh/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Giấy tờ người bảo lãnh</a> <?php } else { ?>Giấy tờ người bảo lãnh<?php } ?></li>        
					<li class="next"><?php if($thongtinhocvien && $hoten && $email&&$cmndbaolanh&&$taikhoan&&$thunhap) {?><a href="../ctv-nop-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Nộp hồ sơ</a><?php } else { ?>Nộp hồ sơ<?php } ?></li>    
					<li class="next"><?php if($view)
					 { ?><a href="../ctv-hoan-thien-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Hoàn thiện hồ sơ</a>
					 <?php } else { ?>Hoàn thiện hồ sơ<?php } ?></li>
					<?php } else { ?>
					<li class="next"><?php if($thongtinhocvien && $hoten && $email) {?><a href="../giay-to-ban-than-hoc-vien/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Giấy tờ bản thân học viên</a> <?php } else { ?>Giấy tờ bản thân học viên <?php } ?></li>   
					<li class="next"><?php if($thongtinhocvien && $hoten && $email&&$hinh34&&$khaisinh&&$hokhau&&$cmnd&&$cap3&&$hocba) {?><a href="../giay-to-nguoi-bao-lanh/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Giấy tờ người bảo lãnh</a> <?php } else { ?>Giấy tờ người bảo lãnh<?php } ?></li>        
					<li class="next"><?php if($thongtinhocvien && $hoten && $email&&$cmndbaolanh&&$taikhoan&&$thunhap) {?><a href="../nop-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Nộp hồ sơ</a><?php } else { ?>Nộp hồ sơ<?php } ?></li>    
					<li class="next"><?php if($view)
					 { ?><a href="../hoan-thien-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Hoàn thiện hồ sơ</a>
					 <?php } else { ?>Hoàn thiện hồ sơ<?php } ?></li>
					<?php } ?>
				  </ul>
				</div>
					
				<!--<div class="contactformcase col-md-9">-->
				<div class="contactformcase col-md-9">
      			<?php if (!empty($contactformtitle)) { 
      					echo '<h3>'. esc_html($contactformtitle).'</h3>';
      					} 
      					if(isset($emailSent) && $emailSent == true) { ?>
							<div class="thanks">
								<div class="form-caption">
									<div class="form-title">
										<h1 class="head-title">Thông tin học viên</h1>
									</div>
									<p><?php _e('Bạn đã cập nhật thành công thông tin học viên.', 'kadencetoolkit');?></p>
									<p><?php _e('Để tiếp tục, bạn vui lòng chuyển qua bước 2 "Giấy tờ bản thân học viên"', 'kadencetoolkit');?></p>
									<hr>
									<p align="center">
									<?php if(get_user_role()=="ctv_role"){ ?>
									<a href="../ctv-giay-to-ban-than-hoc-vien/?id=<?php echo $numID;?>&school=<?php echo $school;?>" class="kad-btn kad-btn-primary" >Chuyển qua bước 2</a>
									<?php } else { ?>
									<a href="../giay-to-ban-than-hoc-vien/?id=<?php echo $numID;?>&school=<?php echo $school;?>" class="kad-btn kad-btn-primary" >Chuyển qua bước 2</a>
									<?php }?>
									</p>
									
								</div>
							</div>
						<?php } else { ?>
						<div class="form-caption">
							<div class="form-title">
								<div class="head-title">Chương trình tự làm hồ sơ du học miễn phí</div>
								<div class="sub-title">Download file mẫu</div>
							</div>
							<p>Điền thông tin theo mẫu, sau đó upload file đã điền đầy đủ thông tin cho chúng tôi theo form bên dưới</p>
							<div class="button-wrapper">
							<a href="../wp-content/themes/virtue-child/personal_records.docx" class="a-btn">
								<span class="a-btn-symbol">Z</span>
								<span class="a-btn-text">Tải file mẫu</span> 
								<span class="a-btn-slide-text">kích thước file 85 Kb</span>
								<span class="a-btn-slide-icon"></span>
							</a>
							</div>
							<div style="clear:both;"></div>
						</div>
						<hr>
						<?php //if(is_user_logged_in()) { ?>
							<?php
							if(isset($hasError) || isset($captchaError)) { ?>
								<p class="error"><?php _e('Sorry, an error occured.', 'kadencetoolkit');?><p>
							<?php } ?>
							<div class="form-caption">
							<div class="form-title">
								<h1 class="head-title">Thông tin học viên</h1>
							</div>
							<form action="<?php the_permalink(); ?>?id=<?php echo $numID;?>&school=<?php echo $school;?>" id="contactForm" method="post" enctype="multipart/form-data">
								<div class="contactform">
									
									<p>
										<label for="contactName"><?php _e('Họ và tên học viên:', 'kadencetoolkit'); ?></label>
										<?php if(isset($nameError)) { ?>
											<span class="error"><?php echo esc_html($nameError);?></span>
										<?php } ?>
										<input type="text" name="contactName" id="contactName" value="<?php if(isset($_POST['contactName'])) echo esc_attr($_POST['contactName']); else if($hoten) echo $hoten; else if(is_user_logged_in()) echo $user_hoten;?>" class="required requiredField full" />
									</p>
									<hr>
									<p>
										<label for="email"><?php _e('Email liên hệ:', 'kadencetoolkit'); ?></label>
											<?php if(isset($emailError)) { ?>
												<span class="error"><?php echo esc_html($emailError);?></span>
											<?php } ?>
										<input type="text" name="email" id="email" value="<?php if(isset($_POST['email'])) echo esc_attr($_POST['email']); else if($email) echo $email; else if(is_user_logged_in()) echo $user_email;?>" class="required requiredField email full" />
									</p>
									<hr>
									<p>
										<label for="file"><?php _e('Thông tin học viên:', 'kadencetoolkit'); ?> <?php if($thongtinhocvien_duyet==1) {?>(<a href="<?php echo get_stylesheet_directory_uri(); ?>/read-file-doc.php?file=<?php echo $thongtinhocvien;?>" target="_blank" title="Thông tin học viên"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/check_ok.png"></a>) <?php } else if($thongtinhocvien_duyet==2) {?>(<a href="<?php echo get_stylesheet_directory_uri(); ?>/read-file-doc.php?file=<?php echo $thongtinhocvien;?>" target="_blank" title="Thông tin học viên"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bad.png"></a>) <?php } else if($thongtinhocvien_duyet==3) {?>(<a href="<?php echo get_stylesheet_directory_uri(); ?>/read-file-doc.php?file=<?php echo $thongtinhocvien;?>" target="_blank" title="Thông tin học viên"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tick_octagon.png"></a>) <?php }?></label>
											<?php if(isset($fileError)) { ?>
												<span class="error"><?php echo esc_html($fileError);?></span>
											<?php } ?>
										<input id="upload-select" type="file" name="records" accept="application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf" class="full"><em>(Chấp nhận file .doc, .docx, .pdf)</em>
									</p>
									<hr>
					
									<?php if(isset($form_math) && $form_math == 'yes') {
										$one = rand(5, 50);
										$two = rand(1, 9);
										$result = md5($one + $two); ?>
										<p>
											<label for="kad_captcha"><?php echo $one.' + '.$two; ?> = </label>
											<input type="text" name="kad_captcha" id="kad_captcha" class="kad_captcha kad-quarter" />
												<?php if(isset($kad_captchaError)) { ?>
													<label class="error"><?php echo esc_html($kad_captchaError);?></label>
												<?php } ?>
											<input type="hidden" name="hval" id="hval" value="<?php echo esc_attr($result);?>" />
										</p>
									<?php } ?>
									<p align="center">
										<input type="submit" class="kad-btn kad-btn-primary" id="submit" value="<?php _e('Lưu thông tin', 'kadencetoolkit'); ?>"></input>
									</p>
								</div><!-- /.contactform-->
								<input type="hidden" name="submitted" id="submitted" value="true" />
							</form>
							</div>
						<?php //} else { echo "Bạn chưa đăng nhập"; }
						} ?>
      		</div>
			<?php /*if(get_user_role()!="ctv_role"){ ?>
			<div class="free-signup widget-free-signup col-md-3">
				<div class="textwidget"><div class="texttitle">TỰ LÀM HỒ SƠ DU HỌC MIỄN PHÍ LÀ GÌ?</div>
				<span style="font-size:13px;font-style:italic;color:#555;">Là chương trình giúp các bạn có thể <strong>tự làm hồ sơ du học Nhật Bản</strong> thông qua sự hướng dẫn từ website và senpai <strong>mà không phải mất chi phí tư vấn cho công ty môi giới du học</strong><hr>
				<strong>TẠI SAO LẠI LÀ MIỄN PHÍ?</strong> Giúp học sinh và gia đình đỡ đi một chút gánh nặng<hr>
				<strong>CHƯƠNG TRÌNH HOẠT ĐỘNG NHỜ NGUỒN KINH PHÍ NÀO?</strong> Chương trình hoạt động với sự tài trợ kinh phí từ các trường ở Nhật Bản</span></div>
			</div>
			<?php }*/ ?><!--contactform-->	
				<?php get_template_part('templates/content', 'page'); ?>
				<?php global $virtue; 
					if(isset($virtue['page_comments']) && $virtue['page_comments'] == '1') {
						comments_template('/templates/comments.php');
					} 		
		?>
		</div><!-- /.main -->
		