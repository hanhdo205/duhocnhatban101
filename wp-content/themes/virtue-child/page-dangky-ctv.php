<?php
/*
Template Name: Dang ky lam ctv
*/


				$current_user = wp_get_current_user();
				$user_id = get_current_user_id();
?>
	<?php global $virtue, $post; 
		$map 				= get_post_meta( $post->ID, '_kad_contact_map', true ); 
		$form_math 			= get_post_meta( $post->ID, '_kad_contact_form_math', true );
		$contactformtitle 	= get_post_meta( $post->ID, '_kad_contact_form_title', true );
		$form 				= get_post_meta( $post->ID, '_kad_contact_form', true );
		//if ($form == 'yes') { ?>
			<script type="text/javascript">jQuery(document).ready(function ($) {$.extend($.validator.messages, {
			        required: "<?php echo __('Ô này không được để trống.', 'kadencetoolkit'); ?>",
					email: "<?php echo __('Địa chỉ email không hợp lệ.', 'kadencetoolkit'); ?>",
				 });
				$("#contactForm").validate();
			});</script>
			<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.validate.js"></script>
				<!-- Datetime picker -- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/bootstrap331.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/moment-with-locales.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/bootstrap-datetimepicker41328.js"></script>
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/datetime-picker.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<script type="text/javascript">
		var j211 = $.noConflict(true);
	</script>
	<!-- Datetime picker -->
	
	
		<?php //} 


	if(isset($_POST['submitted'])) {
		if(isset($form_math) && $form_math == 'yes') {
			if(md5($_POST['kad_captcha']) != $_POST['hval']) {
				$kad_captchaError = __('Check your math.', 'kadencetoolkit');
				$hasError = true;
			}
		}
	if(trim($_POST['contactName']) === '') {
		$nameError = __('Bạn chưa nhập họ và tên.', 'kadencetoolkit');
		$hasError = true;
	} else {
		$name = trim($_POST['contactName']);
	}
	
	if(trim($_POST['phonenumber']) === '') {
		$telError = __('Điện thoại liên lạc của bạn?', 'kadencetoolkit');
		$hasError = true;
	} else {
		$phonenumber = trim($_POST['phonenumber']);
	}
	
	if(trim($_POST['address']) === '') {
		$addressError = __('Bạn cho chúng tôi xin thêm 1 ít thông tin.', 'kadencetoolkit');
		$hasError = true;
	} else {
		$address = trim($_POST['address']);
	}
	
	if(trim($_POST['facebook']) === '') {
		$facebookError = __('Bạn cho chúng tôi xin thêm 1 ít thông tin.', 'kadencetoolkit');
		$hasError = true;
	} else {
		$facebook = trim($_POST['facebook']);
	}
	
	if(trim($_POST['viber']) === '') {
		$viberError = __('Bạn cho chúng tôi xin thêm 1 ít thông tin.', 'kadencetoolkit');
		$hasError = true;
	} else {
		$viber = trim($_POST['viber']);
	}
	
	if(trim($_POST['date']) === '') {
		$dateError = __('Ngày tháng năm sinh?', 'kadencetoolkit');
		$hasError = true;
	} else {
		$date = trim($_POST['date']);
	}

	if(trim($_POST['email']) === '')  {
		$emailError = __('Bạn chưa cung cấp địa chỉ email.', 'kadencetoolkit');
		$hasError = true;
	} else if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['email']))) {
		$emailError = __('Địa chỉ email không hợp lệ.', 'kadencetoolkit');
		$hasError = true;
	} else {
		$email = trim($_POST['email']);
	}
	
	if(trim($_POST['japanese']) === '') {
		$japaneseError = __('Bạn cho chúng tôi xin thêm 1 ít thông tin.', 'kadencetoolkit');
		$hasError = true;
	} else {
		$japanese = trim($_POST['japanese']);
		if($japanese == 4)  $japanese_st = 'Khác';
		else $japanese_st = 'N'.$japanese.' hoặc tương đương N'.$japanese;
	}	

	if(trim($_POST['comments']) === '') {
		$commentError = __('Bạn cho chúng tôi xin thêm 1 ít thông tin.', 'kadencetoolkit');
		$hasError = true;
	} else {
		if(function_exists('stripslashes')) {
			$comments = stripslashes(trim($_POST['comments']));
		} else {
			$comments = trim($_POST['comments']);
		}
	}

	if(!isset($hasError)) {
		if (isset($virtue['contact_email'])) {
			$emailTo = $virtue['contact_email'];
		} else {
			$emailTo = get_option('admin_email');
		}
		$sitename = get_bloginfo('name');
		$subject = '[Đăng ký làm cộng tác viên] '. __("From", "kadencetoolkit") . ' <' . $email . '>';
		
		$body = __('Đăng ký làm cộng tác viên', 'kadencetoolkit')."\n\n<br><br>";
		$body .= __('User ID', 'kadencetoolkit').": $user_id \n\n<br>";
		$body .= __('Họ và tên', 'kadencetoolkit').": $name \n\n<br>";
		$body .= __('Ngày tháng năm sinh', 'kadencetoolkit').": $date \n\n<br>";
		$body .= __('Địa chỉ email', 'kadencetoolkit').": $email \n\n<br>";
		$body .= __('Điện thoại liên hệ', 'kadencetoolkit').": $phonenumber \n\n<br>";
		$body .= __('Địa chỉ liên hệ', 'kadencetoolkit').": $address \n\n<br>";
		$body .= __('Số Viber/Line/Zalo', 'kadencetoolkit').": $viber \n\n<br>";
		$body .= __('Địa chỉ Facebook', 'kadencetoolkit').": $facebook \n\n<br>";
		$body .= __('Trình độ tiếng Nhật', 'kadencetoolkit').": $japanese_st \n\n<br>";
		$body .= __('Tự giới thiệu về bản thân', 'kadencetoolkit').": $comments \n\n<br><br>";
		
		//$body .= __('Email', 'kadencetoolkit').": $email \n\n";
		//$body .= __('Comments', 'kadencetoolkit').":\n $comments";
		//$headers[] = 'Reply-To: ' . esc_html($name) . '<' . $email . '>' . "\r\n";
		
		$query = $wpdb->insert('wp_ctv',array('agent'=>$user_id,'hoten'=>$name,'email'=>$email,'dienthoai'=>$phonenumber,'birthday'=>$date,'address'=>$address,'viber'=>$viber,'facebook'=>$facebook,'japanese'=>$japanese,'comments'=>$comments));
		if($query){
			$headers[] = 'From: Đăng ký làm cộng tác viên <' . $email . '>' . "\r\n";
			$headers[] = 'Cc: '.$name.' <' . $email . '>' . "\r\n";		
			wp_mail($emailTo, $subject, $body, $headers);
			$emailSent = true;
		}
	}

} ?>
<div class="container">
<div class="row">
<?php //the_breadcrumb();?>
</div>
</div>
  	<div id="pageheader" class="titleclass">
		<div class="container">
			<?php get_template_part('templates/page', 'header'); ?>
		</div><!--container-->
	</div><!--titleclass-->
	<div id="content" class="container">
   		<div class="row">
			<?php if(isset($emailSent) && $emailSent == true) { ?>
			<div class="main contactformcase col-md-9" role="main">
				<div class="form-caption">
					<div class="form-title">
						<h2 class="head-title">Form đăng ký làm cộng tác viên (senpai)</h1>
					</div>
					<p>Chúng tôi đã nhận được thông tin của bạn và sẽ xác nhận thông tin trong vòng 03 ngày làm việc.</p>
					<p>Chúc bạn một ngày tốt lành.</p>
					<?php get_template_part('templates/content', 'page'); ?>
				</div>
			</div>
			<div class="free-signup widget-free-signup col-md-3">
				
			</div>
			<?php } else { ?>
			<div class="main contactformcase col-md-9" role="main">
			<div class="form-caption">
				<div class="form-title">
					<h2 class="head-title">Form đăng ký làm cộng tác viên (senpai)</h1>
				</div>
				
				<?php get_template_part('templates/content', 'page'); ?>
				<?php if(is_user_logged_in()) { ?>
				<?php
								
								if(isset($hasError) || isset($captchaError)) { ?>
									<p class="error"><?php _e('Đã có lỗi xảy ra, hãy kiểm tra và chắc chắn bạn không bỏ sót ô nào.', 'kadencetoolkit');?><p>
								<?php } ?>

								<form action="<?php the_permalink(); ?>" id="contactForm" method="post">
									<div class="contactform">
										
										<p>
											<label for="contactName"><?php _e('Họ và tên:', 'kadencetoolkit'); ?></label>
											<?php if(isset($nameError)) { ?>
												<span class="error"><?php echo esc_html($nameError);?></span>
											<?php } ?>
											<input type="text" name="contactName" id="contactName" value="<?php if(isset($_POST['hoten'])) echo esc_attr($_POST['hoten']); else if(is_user_logged_in()) {$user_id = get_current_user_id(); $user_hoten = get_user_meta( $user_id, 'user_ht', true ); echo $user_hoten;}?>" class="required requiredField full" />
										</p>
										<hr>
										<p>
											<label for="date"><?php _e('Ngày tháng năm sinh:', 'kadencetoolkit'); ?></label>
												<?php if(isset($dateError)) { ?>
													<span class="error"><?php echo esc_html($dateError);?></span>
												<?php } ?>
											<input type="text" name="date" id="date" value="<?php if(isset($_POST['date']))  echo esc_attr($_POST['date']); else if(is_user_logged_in()) {$user_id = get_current_user_id(); $user_bday = get_user_meta( $user_id, 'user_bday', true ); echo $user_bday;}?>" class="required requiredField full" />
										</p>
										<hr>
										<p>
											<label for="email"><?php _e('Địa chỉ email:', 'kadencetoolkit'); ?></label>
												<?php if(isset($emailError)) { ?>
													<span class="error"><?php echo esc_html($emailError);?></span>
												<?php } ?>
											<input type="text" name="email" id="email" value="<?php if(isset($_POST['dc-email'])) echo esc_attr($_POST['dc-email']); else if(isset($_POST['email']))  echo esc_attr($_POST['email']); else if(isset($_GET['email'])) echo $_GET['email']; else if(is_user_logged_in()) {$user_email = $current_user->user_email; echo $user_email;}?>" class="required requiredField email full" />
										</p>
										<hr>
										<p>
											<label for="phonenumber"><?php _e('Số điện thoại liên hệ:', 'kadencetoolkit'); ?></label>
												<?php if(isset($telError)) { ?>
													<span class="error"><?php echo esc_html($telError);?></span>
												<?php } ?>
											<input type="tel" name="phonenumber" id="phonenumber" value="<?php if(isset($_POST['phonenumber']))  echo esc_attr($_POST['phonenumber']); else if(is_user_logged_in()) {$user_id = get_current_user_id(); $user_phone_dd = get_user_meta( $user_id, 'user_phone_dd', true ); echo $user_phone_dd;}?>" class="required requiredField full" />
										</p>
										<hr>
										<p>
											<label for="address"><?php _e('Địa chỉ liên hệ hiện nay:', 'kadencetoolkit'); ?></label>
												<?php if(isset($addressError)) { ?>
													<span class="error"><?php echo esc_html($addressError);?></span>
												<?php } ?>
											<input type="text" name="address" id="address" value="<?php if(isset($_POST['address']))  echo esc_attr($_POST['address']); else if(is_user_logged_in()) {$user_id = get_current_user_id(); $user_address_dd = get_user_meta( $user_id, 'user_address_dd', true ); echo $user_address_dd;}?>" class="required requiredField full" />
										</p>
										<hr>
										<p>
											<label for="facebook"><?php _e('Địa chỉ Facebook:', 'kadencetoolkit'); ?></label>
												<?php if(isset($facebookError)) { ?>
													<span class="error"><?php echo esc_html($facebookError);?></span>
												<?php } ?>
											<input type="text" name="facebook" id="facebook" value="<?php if(isset($_POST['facebook']))  echo esc_attr($_POST['facebook']);?>" class="required requiredField full" />
										</p>
										<hr>
										<p>
											<label for="viber"><?php _e('Số Viber/Line/Zalo:', 'kadencetoolkit'); ?></label>
												<?php if(isset($viberError)) { ?>
													<span class="error"><?php echo esc_html($viberError);?></span>
												<?php } ?>
											<input type="text" name="viber" id="viber" value="<?php if(isset($_POST['viber']))  echo esc_attr($_POST['viber']);?>" class="required requiredField full" />
										</p>
										<hr>
										<p>
											<label for="japanese"><?php _e('Trình độ tiếng Nhật:', 'kadencetoolkit'); ?></label>
												
											<select name="japanese" id="japanese">
												  <option value="1" <?php if ( $_POST['japanese'] == "1" ) echo 'selected="selected"';?>>N1 hoặc tương đương N1</option>
												  <option value="2" <?php if ( $_POST['japanese'] == "2" ) echo 'selected="selected"';?>>N2 hoặc tương đương N2</option>
												  <option value="3" <?php if ( $_POST['japanese'] == "3" ) echo 'selected="selected"';?>>N3 hoặc tương đương N3</option>
												  <option value="4" <?php if ( $_POST['japanese'] == "4" ) echo 'selected="selected"';?>>khác</option>
											</select>
										</p>									
										<hr>
										<p>
											<label for="commentsText"><?php _e('Tự giới thiệu:', 'kadencetoolkit'); ?></label>
												<?php if(isset($commentError)) { ?>
													<span class="error"><?php echo esc_html($commentError);?></span>
												<?php } ?>
											<textarea name="comments" id="commentsText" rows="10" class="required requiredField" placeholder="một số thông tin bạn muốn chia sẻ, ví dụ như trình độ học vấn cao nhất, công việc hiện tại, kinh nghiệm sống, học tập, làm việc tại Nhật Bản,v.v..." ><?php if(isset($_POST['comments'])) { if(function_exists('stripslashes')) { echo esc_textarea(stripslashes($_POST['comments'])); } else { echo esc_textarea($_POST['comments']); } } ?></textarea>
										</p>
																			
										
										<?php if(isset($form_math) && $form_math == 'yes') {
											$one = rand(5, 50);
											$two = rand(1, 9);
											$result = md5($one + $two); ?>
											<p>
												<label for="kad_captcha"><?php echo $one.' + '.$two; ?> = </label>
												<input type="text" name="kad_captcha" id="kad_captcha" class="required requiredField kad_captcha kad-quarter" />
													<?php if(isset($kad_captchaError)) { ?>
														<label class="error"><?php echo esc_html($kad_captchaError);?></label>
													<?php } ?>
												<input type="hidden" name="hval" id="hval" value="<?php echo esc_attr($result);?>" />
											</p>
										<?php } ?>
										<hr>
										<p align="center">
											<input type="submit" class="kad-btn kad-btn-primary fwrd button" id="submit" value="<?php _e('Đăng ký làm senpai', 'kadencetoolkit'); ?>" ></input>
										</p>
									</div><!-- /.contactform-->
									<input type="hidden" name="submitted" id="submitted" value="true" />
								</form>
				<?php
				} //if logged in
				else { echo "Bạn chưa đăng nhập. Vui lòng <a href='http://duhocnhatban.jp/login/'>đăng nhập</a> hoặc <a href='http://duhocnhatban.jp/dang-ky-thanh-vien/'>đăng ký</a> tài khoản."; }
				?>				
				</div>
			</div>
			<div class="free-signup widget-free-signup col-md-3">
				
			</div>
			<script type="text/javascript">
			j211(function () {
						j211('#date').datetimepicker({
							locale: 'vi',
							viewMode: 'years',
							format: 'DD/MM/YYYY',
							icons: {
								 previous: "fa fa-arrow-left",
								 next: "fa fa-arrow-right",
							}               
						});		
			});
			</script>
			
			
			<?php }
			?>