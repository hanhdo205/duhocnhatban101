<?php
/*
Template Name: Hoan thien ho so
*/
?>

<div id="pageheader" class="titleclass">
	<div class="container">
		<?php //get_template_part('templates/page', 'header'); ?>
	</div><!--container-->
</div><!--titleclass-->
<?php 
//if(is_user_logged_in()) {
if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
		  $upload_overrides = array( 'test_form' => false );
		  //$attachments = array();
		  
				if(is_user_logged_in()) {
				$current_user = wp_get_current_user();
				$user_id = get_current_user_id();
				$user_email = $current_user->user_email;
				$user_hoten = get_user_meta( $user_id, 'user_ht', true );
				}
				else $user_id = 1;
				if(isset($_GET['id']))
					$numID = $_GET['id'];					
				else $numID = mt_rand_str(8);
				if(isset($_GET['school']))
					$school = $_GET['school'];
							
			
		  $row = $wpdb->get_row("SELECT * FROM `wp_hoso` WHERE numID = '$numID'", ARRAY_A);
			if(null !== $row) {
						$name = $row['hoten'];
						$email = $row['email'];
						$issent = $row['issent'];
						$tinhtrang = $row['tinhtrang'];
						$bosung = $row['comments'];
						$thongtinhocvien_duyet = $row['thongtinhocvien_duyet'];
						$hinh34_duyet = $row['hinh34_duyet'];
						$khaisinh_duyet = $row['khaisinh_duyet'];
						$hokhau_duyet = $row['hokhau_duyet'];
						$cmnd_duyet = $row['cmnd_duyet'];
						$cap3_duyet = $row['cap3_duyet'];
						$hocba_duyet = $row['hocba_duyet'];
						$daihoc_duyet = $row['daihoc_duyet'];
						$bangdiem_duyet = $row['bangdiem_duyet'];
						$tiengnhat_duyet = $row['tiengnhat_duyet'];
						$congtac_duyet = $row['congtac_duyet'];
						$cmndbaolanh_duyet = $row['cmndbaolanh_duyet'];
						$taikhoan_duyet = $row['taikhoan_duyet'];
						$thunhap_duyet = $row['thunhap_duyet'];
						$giaykhac_duyet = $row['giaykhac_duyet'];											
				}

		  if(isset($_POST['submitted'])) {
			if(isset($form_math) && $form_math == 'yes') {
				if(md5($_POST['kad_captcha']) != $_POST['hval']) {
					$kad_captchaError = __('Check your math.', 'kadencetoolkit');
					$hasError = true;
				}
			}

			if(isset($_POST['store-url'])) {				
				$bosungthem = implode(",", $_POST['store-url']);
				if($bosung) $bosung = $bosungthem.",".$bosung;
				else $bosung = $bosungthem;
			}			
			  
			  if(!isset($hasError)) {
				if (isset($virtue['contact_email'])) {
					$emailTo = $virtue['contact_email'];
				} else {
					$emailTo = get_option('admin_email');
				}
				$sitename = get_bloginfo('name');
				//$subject = '['.esc_html($sitename) . ' ' . __("Contact", "kadencetoolkit").'] '. __("From", "kadencetoolkit") . ' ' . esc_html($name);
				$subject = '['.esc_html($sitename) . '] '. __("-", "kadencetoolkit") . ' ' . esc_html($name);
				
				$body = __('Họ và tên học viên', 'kadencetoolkit').": $name \n\n<br>";
				$body .= __('Email liên hệ', 'kadencetoolkit').": $email \n\n<br>";
				$body .= 'Các file đính kèm: <a href="'.site_url().'/mpdf/examples/img2pdf.php?id='.$numID.'" target="_blank" >Xem file PDF</a> <br>';
				$body .= 'Link quản lý, cập nhật, bổ sung hồ sơ:<br>'.site_url().'/hoan-thien-ho-so/?id='.$numID.'&school='.$school.'<br><br>';
				$body .= 'Vâng, tôi đã hoàn thiện hồ sơ của mình! <br>';
				//$body .= __('Comments', 'kadencetoolkit').":\n $comments";
				//$headers[] = 'Reply-To: ' . esc_html($name) . '<' . $email . '>' . '\r\n';
				$headers[] = 'From: Thong tin hoc vien <' . $email . '>' . "\r\n";
				$headers[] = 'Cc: ' . esc_html($name) . ' <' . $email . '>' . "\r\n";
				
				//wp_mail($emailTo, $subject, $body, $headers, $attachments);
				 
							
					if (null !== $row) {
						if($name && $email) {
							$query = $wpdb->update('wp_hoso', array('view'=>'0','comments'=>$bosung), array('numID'=>$numID));
							if(trim($_POST['phanhoi']) != '') {			
								if(function_exists('stripslashes')) {
									$phanhoi = stripslashes(trim($_POST['phanhoi']));
								} else {
									$phanhoi = trim($_POST['phanhoi']);
								}
								date_default_timezone_set('Asia/Ho_Chi_Minh');								
								$wpdb->insert('wp_hoidap',array('numID'=>$numID,'name'=>$name,'comment'=>$phanhoi,'ngaydang'=>Date("Y-m-d\TH:i:s")));								
							}
							
						}
						else {
							$nameError = __('Hãy chắc chắn là bạn đã nhập tên học viên hoặc email liên hệ.', 'kadencetoolkit');
							$hasError = true;
						}
					} 
				
				if($query&&!$tinhtrang){
					wp_mail($emailTo, $subject, $body, $headers);
					$emailSent = true;
				}
			}
		}	  
	  //} //if logged in
	  ?>	

<div id="content" class="container">
   	<div class="row">
     	<div class="main <?php echo esc_attr(kadence_main_class()); ?>" role="main">
			
				
				<div class="checkout-wrap">
				  <ul class="checkout-bar">
				  <?php if(get_user_role()=="ctv_role"){ ?>
					<li class="previous visited">
					  <a href="../tao-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Thông tin học viên</a>
					</li>    
					<li class="previous visited"><a href="../ctv-giay-to-ban-than-hoc-vien/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Giấy tờ bản thân học viên</a></li>   
					<li class="previous visited"><a href="../ctv-giay-to-nguoi-bao-lanh/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Giấy tờ người bảo lãnh</a></li>    
					<li class="previous visited"><a href="../ctv-nop-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Nộp hồ sơ</a></li>    
					<li class="last active"><a href="#">Hoàn thiện hồ sơ</a></li>
					<?php } else { ?>
					<li class="previous visited">
					  <a href="../thong-tin-hoc-vien/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Thông tin học viên</a>
					</li>    
					<li class="previous visited"><a href="../giay-to-ban-than-hoc-vien/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Giấy tờ bản thân học viên</a></li>   
					<li class="previous visited"><a href="../giay-to-nguoi-bao-lanh/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Giấy tờ người bảo lãnh</a></li>    
					<li class="previous visited"><a href="../nop-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Nộp hồ sơ</a></li>    
					<li class="last active"><a href="#">Hoàn thiện hồ sơ</a></li>
					<?php } ?>
				  </ul>
				</div>
				<div class="contactformcase col-md-9">
      			<?php if (!empty($contactformtitle)) { 
      					echo '<h3>'. esc_html($contactformtitle).'</h3>';
      					} 
      					if(isset($emailSent) && $emailSent == true) { ?>
							<div class="thanks">
								<div class="form-caption">
									<div class="form-title">
										<h1 class="head-title">Hoàn thiện hồ sơ</h1>
									</div>
									<p><?php _e('Cám ơn bạn đã tin tưởng và lựa chọn dịch vụ của chúng tôi.', 'kadencetoolkit');?></p>
									<p><?php _e('Hồ sơ của bạn sẽ được chúng tôi kiểm tra và phản hồi trực tiếp tại đây', 'kadencetoolkit');?></p>								
									<hr>
								</div>
							</div>
						<?php } else { ?>
						
						<?php
						
						$result_phanhoi = $wpdb->get_results("SELECT * FROM `wp_hoidap` WHERE numID = '$numID'", ARRAY_A);
						if(null !== $result_phanhoi) { 
						//$sql_phanhoi = "SELECT * FROM wp_hoidap WHERE numID = '$numID'";		  
						//$result_phanhoi = mysql_query($sql_phanhoi);	
						//if (mysql_num_rows($result_phanhoi)) {
						echo "<div class='form-caption'><div class='form-title'><div class='head-title'>Lịch sử trao đổi</div></div>";
							foreach($result_phanhoi as $row_phanhoi) {
							//while($row_phanhoi = mysql_fetch_array($result_phanhoi)){
								$nguoi_post = $row_phanhoi['name'];
								$noidung_phanhoi = $row_phanhoi['comment'];
								//picking up wordpress date time format
								$date_format = get_option('date_format') . ' ' . get_option('time_format');//converting the login time to wordpress format
								$ngaydang = mysql2date($date_format, $row_phanhoi['ngaydang'], false);
								if($nguoi_post=="Admin") $icon ="<i class='fa fa-info-circle' style='font-size: 50px;color:red;'></i>"; else $icon = "<i class='fa fa-user' style='font-size: 50px;color:green;'></i>";
								echo "<p><div class='comment_avatar'>".$icon."</div><div class='comment_text'><strong>".$nguoi_post." (".$ngaydang.")</strong><br>".nl2br($noidung_phanhoi)."</div></p>";										
							}
						echo "</div>";
						}	

						?>

					<hr>
							<?php
							if(isset($hasError) || isset($captchaError)) { ?>
								<p class="error"><?php _e('Sorry, an error occured.', 'kadencetoolkit');?><p>
							<?php } ?>
							<div class="form-caption">
								<div class="form-title">
									<h1 class="head-title">Hoàn thiện hồ sơ</h1>
								</div>
								<?php //if(is_user_logged_in()) { ?>
								<form action="<?php the_permalink(); ?>?id=<?php echo $numID;?>&school=<?php echo $school;?>" id="contactForm" method="post" enctype="multipart/form-data">
									<div class="contactform">	
									
									<p><div id="upload-drop" class="uk-placeholder uk-text-center">
											<i class="fa fa-cloud-upload uk-icon-medium uk-text-muted uk-margin-small-right"></i>Bạn có thể bổ sung thêm giấy tờ bằng cách kéo thả các file vào đây hoặc <a class="uk-form-file">bấm để chọn<input id="upload-select" name="uploadfile" type="file"></a>.
											<div id="progressbar" class="uk-progress uk-hidden">
												<div class="uk-progress-bar" style="width: 0%;">0%</div>
											</div>
											<div id="upload-results" style="display:none;">
												<div class="uk-alert">
													<ul id="files" class="uk-list"><li class="clearfix" style="clear: both;"></li></ul>
												</div>
											</div>
										</div></p>
										
									<p><textarea name="phanhoi" rows="10" style="width: 100%;padding: 8px;max-width:100%;" placeholder="Bạn có thể gởi phản hồi tại đây..."></textarea></p>
										
		
											<?php if(isset($nameError)) { ?>
												<span class="error"><?php echo esc_html($nameError);?></span>
											<?php } ?>
											<input type="hidden" name="contactName" id="contactName" value="<?php if($hoten) echo $hoten;?>" />
											<input type="hidden" name="email" id="email" value="<?php if($email) echo $email;?>" />
										
						
										<?php if(isset($form_math) && $form_math == 'yes') {
											$one = rand(5, 50);
											$two = rand(1, 9);
											$result = md5($one + $two); ?>
											<p>
												<label for="kad_captcha"><?php echo $one.' + '.$two; ?> = </label>
												<input type="text" name="kad_captcha" id="kad_captcha" class="kad_captcha kad-quarter" />
													<?php if(isset($kad_captchaError)) { ?>
														<label class="error"><?php echo esc_html($kad_captchaError);?></label>
													<?php } ?>
												<input type="hidden" name="hval" id="hval" value="<?php echo esc_attr($result);?>" />
											</p>
										<?php } ?>
										<p>Sau khi đã cập nhật lại các file theo yêu cầu hoặc đã xem nhận xét của chúng tôi hoặc đăng một phản hồi, hãy bấm nút "Hoàn thiện hồ sơ" để xác nhận.</p>
										<hr>
										<p align="center"><a href="../../mpdf/examples/img2pdf.php?id=<?php echo $numID;?>&school=<?php echo $school;?>" target="_blank" class="kad-btn kad-btn-primary" >Xem lại</a>
											<input type="submit" class="kad-btn kad-btn-primary" id="submit" value="<?php _e('Hoàn thiện hồ sơ', 'kadencetoolkit'); ?>"></input>
										</p>
									</div><!-- /.contactform-->
								<input type="hidden" name="submitted" id="submitted" value="true" />
							</form>
							<?php //} //if logged in
		//else { echo "<hr>Bạn chưa đăng nhập"; } ?>
						</div>
      		</div><!--contactform-->
			<?php $upload_nonce = wp_create_nonce( 'tp_upload_nonce' );?>
			<script>

				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {

				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'uploadfile',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...

				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },

				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },

				            allcomplete: function(data) {

				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);

					            $("#upload-results").show();
	
								if( data.status == "1" ){

									$('#files .clearfix').before('<li class="success-hoanthien"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/><br />'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-delete="'+ data.id +'"></a><input type="hidden" class="store-url" name="store-url[]" value="'+ data.url +'" ></li>');

									new tp_remove_data_upload("#files .success-hoanthien .remove_img");//hàm xóa file upload

								} else{
									
									$("#upload-results .uk-alert").addClass('uk-alert-danger');
									$('#files .clearfix').before('<li class="error">'+ data.message +'</li>');
									
								}

				            }
				        };

				        var select = UIkit.uploadSelect($("#upload-select"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);
				            
				    })(jQuery);

				</script>
				<?php get_template_part('templates/content', 'page'); ?>
				
				<?php } ?>
				<?php global $virtue; 
					if(isset($virtue['page_comments']) && $virtue['page_comments'] == '1') {
						comments_template('/templates/comments.php');
					} ?>
		</div><!-- /.main -->
		