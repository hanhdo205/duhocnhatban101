<footer id="containerfooter" class="footerclass" role="contentinfo">
<section class="footter-navigation">
  <div class="container">
  	<div class="row">
  		<?php global $virtue; if(isset($virtue['footer_layout'])) { $footer_layout = $virtue['footer_layout']; } else { $footer_layout = 'fourc'; }
  			if ($footer_layout == "fourc") {
  				if (is_active_sidebar('footer_1') ) { ?> 
					<div class="col-md-3 col-sm-6 footercol1">
					<?php dynamic_sidebar('footer_1'); ?>
					</div> 
            	<?php }; ?>
				<?php if (is_active_sidebar('footer_2') ) { ?> 
					<div class="col-md-3  col-sm-6 footercol2">
					<?php dynamic_sidebar('footer_2'); ?>
					</div> 
		        <?php }; ?>
		        <?php if (is_active_sidebar('footer_3') ) { ?> 
					<div class="col-md-3 col-sm-6 footercol3">
					<?php dynamic_sidebar('footer_3'); ?>
					</div> 
	            <?php }; ?>
				<?php if (is_active_sidebar('footer_4') ) { ?> 
					<div class="col-md-3 col-sm-6 footercol4">
					<?php dynamic_sidebar('footer_4'); ?>
					</div> 
		        <?php }; ?>
		    <?php } else if($footer_layout == "threec") {
		    	if (is_active_sidebar('footer_third_1') ) { ?> 
					<div class="col-md-3 footercol1">
					<?php dynamic_sidebar('footer_third_1'); ?>
					</div> 
            	<?php }; ?>
				<?php if (is_active_sidebar('footer_third_2') ) { ?> 
					<div class="col-md-3 footercol2">
					<?php dynamic_sidebar('footer_third_2'); ?>
					</div> 
		        <?php }; ?>
		        <?php if (is_active_sidebar('footer_third_3') ) { ?> 
					<div class="col-md-6 footercol3">
					<?php dynamic_sidebar('footer_third_3'); ?>
					</div> 
	            <?php }; ?>
			<?php } else {
					if (is_active_sidebar('footer_double_1') ) { ?>
					<div class="col-md-6 footercol1">
					<?php dynamic_sidebar('footer_double_1'); ?> 
					</div> 
		            <?php }; ?>
		        <?php if (is_active_sidebar('footer_double_2') ) { ?>
					<div class="col-md-6 footercol2">
					<?php dynamic_sidebar('footer_double_2'); ?> 
					</div> 
		            <?php }; ?>
		        <?php } ?>
        </div>
	</div>
</section>
<section class="listschool">
		<div class="container">
				<h3>CÁC TRƯỜNG NHẬT NGỮ TẠI NHẬT BẢN</h3>
					<?php //hien thi tung category trong trang home
						
						echo "<div id='container' class='job-cat-arc'><ol>";
						
						foreach ( get_job_listing_categories() as $category ) { 
						
							/*echo '<div class="grid job_categories_title"><a href="' . get_job_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), __($category->name) ) . '" ' . '>Trường Nhật ngữ tại ' . __($category->name).'</a><ul>';*/
							echo '<li class="job_region_title"><a href="' . site_url() . '/cac-truong-tai-nhat/?search_category='. $category->term_id .'" title="' . sprintf( __( "Trường Nhật ngữ tại %s" ), __($category->name) ) . '" ' . '>Trường Nhật ngữ tại ' . __($category->name).'</a>';
							echo '</li>';
						}//foreach($categories	
						echo "</ol></div>";
						?>
		</div>
</section>
<section class="duhocnhatban-intro">
	<div class="container">
		<p>DuhocNhatban.jp là cộng đồng các senpai với trình độ tiếng Nhật N1, N2 hỗ trợ dịch thuật và tự làm hồ sơ du học Nhật Bản.<br>
		Giúp du học sinh và gia đình nộp hồ sơ du học bằng tiếng Nhật trực tiếp vào các trường Nhật Ngữ ở Nhật Bản với Visa du học tại Nhật Bản từ 1-2 năm.<br>
		Giá học phí thấp nhất do các trường công bố. Thông tin và chất lượng các trường được chương trình xác thực. <br>
		Chuyển khoản trực tiếp vào tài khoản công bố của nhà trường. Hệ thống hoàn toàn miễn phí và không thu bất kỳ chi phí nào từ du học sinh. 
		</p>
		<div class="col-md-2"><a rel="nofollow" target="_blank" href="http://www.studyjapan.go.jp/en/"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/fimage1.jpg"></a></div>
		<div class="col-md-2"><a rel="nofollow" target="_blank" href="http://www.mext.go.jp/"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/fimage2.jpg"></a></div>
		<div class="col-md-2"><a rel="nofollow" target="_blank" href="http://www.g-studyinjapan.jasso.go.jp/en/"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/fimage3.jpg"></a></div>
		<div class="col-md-2"><a rel="nofollow" target="_blank" href="http://www.jasso.go.jp/ryugaku/study_j/eju/index.html"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/fimage4.jpg"></a></div>
		<div class="col-md-2"><a rel="nofollow" target="_blank" href="http://www.jasso.go.jp/index.html"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/fimage5.jpg"></a></div>
		<div class="col-md-2"><a rel="nofollow" target="_blank" href="http://www.jasso.go.jp/en/study_j/scholarships/index.html"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/fimage6.jpg"></a></div>
	</div>
</section>
	<div class="container banquyen">
        <div class="footercredits clearfix">
    		
    		<?php if (has_nav_menu('footer_navigation')) :
        	?><div class="footernav clearfix"><?php 
              wp_nav_menu(array('theme_location' => 'footer_navigation', 'menu_class' => 'footermenu'));
            ?></div><?php
        	endif;?>
			<div class="col-md-3"></div>
			<div class="col-md-2 flogo"><a rel="nofollow" target="_blank" href="http://duhocnhatban.jp"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/duhocnhatban.png"></a></div>
			<div class="col-md-2 flogo"><a rel="nofollow" target="_blank" href="http://sagojo.com"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/logo_sagojo.png"></a></div>
			<div class="col-md-2 flogo"><a rel="nofollow" target="_blank" href="http://aline.jp"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/logo_aline.png"></a></div>
			<div class="col-md-3"></div>
			<div style="clear:both;"></div>
        	<p><?php if(isset($virtue['footer_text'])) { $footertext = $virtue['footer_text'];} else {$footertext = '[copyright] [the-year] [site-name] [theme-credit]';}
        		$footertext = str_replace('[copyright]','&copy;',$footertext);
        		$footertext = str_replace('[the-year]',date('Y'),$footertext);
        		$footertext = str_replace('[site-name]',get_bloginfo('name'),$footertext);
        		$footertext = str_replace('[theme-credit]','- WordPress Theme by <a href="http://www.kadencethemes.com/" target="_blank">Kadence Themes</a>',$footertext);
        		 echo do_shortcode($footertext); ?></p>
    	</div>
	</div>
  </div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76983352-1', 'auto');
  ga('send', 'pageview');

</script>
</footer>

<?php wp_footer(); ?>