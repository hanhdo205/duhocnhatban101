<?php global $virtue; ?>
  <div id="topbar" class="topclass">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6 kad-topbar-left">
          <div class="topbarmenu clearfix">
		  <!-- Topbar menu -->
<div id="topbar" class="nojq nojs" role="navigation">
	<div class="quicklinks">
		<?php if ( !is_user_logged_in()) { ?>
		<ul id="menu-topbar-navigation" class="login sf-menu sf-js-enabled"  <?php if (!get_option('users_can_register')) { ?> style="width:150px;"<?php } ?>>
	    	<!-- Login / Register -->						
			<li><a id="dangky" href="<?php echo site_url()?>/dang-ky-thanh-vien/"><?php _e('Đăng ký làm senpai'); ?></a></li>
			<li><a id="login" class="ab-item" aria-haspopup="true" href="#"><?php _e('Log In'); ?></a>
				<div class="ab-sub-wrapper">
					<ul class="ab-submenu">
					<?php echo login_form();?>
					</ul>
				</div>
			</li> 			
		</ul>
		<?php } else { ?>
		<ul id="menu-topbar-navigation" class="login sf-menu sf-js-enabled"  <?php if (!get_option('users_can_register')) { ?> style="width:150px;"<?php } ?>>
	    	<!-- Login / Register -->			
			<li><a id="register" class="ab-item" aria-haspopup="true" href="#"><?php $user_info = get_userdata( get_current_user_id() );
			printf( __( 'Howdy, %1$s' ), $user_info->display_name );?><?php //echo get_wp_user_avatar(get_current_user_id(), 18);?></a>
			<div class="ab-sub-wrapper">
			<ul class="chevon ab-submenu">
				<li>
				<span class="content">
					<p><?php $last_login = get_last_login( get_current_user_id() );
					$userID = get_current_user_id();
					if ($last_login) echo "Đăng nhập lúc: ".$last_login;?>
					<?php
					$row = $wpdb->get_row("SELECT SUM(view) AS solanview FROM wp_hoso WHERE userID = '$userID'", ARRAY_A);
					
					if(null !== $row) {
						$view = $row['solanview'];
						if($view) $remain = '<span style="background-color:red;border-radius:15px;color:#fff;padding:0 5px">'.$view.'</span>';
						else $remain = "";
						
					}
					?>
					<span class="avatar"><?php //echo get_wp_user_avatar(get_current_user_id(), 64);?></span>
					</p>
					<?php if(get_user_role()=="ctv_role"){ ?>
					<p style="margin:10px 0 0;"><a class="ab-item" href="<?php echo site_url()?>/tai-khoan-ctv/">Tài khoản<?php //_e('Edit My Profile'); ?></a></p>
					<p style="margin:0;"><a class="ab-item" href="<?php echo site_url()?>/tao-ho-so-moi/">Tạo hồ sơ mới</a></p>

				  <p style="margin:0;"><a class="ab-item" href="<?php echo site_url()?>/ctv-quan-ly-ho-so/">Quản lý hồ sơ<?php echo $remain; //_e('Ho so du hoc'); ?></a></p>
					<p style="margin:0;"><a class="ab-item" href="<?php echo site_url()?>/thu-nhap-ctv/">Thu nhập CTV</a></p>
					<?php } else { ?>
					<p style="margin:10px 0 0;"><a class="ab-item" href="<?php echo site_url()?>/ho-so-cua-ban/">Tài khoản<?php //_e('Edit My Profile'); ?></a></p>
					<!--<p style="margin:0;"><a class="ab-item" href="<?php echo site_url()?>/chon-truong/">Tạo hồ sơ mới (miễn phí)</a></p>
					
				  <p style="margin:0;"><a class="ab-item" href="<?php echo site_url()?>/dang-ky-lam-ho-so-thong-qua-senpai/">Đăng ký nhờ hỗ trợ (miễn phí)</a></p>
				  <p style="margin:0;"><a class="ab-item" href="<?php echo site_url()?>/quan-ly-ho-so/">Quản lý hồ sơ<?php echo $remain; //_e('Ho so du hoc'); ?></a></p>-->
				  <p style="margin:0;"><a class="ab-item" href="<?php echo site_url()?>/dang-ky-ctv/">Đăng ký làm CTV</a></p>
				  <?php } ?>
					<p style="margin:0;"><a class="ab-item" href="<?php echo wp_logout_url( get_permalink() ); ?>">Thoát<?php //_e('Log out'); ?></a></p>
				</span>
				</li>
				
				<?php if (has_nav_menu('slide_down')) : 
				wp_nav_menu(array('theme_location' => 'slide_down', 'menu_class' => 'ab-submenu', ));
				endif;?>
					
			</ul>
			</div>
			</li> 			 	
		</ul>
		<?php } ?>
          <?php if (has_nav_menu('topbar_navigation')) :
              wp_nav_menu(array('theme_location' => 'topbar_navigation', 'menu_class' => 'sf-menu'));
            endif;?>
	</div>
</div>
<!-- Topbar menu -->
            <?php if(kadence_display_topbar_icons()) : ?>
            <div class="topbar_social">
              <ul>
                <?php $top_icons = $virtue['topbar_icon_menu'];
                foreach ($top_icons as $top_icon) {
                  if(!empty($top_icon['target']) && $top_icon['target'] == 1) {
                    $target = '_blank';
                  } else {
                    $target = '_self';
                  }
                  echo '<li><a href="'.esc_url($top_icon['link']).'" target="'.esc_attr($target).'" title="'.esc_attr($top_icon['title']).'" data-toggle="tooltip" data-placement="bottom" data-original-title="'.esc_attr($top_icon['title']).'">';
                  if(!empty($top_icon['url'])) {
                    echo '<img src="'.esc_url($top_icon['url']).'"/>' ;
                  } else {
                    echo '<i class="'.esc_attr($top_icon['icon_o']).'"></i>';
                  }
                  echo '</a></li>';
                } ?>
              </ul>
            </div>
          <?php endif;
          if(isset($virtue['show_cartcount'])) {
            if($virtue['show_cartcount'] == '1') { 
              if (class_exists('woocommerce')) {
                  global $woocommerce; ?>
                  <ul class="kad-cart-total">
                    <li>
                      <a class="cart-contents" href="<?php echo esc_url(WC()->cart->get_cart_url()); ?>" title="<?php esc_attr_e('View your shopping cart', 'virtue'); ?>">
                        <i class="icon-shopping-cart" style="padding-right:5px;"></i>
                        <?php _e('Your Cart', 'virtue');?>
                        <span class="kad-cart-dash">-</span>
                        <?php if ( WC()->cart->tax_display_cart == 'incl' ) {
                            echo WC()->cart->get_cart_subtotal(); 
                          } else {
                            echo WC()->cart->get_cart_total();
                          }?>
                      </a>
                    </li>
                  </ul>
                <?php } 
              } 
            }?>
          </div>
        </div><!-- close col-md-6 --> 
        <div class="col-md-6 col-sm-6 kad-topbar-right">
          <div id="topbar-search" class="topbar-widget">
            <?php if(kadence_display_topbar_widget()) {
                    if(is_active_sidebar('topbarright')) {
                      dynamic_sidebar('topbarright'); 
                    } 
                } else { 
                  if(kadence_display_top_search()) {
                    get_search_form();
                  } 
            } ?>
        </div>
        </div> <!-- close col-md-6-->
      </div> <!-- Close Row -->
    </div> <!-- Close Container -->
  </div>