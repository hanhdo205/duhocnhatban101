<?php if(is_page('tai-khoan-ctv') || is_page('danh-cho-ctv') || is_page('tao-ho-so-moi') || is_page('tao-ho-so') || is_page('ctv-giay-to-ban-than-hoc-vien') || is_page('ctv-giay-to-nguoi-bao-lanh') || is_page('ctv-nop-ho-so') || is_page('ctv-hoan-thien-ho-so') || is_page('ctv-quan-ly-ho-so') || is_page('ctv-thong-tin-lien-he') || is_page('ctv-tro-giup') || is_page('thong-tin-thanh-toan') || is_page('thu-nhap-ctv') || is_page('ho-so-nhan-hoa-hong')) { ?>
<style>
@media (min-width: 800px) {
.checkout-wrap {
    margin: 10px 0 80px 0!important;
}
ul.checkout-bar {width: 95%!important;}
}
.checkout-wrap {
    max-width: 1111px!important;
}
</style>
<?php get_template_part('templates/head'); ?>
<?php if(is_user_logged_in() && get_user_role()=="ctv_role") { ?>
<?php $user_info = get_userdata( get_current_user_id() );
			//printf( __( 'Howdy, %1$s' ), $user_info->display_name );?>
<?php
					$userID = get_current_user_id();
					$sql = $wpdb->get_results("SELECT SUM(view) AS solanview FROM wp_hoso WHERE userID = '$userID'", ARRAY_A);
					
					if(null !== $sql) {
						foreach($sql as $row) {
							$view = $row['solanview'];
							if($view) $remain = '<span style="background-color:red;border-radius:15px;color:#fff;padding:0 5px">'.$view.'</span>';
							else $remain = '';
						}
					}
					?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/SlidePushMenus/default.css" />	
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/SlidePushMenus/component.css" />	
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">	
<body class="cbp-spmenu-push">
<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
	<h3>Menu</h3>
	<a href="../tai-khoan-ctv/">Thông tin đăng nhập</a>
	<a href="../ctv-thong-tin-lien-he/">Thông tin liên hệ</a>
	<a href="../thong-tin-thanh-toan/">Thông tin thanh toán</a>
	<a href="../tao-ho-so-moi/">Tạo hồ sơ mới</a>
	<a href="../ctv-quan-ly-ho-so/">Quản lý hồ sơ <?php echo $remain; ?></a>
	<a href="../thu-nhap-ctv/">Thu nhập CTV</a>
	<a href="../ctv-tro-giup/">Trợ giúp</a>
	<a href="<?php echo wp_logout_url( get_permalink() ); ?>">Thoát</a>
</nav>		
<div class="container">
	<div class="main">

		<section class="buttonset">
		<button id="showLeftPush"><i class="fa fa-bars"></i><span class="howdy"><?php printf( __( 'Chào bạn, %1$s' ), $user_info->display_name );?></span></button>
		<?php include kadence_template_path(); ?>
		</section>	
	<!-- Classie - class helper functions by @desandro https://github.com/desandro/classie -->
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/SlidePushMenus/classie.js"></script>
	<script>
		var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),				
			showLeftPush = document.getElementById( 'showLeftPush' ),
			body = document.body;		
		showLeftPush.onclick = function() {
			classie.toggle( this, 'active' );
			classie.toggle( body, 'cbp-spmenu-push-toright' );
			classie.toggle( menuLeft, 'cbp-spmenu-open' );
			disableOther( 'showLeftPush' );
		};
		function disableOther( button ) {				
			if( button !== 'showLeftPush' ) {
				classie.toggle( showLeftPush, 'disabled' );
			}				
		}
	</script>
	</div><!-- /.main-->
</div><!-- /.container -->
<?php
} //if logged in
else { echo "Bạn chưa đăng nhập"; }
?>	
		</body>
	</html>
<?php } else { ?>
<?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <div id="wrapper" class="container">
    <?php do_action('get_header');
        get_template_part('templates/header');
    ?>
      <div class="wrap contentclass" role="document">

          <?php include kadence_template_path(); ?>
            
          <?php if (kadence_display_sidebar()) : ?>
            <aside class="<?php echo esc_attr(kadence_sidebar_class()); ?> kad-sidebar" role="complementary">
              <div class="sidebar">
                <?php include kadence_sidebar_path(); ?>
              </div><!-- /.sidebar -->
            </aside><!-- /aside -->
          <?php endif; ?>
          </div><!-- /.row-->
        </div><!-- /.content -->
      </div><!-- /.wrap -->
      <?php do_action('get_footer');
      get_template_part('templates/footer'); ?>
    </div><!--Wrapper-->
  </body>
</html>
<?php } ?>
