<?php
/*
Template Name: Giay to nguoi bao lanh
*/
?>
<?php //if(is_user_logged_in()) { ?>
	<?php global $virtue, $post; 
		$map 				= get_post_meta( $post->ID, '_kad_contact_map', true ); 
		$form_math 			= get_post_meta( $post->ID, '_kad_contact_form_math', true );
		$contactformtitle 	= get_post_meta( $post->ID, '_kad_contact_form_title', true );
		$form 				= get_post_meta( $post->ID, '_kad_contact_form', true );
		//if ($form == 'yes') { ?>
			<script type="text/javascript">jQuery(document).ready(function ($) {$.extend($.validator.messages, {
			        required: "<?php echo __('This field is required.', 'kadencetoolkit'); ?>",
					email: "<?php echo __('Please enter a valid email address.', 'kadencetoolkit'); ?>",
				 });
				$("#contactForm").validate();
			});</script>
			<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.validate.js"></script>
			<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/lightbox/lightbox.js"></script>
		<?php //} 
		if ($map == 'yes') { ?>
		    <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
		    	<?php 	$address 	= get_post_meta( $post->ID, '_kad_contact_address', true ); 
						$maptype 	= get_post_meta( $post->ID, '_kad_contact_maptype', true ); 
						$height 	= get_post_meta( $post->ID, '_kad_contact_mapheight', true );
						$mapzoom 	= get_post_meta( $post->ID, '_kad_contact_zoom', true );
							
							if(!empty($height)) {
								$mapheight = $height;
							} else {
								$mapheight = 300;
							}
							if(!empty($mapzoom)) {
								$zoom = $mapzoom;
							} else {
								$zoom = 15;
							} ?>
		    	<script type="text/javascript">
					jQuery(window).load(function() {
					jQuery('#map_address').gmap3({
						map: {
						    address:"<?php echo $address;?>",
							options: {
			              		zoom:<?php echo $zoom;?>,
								draggable: true,
								mapTypeControl: true,
								mapTypeId: google.maps.MapTypeId.<?php echo esc_attr($maptype);?>,
								scrollwheel: false,
								panControl: true,
								rotateControl: false,
								scaleControl: true,
								streetViewControl: true,
								zoomControl: true
							}
						},
						marker:{
			            values:[
			            		 {address: "<?php echo $address;?>",
						 	    data:"<div class='mapinfo'>'<?php echo $address;?>'</div>",
						 	},
			            ],
			            options:{
			              draggable: false,
			            },
						events:{
			              click: function(marker, event, context){
			                var map = jQuery(this).gmap3("get"),
			                  infowindow = jQuery(this).gmap3({get:{name:"infowindow"}});
			                if (infowindow){
			                  infowindow.open(map, marker);
			                  infowindow.setContent(context.data);
			                } else {
			                  jQuery(this).gmap3({
			                    infowindow:{
			                      anchor:marker, 
			                      options:{content: context.data}
			                    }
			                  });
			                }
			              },
			              closeclick: function(){
			                var infowindow = jQuery(this).gmap3({get:{name:"infowindow"}});
			                if (infowindow){
			                  infowindow.close();
			                }
						  }
						}
			          }
			        });
			      });
			</script>
		    <?php echo '<style type="text/css" media="screen">#map_address {height:'.esc_attr($mapheight).'px; margin-bottom:20px;}</style>';
    }
	//if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
		  //$upload_overrides = array( 'test_form' => false );
		  $attachments = array();
		if(is_user_logged_in()) {
		$current_user = wp_get_current_user();
		$user_id = get_current_user_id();
		$user_email = $current_user->user_email;
		$user_hoten = get_user_meta( $user_id, 'user_ht', true );
		}
		else $user_id = 1;
		if(isset($_GET['id']))
			$numID = $_GET['id'];
		else $numID = mt_rand_str(8);
		if(isset($_GET['school']))
					$school = $_GET['school'];

		$row = $wpdb->get_row("SELECT * FROM `wp_hoso` WHERE numID = '$numID'", ARRAY_A);
			if(null !== $row) {
					$cmndbaolanh = $row['cmndbaolanh'];
					$taikhoan = $row['taikhoan'];
					$thunhap = $row['thunhap'];
					$giaykhac = $row['giaykhac'];
					$cmndbaolanh_duyet = $row['cmndbaolanh_duyet'];
					$taikhoan_duyet = $row['taikhoan_duyet'];
					$thunhap_duyet = $row['thunhap_duyet'];
					$giaykhac_duyet = $row['giaykhac_duyet'];
					$view = $row['view'];					
			}
	if(isset($_POST['submitted'])) {
		if(isset($form_math) && $form_math == 'yes') {
			if(md5($_POST['kad_captcha']) != $_POST['hval']) {
				$kad_captchaError = __('Check your math.', 'kadencetoolkit');
				$hasError = true;
			}
		}
	
	
	if(trim($_POST['upload-cmndbaolanh']) === '') {
		if(!$cmndbaolanh){
			$cmndbaolanhError = __('Bạn chưa upload file.', 'kadencetoolkit');
			$hasError = true;
		}
	} else {
		array_push($attachments, $_POST['upload-cmndbaolanh'] );
		$cmndbaolanh = $_POST['store-cmndbaolanh-url'];
		$cmndbaolanh_duyet='3';
	}
	
	if(trim($_POST['upload-taikhoan']) === '') {
		if(!$taikhoan){
			$taikhoanError = __('Bạn chưa upload file.', 'kadencetoolkit');
			$hasError = true;
		}
	} else {
		array_push($attachments, $_POST['upload-taikhoan'] );
		$taikhoan = $_POST['store-taikhoan-url'];
		$taikhoan_duyet='3';
	}
	
	if(trim($_POST['upload-thunhap']) === '') {
		//$thunhapError = __('Bạn chưa upload file.', 'kadencetoolkit');
		//$hasError = true;
	} else {
		array_push($attachments, $_POST['upload-thunhap'] );
		$thunhap = $_POST['store-thunhap-url'];
		$thunhap_duyet='3';
	}
	
	if(trim($_POST['upload-giaykhac']) === '') {
		//$giaykhacError = __('Bạn chưa upload file.', 'kadencetoolkit');
		//$hasError = true;
	} else {
		array_push($attachments, $_POST['upload-giaykhac'] );
		$giaykhac = $_POST['store-giaykhac-url'];
		$giaykhac_duyet='3';
	}


	/*if(trim($_POST['comments']) === '') {
		$commentError = __('Please enter a message.', 'kadencetoolkit');
		$hasError = true;
	} else {
		if(function_exists('stripslashes')) {
			$comments = stripslashes(trim($_POST['comments']));
		} else {
			$comments = trim($_POST['comments']);
		}
	}*/

	if(!isset($hasError)) {
		if (isset($virtue['contact_email'])) {
			$emailTo = $virtue['contact_email'];
		} else {
			$emailTo = get_option('admin_email');
		}
		$sitename = get_bloginfo('name');
		$subject = '['.esc_html($sitename) . ' ' . __("Contact", "kadencetoolkit").'] '. __("From", "kadencetoolkit") . ' ' . esc_html($name);
			
			if(null !== $row) {
						$query = $wpdb->update('wp_hoso', array('cmndbaolanh'=>$cmndbaolanh,'taikhoan'=>$taikhoan,'thunhap'=>$thunhap,'giaykhac'=>$giaykhac,'cmndbaolanh_duyet'=>$cmndbaolanh_duyet,'taikhoan_duyet'=>$taikhoan_duyet,'thunhap_duyet'=>$thunhap_duyet,'giaykhac_duyet'=>$giaykhac_duyet,'view'=>'0'), array('numID'=>$numID));						
					} else {
						$query = $wpdb->insert('wp_hoso',array('userID'=>$user_id,'numID'=>$numID,'cmndbaolanh'=>$cmndbaolanh,'taikhoan'=>$taikhoan,'thunhap'=>$thunhap,'giaykhac'=>$giaykhac,'cmndbaolanh_duyet'=>$cmndbaolanh_duyet,'taikhoan_duyet'=>$taikhoan_duyet,'thunhap_duyet'=>$thunhap_duyet,'giaykhac_duyet'=>$giaykhac_duyet));						
					}
				
				if($query){
					//wp_mail($emailTo, $subject, $body, $headers, $attachments);
					$emailSent = true;
				}

		
	}

}
//} //if logged in ?>
  	<div id="pageheader" class="titleclass">
		<div class="container">
			<?php //get_template_part('templates/page', 'header'); ?>
		</div><!--container-->
	</div><!--titleclass-->
	<?php if ($map == 'yes') { ?>
        <div id="mapheader" class="titleclass">
            <div class="container">
		        <div id="map_address">
	            </div>
	        </div><!--container-->
        </div><!--titleclass-->
  	<?php } ?>
	<div id="content" class="container">
   		<div class="row">
   		<?php if ($form == 'yes') { ?>
	  		<div id="main" class="main col-md-12" role="main"> 
	  	<?php } else { ?>
      		<div id="main" class="main col-md-12" role="main">
	      <?php } ?>
		  
	      <?php get_template_part('templates/content', 'page'); ?>
	      
			<div class="checkout-wrap">
				  <ul class="checkout-bar">
				  <?php if(get_user_role()=="ctv_role"){ ?>
					<li class="previous visited">
					  <a href="../tao-ho-so/?id=<?php echo $numID;?>">Thông tin học viên</a>
					</li>    
					<li class="previous visited"><a href="../ctv-giay-to-ban-than-hoc-vien/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Giấy tờ bản thân học viên</a></li>   
					<li class="active"><a href="#">Giấy tờ người bảo lãnh</a></li>    
					<li class="next">
					<?php if($cmndbaolanh&&$taikhoan&&$thunhap)
					 { ?><a href="../ctv-nop-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Nộp hồ sơ</a>
					 <?php } else { ?>Nộp hồ sơ<?php } ?></li>    
					<li class="next"><?php if($view)
					 { ?><a href="../ctv-hoan-thien-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Hoàn thiện hồ sơ</a>
					 <?php } else { ?>Hoàn thiện hồ sơ<?php } ?></li>
					<?php } else { ?>
					<li class="previous visited">
					  <a href="../thong-tin-hoc-vien/?id=<?php echo $numID;?>">Thông tin học viên</a>
					</li>    
					<li class="previous visited"><a href="../giay-to-ban-than-hoc-vien/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Giấy tờ bản thân học viên</a></li>   
					<li class="active"><a href="#">Giấy tờ người bảo lãnh</a></li>    
					<li class="next">
					<?php if($cmndbaolanh&&$taikhoan&&$thunhap)
					 { ?><a href="../nop-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Nộp hồ sơ</a>
					 <?php } else { ?>Nộp hồ sơ<?php } ?></li>    
					<li class="next"><?php if($view)
					 { ?><a href="../hoan-thien-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>">Hoàn thiện hồ sơ</a>
					 <?php } else { ?>Hoàn thiện hồ sơ<?php } ?></li>					
					<?php } ?>
				  </ul>
			</div>
      		<?php //if ($form == 'yes') { ?>
      		<div class="contactformcase col-md-9">
      			<?php if (!empty($contactformtitle)) { 
      					echo '<h3>'. esc_html($contactformtitle).'</h3>';
      					} 
      					if(isset($emailSent) && $emailSent == true) { ?>
							<div class="thanks">
								<div class="form-caption">
									<div class="form-title">
										<h1 class="head-title">Giấy tờ người bảo lãnh</h1>
									</div>
									<p><?php _e('Bạn đã cập nhật thành công bản sao giấy tờ cá nhân, chúng tôi sẽ kiểm tra ngay.', 'kadencetoolkit');?></p>
									<p><?php _e('Để tiếp tục, bạn vui lòng chuyển qua bước 4 "Nộp hồ sơ"', 'kadencetoolkit');?></p>
									<hr>
									<p align="center">
									<?php if(get_user_role()=="ctv_role"){ ?>
									<a href="../ctv-nop-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>" class="kad-btn kad-btn-primary" >Chuyển qua bước 4</a></p>
									<?php } else { ?>
									<a href="../nop-ho-so/?id=<?php echo $numID;?>&school=<?php echo $school;?>" class="kad-btn kad-btn-primary" >Chuyển qua bước 4</a></p>
									<?php }?>
									</p>
								</div>
							</div>
						<?php } else {
							
							if(isset($hasError) || isset($captchaError)) { ?>
								<p class="error"><?php _e('Sorry, an error occured.', 'kadencetoolkit');?><p>
							<?php } ?>
							<div class="form-caption">
								<div class="form-title">
									<h1 class="head-title">Giấy tờ người bảo lãnh</h1>
								</div>
								<?php //if(is_user_logged_in()) { ?>
								<form action="<?php the_permalink(); ?>?id=<?php echo $numID;?>&school=<?php echo $school;?>" id="contactForm" method="post" enctype="multipart/form-data">
									<div class="contactform">
										
										<p>
											<label for="file"><?php _e('CMND:', 'kadencetoolkit'); ?> <?php if($cmndbaolanh_duyet==1) {?>(<a href="<?php echo $cmndbaolanh;?>" data-lightbox="cmndbaolanh" title="Chứng minh nhân dân người bảo lãnh"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/check_ok.png"></a>) <?php } else if($cmndbaolanh_duyet==2) {?>(<a href="<?php echo $cmndbaolanh;?>" data-lightbox="cmndbaolanh" title="Chứng minh nhân dân người bảo lãnh"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bad.png"></a>) <?php } else if($cmndbaolanh_duyet==3) {?>(<a href="<?php echo $cmndbaolanh;?>" data-lightbox="cmndbaolanh" title="Chứng minh nhân dân người bảo lãnh"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tick_octagon.png"></a>) <?php } else {?>(<a href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sample/cmnd_baolanh.jpg" data-lightbox="cmndbaolanhmau" title="Chứng minh nhân dân mẫu">xem mẫu</a>)<?php }?></label>
												<?php if(isset($cmndbaolanhError)) { ?>
													<span class="error"><?php echo esc_html($cmndbaolanhError);?></span>
												<?php } ?>
											<?php if(trim($_POST['upload-cmndbaolanh'])!= '') { ?>
											<span id="upload-results-cmndbaolanh">
												<span class="uk-alert">
													<span id="files-cmndbaolanh" class="uk-list"><span class="success-cmndbaolanh"><a href="<?php echo esc_attr($_POST['store-cmndbaolanh-url']);?>" data-lightbox="cmndbaolanh" title="CMND người bảo lãnh"><img src="<?php echo esc_attr($_POST['store-cmndbaolanh-url']);?>" alt="" data-name="<?php echo esc_attr($_POST['store-cmndbaolanh-tmp']);?>" style="max-width:35px;"></a><?php echo esc_attr($_POST['store-cmndbaolanh-tmp']);?></span><span class="clearfix"></span></span>
												</span>
											</span>
											<input type="hidden" class="store-cmndbaolanh-tmp" name="store-cmndbaolanh-tmp" value="<?php if(isset($_POST['store-cmndbaolanh-tmp'])) echo esc_attr($_POST['store-cmndbaolanh-tmp']);?>" >
											<input type="hidden" class="store-cmndbaolanh-url" name="store-cmndbaolanh-url" value="<?php if(isset($_POST['store-cmndbaolanh-url'])) echo esc_attr($_POST['store-cmndbaolanh-url']);?>" >
											<input type="hidden" class="store-cmndbaolanh" name="upload-cmndbaolanh" value="<?php if(isset($_POST['upload-cmndbaolanh'])) echo esc_attr($_POST['upload-cmndbaolanh']);?>" >
											<?php } else {?>
											<input type="file" id="upload-cmndbaolanh" name="cmndbaolanh" class="full">
											<input type="hidden" class="store-cmndbaolanh-tmp" name="store-cmndbaolanh-tmp" value="<?php if(isset($_POST['store-cmndbaolanh-tmp'])) echo esc_attr($_POST['store-cmndbaolanh-tmp']);?>" >
											<input type="hidden" class="store-cmndbaolanh-url" name="store-cmndbaolanh-url" value="<?php if(isset($_POST['store-cmndbaolanh-url'])) echo esc_attr($_POST['store-cmndbaolanh-url']);?>" >
											<input type="hidden" class="store-cmndbaolanh" name="upload-cmndbaolanh" value="<?php if(isset($_POST['upload-cmndbaolanh'])) echo esc_attr($_POST['upload-cmndbaolanh']);?>" >
											<span id="progressbar-cmndbaolanh" class="uk-progress uk-hidden">
												<span class="full uk-progress-bar" style="width: 0%;">0%</span>
											</span>
											<span id="upload-results-cmndbaolanh" style="display:none;">
												<span class="uk-alert">
													<span id="files-cmndbaolanh" class="uk-list"><span class="clearfix"></span></span>
												</span>
											</span>
											<?php } ?>
										</p>
										<hr>
										
										<p>
											<label for="file"><?php _e('Giấy xác nhận tài khoản ngân hàng: ', 'kadencetoolkit'); ?> <?php if($taikhoan_duyet==1) {?>(<a href="<?php echo $taikhoan;?>" data-lightbox="taikhoan" title="Giấy xác nhận tài khoản ngân hàng"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/check_ok.png"></a>) <?php } else if($taikhoan_duyet==2) {?>(<a href="<?php echo $taikhoan;?>" data-lightbox="taikhoan" title="Giấy xác nhận tài khoản ngân hàng"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bad.png"></a>) <?php } else if($taikhoan_duyet==3) {?>(<a href="<?php echo $taikhoan;?>" data-lightbox="taikhoan" title="Giấy xác nhận tài khoản ngân hàng"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tick_octagon.png"></a>) <?php } else {?>(<a href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sample/taikhoan.jpg" data-lightbox="taikhoanmau" title="Giấy xác nhận tài khoản ngân hàng mẫu">xem mẫu</a>)<?php }?></label>
												<?php if(isset($taikhoanError)) { ?>
													<span class="error"><?php echo esc_html($taikhoanError);?></span>
												<?php } ?>
											<?php if(trim($_POST['upload-taikhoan'])!= '') { ?>
											<span id="upload-results-taikhoan">
												<span class="uk-alert">
													<span id="files-taikhoan" class="uk-list"><span class="success-taikhoan"><a href="<?php echo esc_attr($_POST['store-taikhoan-url']);?>" data-lightbox="taikhoan" title="Giấy xác nhận tài khoản ngân hàng"><img src="<?php echo esc_attr($_POST['store-taikhoan-url']);?>" alt="" data-name="<?php echo esc_attr($_POST['store-taikhoan-tmp']);?>" style="max-width:35px;"></a><?php echo esc_attr($_POST['store-taikhoan-tmp']);?></span><span class="clearfix"></span></span>
												</span>
											</span>
											<input type="hidden" class="store-taikhoan-tmp" name="store-taikhoan-tmp" value="<?php if(isset($_POST['store-taikhoan-tmp'])) echo esc_attr($_POST['store-taikhoan-tmp']);?>" >
											<input type="hidden" class="store-taikhoan-url" name="store-taikhoan-url" value="<?php if(isset($_POST['store-taikhoan-url'])) echo esc_attr($_POST['store-taikhoan-url']);?>" >
											<input type="hidden" class="store-taikhoan" name="upload-taikhoan" value="<?php if(isset($_POST['upload-taikhoan'])) echo esc_attr($_POST['upload-taikhoan']);?>" >
											<?php } else {?>
											<input type="file" id="upload-taikhoan" name="taikhoan" class="full">
											<input type="hidden" class="store-taikhoan-tmp" name="store-taikhoan-tmp" value="<?php if(isset($_POST['store-taikhoan-tmp'])) echo esc_attr($_POST['store-taikhoan-tmp']);?>" >
											<input type="hidden" class="store-taikhoan-url" name="store-taikhoan-url" value="<?php if(isset($_POST['store-taikhoan-url'])) echo esc_attr($_POST['store-taikhoan-url']);?>" >
											<input type="hidden" class="store-taikhoan" name="upload-taikhoan" value="<?php if(isset($_POST['upload-taikhoan'])) echo esc_attr($_POST['upload-taikhoan']);?>" >
											<span id="progressbar-taikhoan" class="uk-progress uk-hidden">
												<span class="full uk-progress-bar" style="width: 0%;">0%</span>
											</span>
											<span id="upload-results-taikhoan" style="display:none;">
												<span class="uk-alert">
													<span id="files-taikhoan" class="uk-list"><span class="clearfix"></span></span>
												</span>
											</span>
											<?php } ?>
										<em>(trong tài khoản ít nhất phải từ 450tr trở lên)</em>	
										</p>
										<hr>
										<p>
											<label for="file"><?php _e('Giấy xác nhận việc làm và thu nhập: ', 'kadencetoolkit'); ?> <?php if($thunhap_duyet==1) {?>(<a href="<?php echo $thunhap;?>" data-lightbox="thunhap" title="Giấy xác nhận việc làm và thu nhập"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/check_ok.png"></a>) <?php } else if($thunhap_duyet==2) {?>(<a href="<?php echo $thunhap;?>" data-lightbox="thunhap" title="Giấy xác nhận việc làm và thu nhập"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bad.png"></a>) <?php } else if($thunhap_duyet==3) {?>(<a href="<?php echo $thunhap;?>" data-lightbox="thunhap" title="Giấy xác nhận việc làm và thu nhập"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tick_octagon.png"></a>) <?php } else {?>(<a href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sample/thunhap.jpg" data-lightbox="thunhapmau" title="Giấy xác nhận việc làm và thu nhập mẫu">xem mẫu</a>)<?php }?></label>
												<?php if(isset($thunhapError)) { ?>
													<span class="error"><?php echo esc_html($thunhapError);?></span>
												<?php } ?>
											<?php if(trim($_POST['upload-thunhap'])!= '') { ?>
											<span id="upload-results-thunhap">
												<span class="uk-alert">
													<span id="files-thunhap" class="uk-list"><span class="success-thunhap"><a href="<?php echo esc_attr($_POST['store-thunhap-url']);?>" data-lightbox="thunhap" title="Xác nhận việc làm và thu nhập"><img src="<?php echo esc_attr($_POST['store-thunhap-url']);?>" alt="" data-name="<?php echo esc_attr($_POST['store-thunhap-tmp']);?>" style="max-width:35px;"></a><?php echo esc_attr($_POST['store-thunhap-tmp']);?></span><span class="clearfix"></span></span>
												</span>
											</span>
											<input type="hidden" class="store-thunhap-tmp" name="store-thunhap-tmp" value="<?php if(isset($_POST['store-thunhap-tmp'])) echo esc_attr($_POST['store-thunhap-tmp']);?>" >
											<input type="hidden" class="store-thunhap-url" name="store-thunhap-url" value="<?php if(isset($_POST['store-thunhap-url'])) echo esc_attr($_POST['store-thunhap-url']);?>" >
											<input type="hidden" class="store-thunhap" name="upload-thunhap" value="<?php if(isset($_POST['upload-thunhap'])) echo esc_attr($_POST['upload-thunhap']);?>" >
											<?php } else {?>
											<input type="file" id="upload-thunhap" name="thunhap" class="full">
											<input type="hidden" class="store-thunhap-tmp" name="store-thunhap-tmp" value="<?php if(isset($_POST['store-thunhap-tmp'])) echo esc_attr($_POST['store-thunhap-tmp']);?>" >
											<input type="hidden" class="store-thunhap-url" name="store-thunhap-url" value="<?php if(isset($_POST['store-thunhap-url'])) echo esc_attr($_POST['store-thunhap-url']);?>" >
											<input type="hidden" class="store-thunhap" name="upload-thunhap" value="<?php if(isset($_POST['upload-thunhap'])) echo esc_attr($_POST['upload-thunhap']);?>" >
											<span id="progressbar-thunhap" class="uk-progress uk-hidden">
												<span class="full uk-progress-bar" style="width: 0%;">0%</span>
											</span>
											<span id="upload-results-thunhap" style="display:none;">
												<span class="uk-alert">
													<span id="files-thunhap" class="uk-list"><span class="clearfix"></span></span>
												</span>
											</span>
											<?php } ?>
										<em>(trong giấy xác nhận phải ghi rõ mã số thuế hoặc đính kèm bản sao giấy phép kinh doanh.)</em></p>
										<hr>

										<p>
											<label for="file"><?php _e('Giấy tờ khác: ', 'kadencetoolkit'); ?><?php if($giaykhac_duyet==1) {?>(<a href="<?php echo $giaykhac;?>" data-lightbox="giaykhac" title="Giấy tờ khác"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/check_ok.png"></a>) <?php } else if($giaykhac_duyet==2) {?>(<a href="<?php echo $giaykhac;?>" data-lightbox="giaykhac" title="Giấy tờ khác"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/bad.png"></a>) <?php } else if($giaykhac_duyet==3) {?>(<a href="<?php echo $giaykhac;?>" data-lightbox="giaykhac" title="Giấy tờ khác"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/tick_octagon.png"></a>) <?php }?></label>
												<?php if(isset($giaykhacError)) { ?>
													<span class="error"><?php echo esc_html($giaykhacError);?></span>
												<?php } ?>
											<?php if(trim($_POST['upload-giaykhac'])!= '') { ?>
											<span id="upload-results-giaykhac">
												<span class="uk-alert">
													<span id="files-giaykhac" class="uk-list"><span class="success-giaykhac"><a href="<?php echo esc_attr($_POST['store-giaykhac-url']);?>" data-lightbox="giaykhac" title="Giấy tờ khác"><img src="<?php echo esc_attr($_POST['store-giaykhac-url']);?>" alt="" data-name="<?php echo esc_attr($_POST['store-giaykhac-tmp']);?>" style="max-width:35px;"></a><?php echo esc_attr($_POST['store-giaykhac-tmp']);?></span><span class="clearfix"></span></span>
												</span>
											</span>
											<input type="hidden" class="store-giaykhac-tmp" name="store-giaykhac-tmp" value="<?php if(isset($_POST['store-giaykhac-tmp'])) echo esc_attr($_POST['store-giaykhac-tmp']);?>" >
											<input type="hidden" class="store-giaykhac-url" name="store-giaykhac-url" value="<?php if(isset($_POST['store-giaykhac-url'])) echo esc_attr($_POST['store-giaykhac-url']);?>" >
											<input type="hidden" class="store-giaykhac" name="upload-giaykhac" value="<?php if(isset($_POST['upload-giaykhac'])) echo esc_attr($_POST['upload-giaykhac']);?>" >
											<?php } else {?>
											<input type="file" id="upload-giaykhac" name="giaykhac" class="full">
											<input type="hidden" class="store-giaykhac-tmp" name="store-giaykhac-tmp" value="<?php if(isset($_POST['store-giaykhac-tmp'])) echo esc_attr($_POST['store-giaykhac-tmp']);?>" >
											<input type="hidden" class="store-giaykhac-url" name="store-giaykhac-url" value="<?php if(isset($_POST['store-giaykhac-url'])) echo esc_attr($_POST['store-giaykhac-url']);?>" >
											<input type="hidden" class="store-giaykhac" name="upload-giaykhac" value="<?php if(isset($_POST['upload-giaykhac'])) echo esc_attr($_POST['upload-giaykhac']);?>" >
											<span id="progressbar-giaykhac" class="uk-progress uk-hidden">
												<span class="full uk-progress-bar" style="width: 0%;">0%</span>
											</span>
											<span id="upload-results-giaykhac" style="display:none;">
												<span class="uk-alert">
													<span id="files-giaykhac" class="uk-list"><span class="clearfix"></span></span>
												</span>
											</span>
											<?php } ?>
										<em>( Nếu là doanh nghiệp tư nhân hay chủ doanh nghiệp phải có giấy nộp thuế môn bài 3 năm , bản sao giấy phép kinh doanh, bản sao báo cáo kết quả kinh doanh 3 năm gần nhất ) </em></p>
										<hr>
										<p>
										
										
										<?php if(isset($form_math) && $form_math == 'yes') {
											$one = rand(5, 50);
											$two = rand(1, 9);
											$result = md5($one + $two); ?>
											<p>
												<label for="kad_captcha"><?php echo $one.' + '.$two; ?> = </label>
												<input type="text" name="kad_captcha" id="kad_captcha" class="kad_captcha kad-quarter" />
													<?php if(isset($kad_captchaError)) { ?>
														<label class="error"><?php echo esc_html($kad_captchaError);?></label>
													<?php } ?>
												<input type="hidden" name="hval" id="hval" value="<?php echo esc_attr($result);?>" />
											</p>
										<?php } ?>
										<p align="center">
											<input type="submit" class="kad-btn kad-btn-primary" id="submit" value="<?php _e('Upload giấy tờ', 'kadencetoolkit'); ?>"></input>
										</p>
									</div><!-- /.contactform-->
								<input type="hidden" name="submitted" id="submitted" value="true" />
							</form>
							<?php //} //if logged in
							//else { echo "<hr>Bạn chưa đăng nhập"; } ?>
							</div>
						<?php } ?>
      		</div>
			<!--<div class="free-signup widget-free-signup col-md-3">
				<div class="textwidget"><div class="texttitle">TỰ LÀM HỒ SƠ DU HỌC MIỄN PHÍ LÀ GÌ?</div>
				<span style="font-size:13px;font-style:italic;color:#555;">Là chương trình giúp các bạn có thể <strong>tự làm hồ sơ du học Nhật Bản</strong> thông qua sự hướng dẫn từ website và senpai <strong>mà không phải mất chi phí tư vấn cho công ty môi giới du học</strong><hr>
				<strong>TẠI SAO LẠI LÀ MIỄN PHÍ?</strong> Giúp học sinh và gia đình đỡ đi một chút gánh nặng<hr>
				<strong>CHƯƠNG TRÌNH HOẠT ĐỘNG NHỜ NGUỒN KINH PHÍ NÀO?</strong> Chương trình hoạt động với sự tài trợ kinh phí từ các trường ở Nhật Bản</span></div>
			</div>--><!--contactform-->
      		<?php //} ?>
			<?php $upload_nonce = wp_create_nonce( 'tp_upload_nonce' );?>		        
				<script>
				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar-cmndbaolanh"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {
				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload_cmndbaolanh&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'cmndbaolanh',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...
				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },
				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },
				            allcomplete: function(data) {
				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);
					            $("#upload-results-cmndbaolanh").show();														
								if( data.status == "11" ){
									$('#files-cmndbaolanh .clearfix').before('<span class="success-cmndbaolanh"><a href="'+ data.url +'" data-lightbox="cmndbaolanh" title="CMND người bảo lãnh"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success-cmndbaolanh" data-delete="'+ data.id +'"></a></span>');
									$(".store-cmndbaolanh").val(data.file);
									$(".store-cmndbaolanh-tmp").val(data.name_tmp);
									$(".store-cmndbaolanh-url").val(data.url);
									$("#upload-cmndbaolanh").hide();
									new tp_remove_data_upload("#files-cmndbaolanh .success-cmndbaolanh .remove_img");//hàm xóa file upload
								} else {									
									$("#upload-results-cmndbaolanh .uk-alert").addClass('uk-alert-danger');
									$('#files-khaisinh .clearfix').before('<span class="error">'+ data.message +'</span>');									
								}
				            }
				        };
				        var select = UIkit.uploadSelect($("#upload-cmndbaolanh"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);				            
				    })(jQuery);
				</script>
				<script>
				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar-taikhoan"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {
				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload_taikhoan&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'taikhoan',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...
				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },
				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },
				            allcomplete: function(data) {
				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);
					            $("#upload-results-taikhoan").show();														
								if( data.status == "12" ){
									$('#files-taikhoan .clearfix').before('<span class="success-taikhoan"><a href="'+ data.url +'" data-lightbox="taikhoan" title="Xác nhận tài khoản ngân hàng"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success-taikhoan" data-delete="'+ data.id +'"></a></span>');
									$(".store-taikhoan").val(data.file);
									$(".store-taikhoan-tmp").val(data.name_tmp);
									$(".store-taikhoan-url").val(data.url);
									$("#upload-taikhoan").hide();
									new tp_remove_data_upload("#files-taikhoan .success-taikhoan .remove_img");//hàm xóa file upload
								} else {									
									$("#upload-results-taikhoan .uk-alert").addClass('uk-alert-danger');
									$('#files-khaisinh .clearfix').before('<span class="error">'+ data.message +'</span>');									
								}
				            }
				        };
				        var select = UIkit.uploadSelect($("#upload-taikhoan"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);				            
				    })(jQuery);
				</script>
				<script>
				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar-thunhap"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {
				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload_thunhap&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'thunhap',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...
				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },
				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },
				            allcomplete: function(data) {
				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);
					            $("#upload-results-thunhap").show();														
								if( data.status == "13" ){
									$('#files-thunhap .clearfix').before('<span class="success-thunhap"><a href="'+ data.url +'" data-lightbox="thunhap" title="Xác nhận việc làm và thu nhập"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success-thunhap" data-delete="'+ data.id +'"></a></span>');
									$(".store-thunhap").val(data.file);
									$(".store-thunhap-tmp").val(data.name_tmp);
									$(".store-thunhap-url").val(data.url);
									$("#upload-thunhap").hide();
									new tp_remove_data_upload("#files-thunhap .success-thunhap .remove_img");//hàm xóa file upload
								} else {									
									$("#upload-results-thunhap .uk-alert").addClass('uk-alert-danger');
									$('#files-khaisinh .clearfix').before('<span class="error">'+ data.message +'</span>');									
								}
				            }
				        };
				        var select = UIkit.uploadSelect($("#upload-thunhap"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);				            
				    })(jQuery);
				</script>
				<script>
				    (function($) {
						"use strict";
					    var progressbar = $("#progressbar-giaykhac"),
				            bar         = progressbar.find('.uk-progress-bar'),
				            settings    = {
				            action: "<?php echo esc_url( admin_url('admin-ajax.php'));?>?action=tp_upload_giaykhac&_wpnonce=<?php echo $upload_nonce; ?>", // ajax upload url
				            param: 'giaykhac',//gửi data input file để xử lý trong php
				            allow : "*.(jpg|png|jpeg|gif)",//file cho phép upload
				            type: 'json',//kiểu trả về - json, html...
				            loadstart: function() {
				                bar.css("width", "0%").text("0%");
				                progressbar.removeClass("uk-hidden");
				            },
				            progress: function(percent) {
				                percent = Math.ceil(percent);
				                bar.css("width", percent+"%").text(percent+"%");
				            },
				            allcomplete: function(data) {
				            	bar.css("width", "100%").text("100%");
					            setTimeout(function(){ progressbar.addClass("uk-hidden");}, 250);
					            $("#upload-results-giaykhac").show();														
								if( data.status == "14" ){
									$('#files-giaykhac .clearfix').before('<span class="success-giaykhac"><a href="'+ data.url +'" data-lightbox="giaykhac" title="Giấy tờ khác"><img src="'+ data.url +'" alt="" data-name="'+ data.name_tmp +'" data-id="'+ data.id +'" style="max-width:35px;"/></a>'+ data.name_tmp +' <a href="" class="remove_img uk-alert-close uk-close" data-class=".success-giaykhac" data-delete="'+ data.id +'"></a></span>');
									$(".store-giaykhac").val(data.file);
									$(".store-giaykhac-tmp").val(data.name_tmp);
									$(".store-giaykhac-url").val(data.url);
									$("#upload-giaykhac").hide();
									new tp_remove_data_upload("#files-giaykhac .success-giaykhac .remove_img");//hàm xóa file upload
								} else {									
									$("#upload-results-giaykhac .uk-alert").addClass('uk-alert-danger');
									$('#files-khaisinh .clearfix').before('<span class="error">'+ data.message +'</span>');									
								}
				            }
				        };
				        var select = UIkit.uploadSelect($("#upload-giaykhac"), settings),
				            drop   = UIkit.uploadDrop($("#upload-drop"), settings);				            
				    })(jQuery);
				</script>
				
			</div><!--main col-md-12-->