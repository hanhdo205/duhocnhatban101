<?php
/*
Template Name: Register Page
*/
if(is_user_logged_in()){
	wp_redirect( site_url().'/ho-so-cua-ban/' ); exit; 
} else {
global $virtue;
get_header();
require_once "formvalidator.php";
$show_form=true; 
$successmsg = "";


if(isset($_POST['wp-submit'])){

	error_reporting(E_ALL ^ E_DEPRECATED);
	$validator = new FormValidator();
	
	
	$username = $_POST['user_login'];

	
		if (username_exists( $username )) {		
			$validator->addValidation("user_login","num","Tài khoản này đã có người sử dụng, vui lòng kiểm tra lại.");		
		}		
	
		$validator->addValidation("user_login","req","Vui lòng nhập tên tài khoản.");		
		$validator->addValidation("user_pass","req","Vui lòng nhập mật khẩu.");
		$validator->addValidation("user_pass2","eqelmnt=user_pass","Mật khẩu nhập lại không khớp.");
		$validator->addValidation("user_email","req","Vui lòng nhập địa chỉ email.");
		$validator->addValidation("user_email","email","Địa chỉ email không hợp lệ.");
		$validator->addValidation("user_email2","eqelmnt=user_email","Vui lòng nhập địa chỉ email chính xác.");
		
		$validator->addValidation("user_ht","req","Vui lòng nhập họ và tên.");
		$validator->addValidation("user_bday","req","Vui lòng nhập ngày tháng năm sinh.");
		$validator->addValidation("user_address_dd","req","Vui lòng nhập địa chỉ.");

		$validator->addValidation("user_phone_dd","req","Vui lòng nhập số điện thoại.");
		$validator->addValidation("user_phone_dd","num","Số điện thoại không hợp lệ.");
		$validator->addValidation("facebook","req","Bạn cho chúng tôi xin thêm 1 ít thông tin.");
		$validator->addValidation("viber","req","Bạn cho chúng tôi xin thêm 1 ít thông tin.");
		$validator->addValidation("comments","req","Bạn cho chúng tôi xin thêm 1 ít thông tin.");
	
	if($validator->ValidateForm())
    {
        
		//cap nhat user meta  
		//$random_password = wp_generate_password( $length=8, $include_standard_special_chars=false );
		//$user_id = wp_create_user($_POST['user_login'], $_POST['user_pass'], $_POST['user_email']);
		$userdata = array(
			'user_login'  =>  $_POST['user_login'],
			'user_email'  =>  $_POST['user_email'],
			'user_pass'   =>  $_POST['user_pass']
		);

		$user_id = wp_insert_user( $userdata ) ;		
		$username = $_POST['user_login'];
		$user = get_user_by('login', $username );

		// Redirect URL //
		
		update_user_meta($user_id,'user_ht', $_POST['user_ht']);
		update_user_meta($user_id,'user_bday', $_POST['user_bday']);
		update_user_meta($user_id,'user_sex', $_POST['user_sex']);
		
		
		update_user_meta($user_id,'user_address_dd', $_POST['user_address_dd']);
		update_user_meta($user_id,'user_phone_dd', $_POST['user_phone_dd']);
		update_user_meta($user_id,'user_level_dd', $_POST['user_level_dd']);
		//update_user_meta($user_id,'user_dk_dd', $_POST['user_dk_dd']);
		if (isset($virtue['contact_email'])) {
			$emailTo = $virtue['contact_email'];
		} else {
			$emailTo = get_option('admin_email');
		}
		$sitename = get_bloginfo('name');
		$subject = '[Đăng ký làm cộng tác viên] '. __("From", "kadencetoolkit") . ' <' . $email . '>';
		$name = $_POST['user_ht'];
		$date = $_POST['user_bday'];
		$email = $_POST['user_email'];
		$phonenumber = $_POST['user_phone_dd'];
		$address = $_POST['user_address_dd'];
		$viber = $_POST['viber'];
		$facebook = $_POST['facebook'];
		$japanese_st = $_POST['user_level_dd'];
		$comments = $_POST['comments'];
				
		$body = __('Đăng ký làm cộng tác viên', 'kadencetoolkit')."\n\n<br><br>";
		$body .= __('User ID', 'kadencetoolkit').": $user_id \n\n<br>";
		$body .= __('Họ và tên', 'kadencetoolkit').": $name \n\n<br>";
		$body .= __('Ngày tháng năm sinh', 'kadencetoolkit').": $date \n\n<br>";
		$body .= __('Địa chỉ email', 'kadencetoolkit').": $email \n\n<br>";
		$body .= __('Điện thoại liên hệ', 'kadencetoolkit').": $phonenumber \n\n<br>";
		$body .= __('Địa chỉ liên hệ', 'kadencetoolkit').": $address \n\n<br>";
		$body .= __('Số Viber/Line/Zalo', 'kadencetoolkit').": $viber \n\n<br>";
		$body .= __('Địa chỉ Facebook', 'kadencetoolkit').": $facebook \n\n<br>";
		$body .= __('Trình độ tiếng Nhật', 'kadencetoolkit').": $japanese_st \n\n<br>";
		$body .= __('Tự giới thiệu về bản thân', 'kadencetoolkit').": $comments \n\n<br><br>";
		
		$query = $wpdb->insert('wp_ctv',array('agent'=>$user_id,'hoten'=>$name,'email'=>$email,'dienthoai'=>$phonenumber,'birthday'=>$date,'address'=>$address,'viber'=>$viber,'facebook'=>$facebook,'japanese'=>$japanese,'comments'=>$comments));
		if($query){
			$headers[] = 'From: Đăng ký làm cộng tác viên <' . $email . '>' . "\r\n";
			$headers[] = 'Cc: '.$name.' <' . $email . '>' . "\r\n";		
			wp_mail($emailTo, $subject, $body, $headers);
			$emailSent = true;
		}
        $show_form=false;
		if ( !is_wp_error( $user ) )
		{
			wp_clear_auth_cookie();
			wp_set_current_user ( $user->ID );
			wp_set_auth_cookie  ( $user->ID );

			//$redirect_to = site_url()."/dang-ky-thanh-vien/";
			//wp_safe_redirect( $redirect_to );
			//exit();
		}
		
    }
    else
    {
		$fail = true;
		//echo '<style> input[type="text"]{border:1px solid red;}</style>';
        $errorvar = "<p class=\"error\">";
 
        $error_hash = $validator->GetErrors();
        foreach($error_hash as $inpname => $inp_err)
        {
			echo '<style> #'.$inpname.' {border:1px solid red;}</style>'; 
          $errorvar .= "<strong>Lỗi</strong>: $inp_err\n<br>";
        }
		$errorvar .= "</p>";
    }

}
if(false == $show_form) {
			?>
				<style>
				div#theme-my-login{
					background: url(<?php echo get_stylesheet_directory_uri() ?>/img/bg_login_sucess.png) no-repeat !important;
					background-position: 5px 0px;
					margin-top: 10px;
					min-height: 365px;
					padding: 0px 30px;
					padding-top: 30px;
					min-height: 365px;
				}
				.login p.message {
					padding: 5px;
					border: 1px solid #e6db55;
					background-color: rgba(255,255,224,0.5);
					color: #333;
					width: 45%;
					}
				</style>
				
				  	<div id="pageheader" class="titleclass">
						<div class="container">
							<div class="page-header">
							<h2>Form đăng ký làm cộng tác viên (senpai)</h2>
							</div>
						</div><!--container-->
					</div><!--titleclass-->
				<div id="content" class="container">
				<div class="row">
					<div class="login" id="theme-my-login">
						<p class="message">Cảm ơn bạn đã đăng ký trở thành senpai hỗ trợ làm hồ sơ du học.</p>
						<p>Thông tin đăng ký của bạn đã hoàn tất. Bạn có thể click vào đây để vào trang quản lý chỉ dành riêng cho senpai.</p>
						<p><a href="http://duhocnhatban.jp/danh-cho-ctv/">http://duhocnhatban.jp/danh-cho-ctv/</a></p>
					</div>
				</div><!-- .entry-content -->
			<?php }

if(true == $show_form) {
?>
<!-- Datetime picker -- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/bootstrap331.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/moment-with-locales.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/bootstrap-datetimepicker41328.js"></script>
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/datetime-picker.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<script type="text/javascript">
		var j211 = $.noConflict(true);
	</script>
	<!-- Datetime picker -->
  	<div id="pageheader" class="titleclass">
		<div class="container">
			<?php //get_template_part('templates/page', 'header'); ?>
		</div><!--container-->
	</div><!--titleclass-->
<div id="content" class="container">
   	<div class="row">
		<div class="contactformcase col-md-9 login">
			<div class="form-caption">
				<div class="form-title">
					<h2 class="head-title">Form đăng ký làm cộng tác viên (senpai)</h2>
				</div>
				<p class="message">Đăng ký trở thành cộng tác viên tiếng Nhật có N1,N2,N3 - hỗ trợ du học sinh làm hồ sơ du học Nhật Bản</p>
				<?php if($fail) echo $errorvar;?>
				<form name="registerform" id="registerform" action="<?php echo site_url();?>/dang-ky-thanh-vien/" method="post">
					<div class="contactform">
						<p>
							<label for="user_login">Tài khoản <span class="description">(*)</span></label>
							<input type="text" name="user_login" id="user_login" class="input full" value="<?php echo $_POST['user_login'];?>" size="20">
						</p>
						<hr>
						<p>
							<label for="user_login">Mật khẩu <span class="description">(*)</span></label>
							<input type="password" name="user_pass" id="user_pass" class="input full" value="<?php echo $_POST['user_pass'];?>" size="20">
						</p>
						<hr>
						<p>
							<label for="user_login">Xác nhận mật khẩu <span class="description">(*)</span></label>
							<input type="password" name="user_pass2" id="user_pass2" class="input full" value="<?php echo $_POST['user_pass2'];?>" size="20">
						</p>
						<hr>
						<p>
							<label for="user_email">E-mail <span class="description">(*)</span></label>
							<input type="text" name="user_email" id="user_email" class="input full" value="<?php echo $_POST['user_email'];?>" size="20">
						</p>
						<hr>
						<p>
							<label for="user_email2">Xác nhận lại Email <span class="description"> (*)</span></label>
							<input type="text" name="user_email2" id="user_email2" class="input full" value="<?php echo $_POST['user_email2'];?>" size="20">
						</p>
						<hr>
						<p>
							<label for="user_ht">Họ và tên <span class="description"> (*)</span></label>
							<input type="text" name="user_ht" id="user_ht" class="input full" value="<?php echo $_POST['user_ht'];?>" size="20">
						</p>
						<hr>	
						<p>
							<td><label for="user_bday"><?php _e( 'Ngày tháng năm sinh','theme-my-login' );?><?php _e( ' (*)' ); ?></span></label></td>
							<td><input type="text" name="user_bday" id="user_bday" value="<?php echo $_POST['user_bday']?>" class="regular-text full" /></td>
						</p>
						<hr>
						<p>
							<label for="user_sex">Giới tính</label>
							<input type="radio" name="user_sex" value="1" <?php if ( !isset($_POST['user_sex']) || $_POST['user_sex'] == "1" ) echo 'checked';?>>Nam&nbsp;&nbsp;<input type="radio" name="user_sex" value="2" <?php if ( $_POST['user_sex'] == "2" ) echo 'checked';?>>Nữ		
						</p>
						<hr>	
						<p>
							<label for="user_phone_dd">Số điện thoại liên hệ <span class="description"> (*)</span></label>
							<input type="tel" name="user_phone_dd" id="user_phone_dd" value="<?php echo $_POST['user_phone_dd'];?>" class="input full"><span id="spnPhoneStatus"></span>
						</p>
						<hr>
						<p>
							<label for="user_address_dd">Địa chỉ liên hệ hiện nay <span class="description"> (*)</span></label>
							<input type="text" name="user_address_dd" id="user_address_dd" class="input full" value="<?php echo $_POST['user_address_dd'];?>" size="20">
						</p>
						<hr>
						<p>
							<label for="facebook">Địa chỉ Facebook<span class="description"> (*)</span></label>
							<input type="text" name="facebook" id="facebook" value="<?php echo $_POST['facebook'];?>" class="input full"><span id="spnfacebookStatus"></span>
						</p>
						<hr>
						<p>
							<label for="viber">Số Viber/Line/Zalo<span class="description"> (*)</span></label>
							<input type="text" name="viber" id="viber" value="<?php echo $_POST['viber'];?>" class="input full"><span id="spnviberStatus"></span>
						</p>
						<hr>
						<p>
							<label for="user_level_dd">Trình độ tiếng Nhật</label>
								
							<select name="user_level_dd" id="user_level_dd">
								  <option value="1" <?php if ( $_POST['user_level_dd'] == "1" ) echo 'selected="selected"';?>>N1</option>
								  <option value="2" <?php if ( $_POST['user_level_dd'] == "2" ) echo 'selected="selected"';?>>N2</option>
								  <option value="3" <?php if ( $_POST['user_level_dd'] == "3" ) echo 'selected="selected"';?>>N3</option>
								  <option value="4" <?php if ( $_POST['user_level_dd'] == "4" ) echo 'selected="selected"';?>>N4</option>
								  <option value="5" <?php if ( $_POST['user_level_dd'] == "5" ) echo 'selected="selected"';?>>N5</option>
								  <option value="6" <?php if ( $_POST['user_level_dd'] == "6" ) echo 'selected="selected"';?>>Chưa biết tiếng Nhật</option>
								</select>
						</p>
						<hr>
						<p>
							<label for="commentsText"><?php _e('Tự giới thiệu về bản thân:', 'kadencetoolkit'); ?></label>
								<?php if(isset($commentError)) { ?>
									<span class="error"><?php echo esc_html($commentError);?></span>
								<?php } ?>
							<textarea name="comments" id="commentsText" rows="10" class="required requiredField" placeholder="một số thông tin bạn muốn chia sẻ, ví dụ như trình độ học vấn cao nhất, công việc hiện tại, kinh nghiệm sống, học tập, làm việc tại Nhật Bản,v.v..." ><?php if(isset($_POST['comments'])) { if(function_exists('stripslashes')) { echo esc_textarea(stripslashes($_POST['comments'])); } else { echo esc_textarea($_POST['comments']); } } ?></textarea>
						</p>
						<!--<p>
					
							<i><input type="checkbox" name="user_dk_dd" id="user_dk_dd" value="1"  <?php if ( isset($_POST['user_dk_dd']) ) echo 'checked=checked';?>>&nbsp;&nbsp;<a href="<?php echo site_url();?>/noi-quy-clb-dong-du/" target="_blank">Tôi đã xem và đồng ý với các điều khoản của nội quy Câu lạc bộ.</a></i>
						</p>
						
							
						<p id="reg_passmail">Mật khẩu sẽ được gởi tới email của bạn.</p>-->
						<hr>
						<p align="center" class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="kad-btn kad-btn-primary" value="Đăng ký làm CTV"></p>
					</div>
				</form>
			</div>
		</div>
		<div class="free-signup widget-free-signup col-md-3">
			<div class="textwidget"><h3 class="texttitle">Cộng tác viên tiếng Nhật</h3>
				<span style="font-size:13px;font-style:italic;color:#555;"><p>Công việc: Hỗ trợ du học sinh làm hồ sơ du học Nhật Bản.</p>
				<p>Có chứng chỉ tiếng Nhật N3,N2,N1 hoặc khả năng tương đương.</p>
				<p>Ưu tiên ứng viên đã từng sống, học tập hoặc làm việc tại Nhật Bản</p>
				<p><strong>Lương:</strong> thỏa thuận.</p>
				<p><strong>Hình thức làm việc:</strong> online.</p>
				<p>Việc làm không ràng buộc thời gian, làm khi rảnh, thu nhập tốt.</p>
				<p>Sau khi bạn đăng ký, chúng tôi sẽ lưu dữ liệu của bạn vào danh sách cộng tác viên và đội ngũ quản lý hồ sơ cộng tác viên sẽ liên lạc với bạn khi có công việc phù hợp.</p>	
				</span></div>	
		</div>
	</div><!-- .row -->
<script type="text/javascript">
			j211(function () {
						j211('#user_bday').datetimepicker({
							locale: 'vi',
							viewMode: 'years',
							format: 'DD/MM/YYYY',
							icons: {
								 previous: "fa fa-arrow-left",
								 next: "fa fa-arrow-right",
							}               
						});
			});
			</script>
<?PHP

}//true == $show_form

}
?>