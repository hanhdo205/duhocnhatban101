<?php
  $bg = array('img_01bk.jpg', 'img_02bk.jpg', 'img_03bk.jpg', 'img_04bk.jpg', 'img_05bk.jpg', 'img_06bk.jpg', 'img_07bk.jpg', 'img_08bk.jpg', 'img_09bk.jpg', 'img_10bk.jpg' ); // array of filenames

  $i = rand(0, count($bg)-1); // generate random number size of the array
  $selectedBg = "$bg[$i]"; // set variable equal to which random filename was chosen
?>

<style type="text/css">

.sect-banner{
background: url(<?php echo get_stylesheet_directory_uri();?>/assets/img/slider/<?php echo $selectedBg; ?>) no-repeat no-repeat center center;
	/*background-color: #ff7a7a;
	background-blend-mode: multiply;*/
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover ;
	min-height:599px;
	padding: 370px 0 50px;
}
@media (max-width:991px){
	.sect-banner{
		min-height:auto;
		padding: 20px 0 20px;
	}
}
</style>
<section class="sect-banner">
	<div id="search" class="container">		
		<?php search_job_form ();?>
	</div>
	<h3 class="home-all-scholl-link"><a href="<?php echo site_url();?>/cac-truong-tai-nhat">Xem toàn bộ danh sách các trường</a></h3>
</section>
<section class="intro-bg">
	<div class="container intro">
		<h1 class="350truong">Hơn 350 trường tiếng Nhật. Chỉ với 01 bộ hồ sơ duy nhất. Và tất cả là miễn phí.</h1>
	</div>
</section>
<section class="truongvp-bg">
	<div class="container truongvp">
				<div class="col-md-4 col-sm-4 canhgiua image-cropper">
					<img src="wp-content/themes/virtue-child/assets/img/huongdanhs.jpg">
					<h5>HƯỚNG DẪN LÀM HỒ SƠ 100% MIỄN PHÍ</h5>
					<p style="text-align: justify;">Chương trình sẽ giúp các bạn học sinh và gia đình tự làm hồ sơ du học Nhật Bản thông qua sự hướng dẫn từ website và senpai (các đàn anh, đàn chị, những người đã từng sống, học tập hoặc làm việc tại Nhật Bản) mà không phải mất chi phí tư vấn cho công ty môi giới du học.</p>
				</div>
				<div class="col-md-4 col-sm-4 canhgiua image-cropper">
					<img src="wp-content/themes/virtue-child/assets/img/kiemtrahs.jpg">
					<h5>KIỂM TRA HỒ SƠ 100% MIỄN PHÍ</h5>
					<p style="text-align: justify;">Sau khi nhận được bản sao các hồ sơ của bạn, chúng tôi sẽ kiểm tra, xét duyệt và phản hồi trực tiếp trên hệ thống. Các bạn có thể chỉnh sửa, cập nhật hồ sơ trực tiếp.</p>
				</div>
				<div class="col-md-4 col-sm-4 canhgiua image-cropper">
					<img src="wp-content/themes/virtue-child/assets/img/nophs.jpg">
					<h5>NỘP HỒ SƠ 100% MIỄN PHÍ</h5>
					<p style="text-align: justify;">Những hồ sơ được chúng tôi phản hồi là hợp lệ, các bạn có thể gửi bản gốc sang Nhật. Chúng tôi sẽ hỗ trợ các bạn làm các thủ tục cần thiết để nhận được Giấy Chứng Nhận Tư Cách Lưu Trú (COE) và các bước tiếp theo để có thể sang Nhật du học.</p>
				</div>
	</div>		
</section>
<section class="tophome-bg">
	<div class="container truongvp">
			<div class="col-md-2 col-sm-2"></div>
			<div class="col-md-8 col-sm-8"><h2>Với sự hướng dẫn từ các senpai (đàn anh-đàn chị) đã từng du học và làm việc tại Nhật bản, trình độ tiếng Nhật N2,N1</h2>
			<div style="clear:both;margin-bottom:65px;"></div>
			<a href="<?php echo site_url();?>/dang-ky-lam-ho-so-thong-qua-senpai/" title="Đăng ký nhờ hỗ trợ thông qua senpai" class="kad-btn kad-btn-primary" style="min-width:230px;margin-top: 5px;"><span style="font-size:25px;display:block;">ĐĂNG KÝ</span>nhờ senpai hướng dẫn</a>
			<a href="<?php echo site_url();?>/portfolio/" title="Xem danh sách senpai" class="kad-btn kad-btn-primary" style="min-width:230px;margin-top: 5px;"><span style="font-size:25px;display:block;">XEM</span>danh sách senpai</a></div>
			<div class="col-md-2 col-sm-2"></div>
	</div>
</section>	

<?php  global $virtue; 
			if(isset($virtue['mobile_switch'])) {
				$mobile_slider = $virtue['mobile_switch']; 
			} else { 
				$mobile_slider = '0';
			}
			if(isset($virtue['choose_slider'])) {
				$slider = $virtue['choose_slider'];
			} else {
				$slider = 'mock_flex';
			}
			if($mobile_slider == '1') {
		 		$mslider = $virtue['choose_mobile_slider'];
				if ($mslider == "flex") {
					get_template_part('templates/mobile_home/mobileflex', 'slider');
				} else if ($mslider == "video") {
					get_template_part('templates/mobile_home/mobilevideo', 'block');
				} 
			}
			if ($slider == "flex") {
					get_template_part('templates/home/flex', 'slider');
			} else if ($slider == "thumbs") {
					get_template_part('templates/home/thumb', 'slider');
			} else if ($slider == "fullwidth") {
					get_template_part('templates/home/flex', 'slider-fullwidth');
			} else if ($slider == "latest") {
					get_template_part('templates/home/latest', 'slider');
			} else if ($slider == "carousel") {
					get_template_part('templates/home/carousel', 'slider');
			} else if ($slider == "video") {
					get_template_part('templates/home/video', 'block');
			} else if ($slider == "mock_flex") {
					get_template_part('templates/home/mock', 'flex');
			}
			$show_pagetitle = false;
			if(isset($virtue['homepage_layout']['enabled'])){
				$i = 0;
				foreach ($virtue['homepage_layout']['enabled'] as $key=>$value) {
					if($key == "block_one") {
						$show_pagetitle = true;
					}
					$i++;
					if($i==2) break;
				}
			} 
			if($show_pagetitle == true) { ?>
				<div id="homeheader" class="welcomeclass">
					<div class="container">
						<?php get_template_part('templates/page', 'header'); ?>
					</div>
				</div><!--titleclass-->
			<?php } ?>

    <div id="content" class="container homepagecontent">
   		<div class="row">
          	<div class="main <?php echo esc_attr(kadence_main_class()); ?>" role="main">
          	<div class="entry-content" itemprop="mainContentOfPage">

      		<?php if(isset($virtue['homepage_layout']['enabled'])) { 
      			$layout = $virtue['homepage_layout']['enabled']; 
      		  } else {
      		  	$layout = array("block_one" => "block_one", "block_four" => "block_four"); 
      		  }

				if ($layout):

				foreach ($layout as $key=>$value) {

				    switch($key) {

				    	case 'block_one':
						   	if($show_pagetitle == false) {?>
					    	  <div id="homeheader" class="welcomeclass">
									<?php get_template_part('templates/page', 'header'); ?>
								</div><!--titleclass-->
							<?php }

					    break;
						case 'block_four':
							if(is_home()) {
									if(kadence_display_sidebar()) {
										$display_sidebar = true; 
										$fullclass = '';
									} else {
										$display_sidebar = false;
										$fullclass = 'fullwidth';
									} 
									if(isset($virtue['home_post_summery']) and ($virtue['home_post_summery'] == 'full')) {
										$summery = "full";
										$postclass = "single-article fullpost";
									} else {
										$summery = "summery";
										$postclass = "postlist";
									} ?>
								<div class="homecontent <?php echo esc_attr($fullclass); ?>  <?php echo esc_attr($postclass); ?> clearfix home-margin"> 
									<?php while (have_posts()) : the_post(); 
								  			if($summery == 'full') {
												if($display_sidebar){
													get_template_part('templates/content', 'fullpost'); 
												} else {
													get_template_part('templates/content', 'fullpostfull');
												}
											} else {
												if($display_sidebar){
												 	get_template_part('templates/content', get_post_format()); 
												 } else {
												 	get_template_part('templates/content', 'fullwidth');
												 }
											}
										endwhile; 
									
										//Page Navigation
								        if ($wp_query->max_num_pages > 1) :
								        	virtue_wp_pagenav();
								        endif; ?>
								</div> 
							<?php } else { ?>
								<div class="homecontent clearfix home-margin"> 
									<?php get_template_part('templates/content', 'page'); ?>
								</div>
							<?php }
						break;
						case 'block_five':
								get_template_part('templates/home/blog', 'home'); 
						break;
						case 'block_six':
								get_template_part('templates/home/portfolio', 'carousel');		 
						break; 
						case 'block_seven':
								get_template_part('templates/home/icon', 'menu');		 
						break;
						case 'block_twenty':
								get_template_part('templates/home/icon', 'menumock');		 
						break;  
					}
				}
			endif; ?>  
			</div>
		</div><!-- /.main -->
		<div class="container popular">
	<h2>Các trường Nhật ngữ đã được xác minh thông tin tại các tỉnh thành ở Nhật bản</h2>
	<div class="row">
		
		<?php //hien thi tung category trong trang home
					$cat_args = array(
					  'orderby' => 'count',
					  'order' => 'DESC',
					  'child_of' => 0,
					  'exclude'  => '1',
					  //'number' => '12'
					);
					$categories =   get_job_categories($cat_args); 					
					foreach($categories as $category) { 					
						echo '<div class="col-md-3 col-lg-3 school-list"><a href="' . site_url() . '/cac-truong-tai-nhat/?search_category='. $category->term_id .'" title="' . sprintf( __( "Trường Nhật ngữ tại %s" ), __($category->name) ) . '" ' . '><img src="'.get_stylesheet_directory_uri().'/assets/img/popular/'.$category->slug.'.jpg" class="popular-img"><div class="category-name">' . __($category->name).'</a><span class="category-count">'.$category->count.' trường</span></div></div>';
					}//foreach($categories					
					?>	

	</div>
</div>