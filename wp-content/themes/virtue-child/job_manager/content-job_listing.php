<?php global $post; ?>
<li <?php job_listing_class(); ?> data-longitude="<?php echo esc_attr( $post->geolocation_lat ); ?>" data-latitude="<?php echo esc_attr( $post->geolocation_long ); ?>">
	<?php if ( is_position_featured( $post ) ) { echo '<div class="ribbon red"><span>hàng đầu</span></div>';}?>
	<div class="col-md-3 listing-img"><div class="row"><a href="<?php the_job_permalink(); ?>"><?php the_company_logo(); ?></a></div></div>
	<div class="col-md-9 listing-content"><div class="row">
	<a href="<?php the_job_permalink(); ?>">
	<div class="col-md-8 listing-content">
		<span class="school-title">
			<?php //the_company_logo(); ?>
			<div class="position">
				<h3 style="display:inline-block;"><strong><?php the_title(); ?></strong></h3><span class="content <?php echo get_the_job_type() ? $r=sanitize_title( get_the_job_type()->slug ) : $r=0; ?>"><?php echo do_shortcode('[star rating="'.$r.'" max="5"]');?><?php if ( is_position_featured( $post ) ) { echo "<i class=\"fa fa-thumbs-up\" style=\"color:#0283DF;\" aria-hidden=\"true\"></i>";}?></span>
				<div class="company">
					<?php the_company_name( '<strong>', '</strong> ' );if ( is_position_featured( $post ) ) {echo "<span class=\"certificate\">ĐÃ XÁC NHẬN</span>";} ?>
					<?php the_company_tagline( '<span class="tagline">', '</span>' ); ?>				
				</div>
				
			</div>
			<!--<div class="location">
				<?php //the_job_location( false ); ?>
			</div>-->			
		</span>
		<i class="fa fa-map-marker" aria-hidden="true"></i><div class="khuvuc"><?php display_job_tags_data(); display_categories_data();?></div>
		<div class="kytucxa"><?php the_jobtimes();?></div>
		<div class="vietnam"><?php the_students();?></div>		
	</div>
	<div class="col-md-4 custom-rating">		
		<?php 
		echo average_rating();
		echo "<div class=\"css-button\" style=\"vertical-align:middle\"><span>Xem học phí </span></div >";
		do_action( 'job_listing_meta_end' ); ?>
	</div>
	<div style="clear:both;"></div>
	<div class="col-md-12 listing-banner"><div class="margin-right-15">
		<?php 
			$nowM = date('m');
			$nowY = date('Y');
			$nextY = date('Y', strtotime('+1 year'));
			$thang3 = 3;
			$thang6 = 6;
			$thang9 = 9;
			$thang11 = 11;
			$termthang9 = 27;
			$termthang11 = 28;
			$termthang3 = 29;
			$termthang6 = 30;
			//echo (int)$nowM;
			
			$thang1 = false;$thang4 = false;$thang7 = false;$thang10 = false;
			$term_list = wp_get_post_terms($post->ID, 'job_listing_region', array("fields" => "ids"));
			foreach($term_list as $term){
				if(($term == $termthang9)&&(int)$nowM <= $thang9) $thang1=true;
				else if(($term == $termthang11)&&(int)$nowM <= $thang11) $thang4=true;
				else if(($term == $termthang3)&&(int)$nowM <= $thang3) $thang7=true;
				else if(($term == $termthang6)&&(int)$nowM <= $thang6) $thang10=true;
			}
			if ( is_position_filled() ) { ?>
					<div class="listing-footer">
						<div class="col-md-8" style="padding-right:0;">
							<div class="dangtuyen"><span class="ngungnhan">Hiện tại trường ngưng nhận du học sinh Việt Nam</span></div>
							<div class="available">&nbsp;<span style="float:right;"></div>
						</div>

						<div class="col-md-4" style="text-align:right;">
							<div class="dangtuyen">Học phí 1 năm từ</div>
							<div class="available"><span class="hocphi"><?php display_job_salary_data();?></span> đồng</div>
						</div>
					</div>
			<?php }
			else {
			if( (int)$nowM <= $thang3 ){
				if($thang7) {
				$thang = '07/'.$nowY;?>
				<?php if(the_no_students()) { ?>
					<div class="nophoso">Có <?php echo the_no_students();?> du học sinh đã nộp hồ sơ xét tuyển cho kì nhập học tháng <?php echo $thang;?></div>
				<?php } ?>
				<div class="listing-footer">
					<div class="col-md-8" style="padding-right:0;">
						<div class="dangtuyen">Hiện tại trường đang tuyển sinh cho kì nhập học tháng <?php echo $thang;?></div>
						<div class="available">Còn chỗ cho <?php echo the_available_students();?> du học sinh Việt Nam</div>
					</div>
					<div class="col-md-4" style="text-align:right;">
						<div class="dangtuyen">Học phí 1 năm từ</div>
						<div class="available"><span class="hocphi"><?php display_job_salary_data();?></span> đồng</div>
					</div>
				</div>
			<?php
			} else { ?>
			<div class="listing-footer">
				<div class="col-md-8" style="padding-right:0;">
					<div class="dangtuyen">&nbsp;</div>
					<div class="available">&nbsp;</div>
				</div>
				<div class="col-md-4">
					<div class="dangtuyen"><span style="float:right;">Học phí 1 năm từ</span></div>
					<div class="available"><span style="float:right;"><span class="hocphi"><?php display_job_salary_data();?></span> đồng</span></div>
				</div>
			</div>
			<?php }
			}
			else if( (int)$nowM <= $thang6 ){
				if($thang10) {
				$thang = '10/'.$nowY;?>
				<?php if(the_no_students()) { ?>
					<div class="nophoso">Có <?php echo the_no_students();?> du học sinh đã nộp hồ sơ xét tuyển cho kì nhập học tháng <?php echo $thang;?></div>
				<?php } ?>				<div class="listing-footer">
					<div class="col-md-8" style="padding-right:0;">
						<div class="dangtuyen">Hiện tại trường đang tuyển sinh cho kì nhập học tháng <?php echo $thang;?></div>
						<div class="available">Còn chỗ cho <?php echo the_available_students();?> du học sinh Việt Nam</div>
					</div>
					<div class="col-md-4" style="text-align:right;">
						<div class="dangtuyen">Học phí 1 năm từ</div>
						<div class="available"><span class="hocphi"><?php display_job_salary_data();?></span> đồng</div>
					</div>
				</div>
			<?php
			} else { ?>
			<div class="listing-footer">
				<div class="col-md-8" style="padding-right:0;">
					<div class="dangtuyen">&nbsp;</div>
					<div class="available">&nbsp;</div>
				</div>
				<div class="col-md-4">
					<div class="dangtuyen"><span style="float:right;">Học phí 1 năm từ</span></div>
					<div class="available"><span style="float:right;"><span class="hocphi"><?php display_job_salary_data();?></span> đồng</span></div>
				</div>
			</div>
			<?php }
			}
			else if( (int)$nowM <= $thang9 ){
				if($thang1) {
				$thang = '01/'.$nextY;?>
				<?php if(the_no_students()) { ?>
					<div class="nophoso">Có <?php echo the_no_students();?> du học sinh đã nộp hồ sơ xét tuyển cho kì nhập học tháng <?php echo $thang;?></div>
				<?php } ?>				<div class="listing-footer">
					<div class="col-md-8" style="padding-right:0;">
						<div class="dangtuyen">Hiện tại trường đang tuyển sinh cho kì nhập học tháng <?php echo $thang;?></div>
						<div class="available">Còn chỗ cho <?php echo the_available_students();?> du học sinh Việt Nam</div>
					</div>
					<div class="col-md-4" style="text-align:right;">
						<div class="dangtuyen">Học phí 1 năm từ</div>
						<div class="available"><span class="hocphi"><?php display_job_salary_data();?></span> đồng</div>
					</div>
				</div>
			<?php
			} else { ?>
			<div class="listing-footer">
				<div class="col-md-8" style="padding-right:0;">
					<div class="dangtuyen">&nbsp;</div>
					<div class="available">&nbsp;</div>
				</div>
				<div class="col-md-4">
					<div class="dangtuyen"><span style="float:right;">Học phí 1 năm từ</span></div>
					<div class="available"><span style="float:right;"><span class="hocphi"><?php display_job_salary_data();?></span> đồng</span></div>
				</div>
			</div>
			<?php }
			}
			else if( (int)$nowM <= $thang11 ){
				if($thang4) {
				$thang = '04/'.$nextY;?>
				<?php if(the_no_students()) { ?>
					<div class="nophoso">Có <?php echo the_no_students();?> du học sinh đã nộp hồ sơ xét tuyển cho kì nhập học tháng <?php echo $thang;?></div>
				<?php } ?>				<div class="listing-footer">
					<div class="col-md-8" style="padding-right:0;">
						<div class="dangtuyen">Hiện tại trường đang tuyển sinh cho kì nhập học tháng <?php echo $thang;?></div>
						<div class="available">Còn chỗ cho <?php echo the_available_students();?> du học sinh Việt Nam</div>
					</div>
					<div class="col-md-4" style="text-align:right;">
						<div class="dangtuyen">Học phí 1 năm từ</div>
						<div class="available"><span class="hocphi"><?php display_job_salary_data();?></span> đồng</div>
					</div>
				</div>
			<?php
			} else { ?>
			<div class="listing-footer">
				<div class="col-md-8" style="padding-right:0;">
					<div class="dangtuyen">&nbsp;</div>
					<div class="available">&nbsp;</div>
				</div>
				<div class="col-md-4">
					<div class="dangtuyen"><span style="float:right;">Học phí 1 năm từ</span></div>
					<div class="available"><span style="float:right;"><span class="hocphi"><?php display_job_salary_data();?></span> đồng</span></div>
				</div>
			</div>
			<?php }
			}
			} //is_position_filled()
			
		?>
	
	</div></div></a></div></div>
</li><div style="clear:both;"></div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script type="text/javascript">
jQuery.noConflict();
function togglekeyword(divId) {
   jQuery("#"+divId).toggle(500);

		
	  if (jQuery(".title_filter_by_keyword").hasClass('highlight')){
    	// Do something if class exists
    	jQuery(".title_filter_by_keyword").removeClass('highlight');
	} else {
	    // Do something if class does not exist
	    jQuery(".title_filter_by_keyword").addClass('highlight');
	}
	  
}
function toggleprice(divId) {
   jQuery("#"+divId).toggle(500);
		
	  if (jQuery(".title_filter_by_price").hasClass('highlight')){
    	// Do something if class exists
    	jQuery(".title_filter_by_price").removeClass('highlight');
	} else {
	    // Do something if class does not exist
	    jQuery(".title_filter_by_price").addClass('highlight');
	}
}
function toggletype(divId) {
   jQuery("#"+divId).toggle(500);	  
		
	  if (jQuery(".title_filter_by_type").hasClass('highlight')){
    	// Do something if class exists
    	jQuery(".title_filter_by_type").removeClass('highlight');
	} else {
	    // Do something if class does not exist
	    jQuery(".title_filter_by_type").addClass('highlight');
	}
	  
}
function toggleByClass(className) {
     jQuery("."+className).toggle(500);
		
	  if (jQuery(".title_filter_by_tag").hasClass('highlight')){
    	// Do something if class exists
    	jQuery(".title_filter_by_tag").removeClass('highlight');
	} else {
	    // Do something if class does not exist
	    jQuery(".title_filter_by_tag").addClass('highlight');
	}
	  
}
</script>