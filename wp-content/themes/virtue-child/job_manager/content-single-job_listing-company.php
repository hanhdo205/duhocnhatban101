<?php
/**
 * Single view Company information box
 *
 * Hooked into single_job_listing_start priority 30
 *
 * @since  1.14.0
 */

if ( ! get_the_company_name() ) {
	return;
}
?>
<div class="col-md-10">
<div class="row">
<div class="company" itemscope itemtype="http://data-vocabulary.org/Organization">
	<?php //the_company_logo(); ?>

	<p class="name">
		<!--<?php if ( $website = get_the_company_website() ) : ?>
			<a class="website" href="<?php echo esc_url( $website ); ?>" itemprop="url" target="_blank" rel="nofollow"><?php _e( 'Website', 'wp-job-manager' ); ?></a>
		<?php endif; ?>-->
		<?php //the_company_twitter(); ?>
		<?php the_company_name( '<strong itemprop="name">', '</strong>' ); ?><?php echo do_shortcode('[star rating="'.get_the_job_type()->slug.'" max="5"]'); //the_job_type(); ?><?php if ( is_position_featured( $post ) ) { echo "<i class=\"fa fa-thumbs-up\" style=\"color:#0283DF;font-size: 20px;\" aria-hidden=\"true\"></i>"; echo "<span class=\"certificate\">ĐÃ XÁC NHẬN</span>";}?>
	</p>
	<p class="location" itemprop="jobLocation"><?php the_job_address(); ?></p>
	<?php //the_company_tagline( '<p class="tagline">', '</p>' ); ?>
	<?php //the_company_video(); ?>
</div>
</div>
</div>
<div class="col-md-2" style="margin-top:-20px;    text-align: right;">
<div class="row">
	<?php the_map_address(); ?>
</div>
</div>