<?php if ( ! is_tax( 'job_listing_type' ) && empty( $job_types ) ) : ?>
	<a href="javascript:toggletype('by_type');"><div class="title_filter_by_type">Xếp hạng sao</div></a>
	<div id="by_type">
	<ul class="job_types">
		<?php foreach ( get_job_listing_types() as $type ) : ?>
			<li><label for="job_type_<?php echo $type->slug; ?>" class="<?php echo sanitize_title( $type->name ); ?>"><input type="checkbox" name="filter_job_type[]" value="<?php echo $type->slug; ?>" <?php checked( in_array( $type->slug, $selected_job_types ), true ); ?> id="job_type_<?php echo $type->slug; ?>" /> <?php if($type->slug=="0-star") {echo "<span class='shortcode-star-rating'>Chưa xếp hạng</span>"; echo "<span class='type-count'>(".$type->count.")</span>";} else { echo do_shortcode('[star rating="'.$type->name.'" max="5"]'); echo "<span class='type-count'>(".$type->count.")</span>";}?></label></li>
		<?php endforeach; ?>
	</ul>
	</div>
	<input type="hidden" name="filter_job_type[]" value="" />
<?php elseif ( $job_types ) : ?>
	<?php foreach ( $job_types as $job_type ) : ?>
		<input type="hidden" name="filter_job_type[]" value="<?php echo sanitize_title( $job_type ); ?>" />
	<?php endforeach; ?>
<?php endif; ?>