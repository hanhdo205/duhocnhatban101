<?php
/**
 *
 * Template Name: Tai khoan CTV
 *
 * Allow users to update their profiles from Frontend.
 *
 */
 
/* Get user info. */
global $current_user, $wp_roles;
//get_currentuserinfo(); //deprecated since 3.1

/* Load the registration file. */
//require_once( ABSPATH . WPINC . '/registration.php' ); //deprecated since 3.1
$error = array();    
/* If profile was saved, update profile. */
if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'update-user' ) {

    

    /* Update user password. */
    if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {
        if ( $_POST['pass1'] == $_POST['pass2'] )
            wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST['pass1'] ) ) );
        else
            $error[] = __('The passwords you entered do not match.  Your password was not updated.', 'virtue');
    }

    /* Update user information. */
    if ( !empty( $_POST['url'] ) )
        wp_update_user( array( 'ID' => $current_user->ID, 'user_url' => esc_url( $_POST['url'] ) ) );
    if ( !empty( $_POST['email'] ) ){
        if (!is_email(esc_attr( $_POST['email'] )))
            $error[] = __('The Email you entered is not valid.  please try again.', 'virtue');
        elseif(email_exists(esc_attr( $_POST['email'] )) != $current_user->id )
            $error[] = __('This email is already used by another user.  try a different one.', 'virtue');
        else{
            wp_update_user( array ('ID' => $current_user->ID, 'user_email' => esc_attr( $_POST['email'] )));
        }
    }

    if ( !empty( $_POST['first-name'] ) )
        update_user_meta( $current_user->ID, 'first_name', esc_attr( $_POST['first-name'] ) );
    if ( !empty( $_POST['last-name'] ) )
        update_user_meta($current_user->ID, 'last_name', esc_attr( $_POST['last-name'] ) );
    if ( !empty( $_POST['description'] ) )
        update_user_meta( $current_user->ID, 'description', esc_attr( $_POST['description'] ) );
    if ( !empty( $_POST['display_name'] ) )
        wp_update_user(array('ID' => $current_user->ID, 'display_name' => esc_attr( $_POST['display_name'] )));
        update_user_meta($current_user->ID, 'display_name' , esc_attr( $_POST['display_name'] ));

    /* Redirect so the page will show updated info.*/
  /*I am not Author of this Code- i dont know why but it worked for me after changing below line to if ( count($error) == 0 ){ */
    if ( count($error) == 0 ) {
        //action hook for plugins and extra fields saving
        do_action('edit_user_profile_update', $current_user->ID);
        //wp_redirect( get_permalink() );
		
        //exit;
    }
}
?>


<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="main contactformcase col-md-9" role="main">
<?php if ( count($error) > 0 ) echo '<p class="error">' . implode("<br />", $error) . '</p>'; ?>
	<div class="form-caption">
		<div class="form-title">
						<h1 class="head-title"><?php the_title(); ?></h1>
		</div>
            <?php the_content(); ?>
            <?php if ( !is_user_logged_in() ) : ?>
                    <p class="warning">
                        <?php _e('You must be logged in to edit your profile.', 'virtue'); ?>
                    </p><!-- .warning -->
            <?php else : ?>
                
                <form class="cleanlogin-form" method="post" id="adduser" action="<?php the_permalink(); ?>">
                    <p class="message"><?php _e('Thông tin chung', 'virtue'); ?></p>


                        <p>
                            <label for="username"><?php _e('Tên đăng nhập', 'virtue'); ?></label>
                            <input type="text" name="user_login" id="user_login" value="<?php the_author_meta( 'user_login', $current_user->ID ); ?>" disabled="disabled" class="regular-text">
                        </p><!-- .form-username --> 
                        
                        <p>
                            <label for="first-name"><?php _e('Họ và tên lót', 'virtue'); ?></label>
                            <input class="text-input" name="first-name" type="text" id="first-name" value="<?php the_author_meta( 'first_name', $current_user->ID ); ?>" />
                        </p><!-- .form-username -->
                        <p>
                            <label for="last-name"><?php _e('Tên', 'virtue'); ?></label>
                            <input class="text-input" name="last-name" type="text" id="last-name" value="<?php the_author_meta( 'last_name', $current_user->ID ); ?>" />
                        </p><!-- .form-username -->
                        <!-- .form-display_name -->
                        <p>
                            <label for="display_name"><?php _e('Tên hiển thị', 'virtue') ?></label>
                                    <select name="display_name" id="display_name" class="postform form-control"><br/>
                                    <?php
                                        $public_display = array();
                                        $public_display['display_nickname']  = $current_user->nickname;
                                        $public_display['display_username']  = $current_user->user_login;

                                        if ( !empty($current_user->first_name) )
                                            $public_display['display_firstname'] = $current_user->first_name;

                                        if ( !empty($current_user->last_name) )
                                            $public_display['display_lastname'] = $current_user->last_name;

                                        if ( !empty($current_user->first_name) && !empty($current_user->last_name) ) {
                                            $public_display['display_firstlast'] = $current_user->first_name . ' ' . $current_user->last_name;
                                            $public_display['display_lastfirst'] = $current_user->last_name . ' ' . $current_user->first_name;
                                        }

                                        if ( ! in_array( $current_user->display_name, $public_display ) ) // Only add this if it isn't duplicated elsewhere
                                            $public_display = array( 'display_displayname' => $current_user->display_name ) + $public_display;

                                        $public_display = array_map( 'trim', $public_display );
                                        $public_display = array_unique( $public_display );

                                        foreach ( $public_display as $id => $item ) {
                                    ?>
                                        <option <?php selected( $current_user->display_name, $item ); ?>><?php echo $item; ?></option>
                                    <?php
                                        }
                                    ?>
                                    </select>
                        </p><!-- .form-display_name -->
                        <p>
                            <label for="email"><?php _e('E-mail', 'virtue'); ?></label>
                            <input class="text-input" name="email" type="text" id="email" value="<?php the_author_meta( 'user_email', $current_user->ID ); ?>" />
                        </p><!-- .form-email -->

                    <p class="message"><?php _e('Mật khẩu', 'virtue'); ?></p>
                    <p class="cleanlogin-form-description"><?php _e( 'Bỏ qua phần này nếu bạn không có ý định thay đổi mật khẩu.','virtue' ); ?></p>
                    <p>
                        <label for="pass1"><?php _e('Mật khẩu mới', 'virtue'); ?> </label>
                        <input class="text-input" name="pass1" type="password" id="pass1" />
                    </p><!-- .form-password -->
                    <p>
                        <label for="pass2"><?php _e('Xác nhận lại mật khẩu mới', 'virtue'); ?></label>
                        <input class="text-input" name="pass2" type="password" id="pass2" />
                    </p><!-- .form-password -->

                     <?php //echo do_shortcode('[avatar_upload]');?>

                    <?php 
                        //action hook for plugin and extra fields
                        //do_action('edit_user_profile',$current_user); 
                    ?>
					<hr>
                    <p class="form-submit"  align="center">
                        <?php echo $referer; ?>
                        <input name="updateuser" type="submit" id="updateuser" class="submit button" value="<?php _e('Cập nhật tài khoản', 'virtue'); ?>" />
                        <?php wp_nonce_field( 'update-user' ) ?>
                        <input name="action" type="hidden" id="action" value="update-user" />
                    </p><!-- .form-submit -->
                </form><!-- #adduser -->
            <?php endif; ?>
			</div>
     </div>
    <?php endwhile; ?>
<?php else: ?>
    <p class="no-data">
        <?php _e('Sorry, no page matched your criteria.', 'virtue'); ?>
    </p><!-- .no-data -->
<?php endif; ?>
  
			
			
			
			
		

		